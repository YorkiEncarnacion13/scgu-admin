﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace AuthServerHelpers
{
    public class AuthServerClaimsTypes
    {
        public const string UserID = "UserID";
        public const string FirstName = "FirstName";
        public const string SecondName = "SecondName";
        public const string FirstLastName = "FirstLastName";
        public const string SecondLastName = "SecondLastName";
        public const string Email = "Email";
        public const string DepartmentID = "DepartmentID";
        public const string DepartmentName = "DepartmentName";
        public const string DepartmentCode = "DepartmentCode";
        public const string AppRole = "AppRole";
        public static string[] GetAll()
        {
            return new string[] {
                    UserID,
                    FirstName ,
                    SecondName ,
                    FirstLastName ,
                    SecondLastName,
                    Email,
                    DepartmentID,
                    DepartmentName,
                    DepartmentCode,
                    AppRole
                };
        }
    }
    public class MicmUserRoles
    {
        public string AppID { get; set; }
        public string RoleID { get; set; }
    }
    public class MicmUser
    {
        public MicmUser(IEnumerable<System.Security.Claims.Claim> claims)
        {
            var ClaimTypes = AuthServerClaimsTypes.GetAll();

            List<MicmUserRoles> Roles = claims.Where(c => c.Type == AuthServerClaimsTypes.AppRole)
                                              .Select(r => new MicmUserRoles
                                              {
                                                  AppID = r.Value.Split('?')[1],
                                                  RoleID = r.Value.Split('?')[0]
                                              }).ToList();

            if (Roles?.Count > 0) this.Roles = Roles.ToArray();

            foreach (var type in ClaimTypes)
            {

                if (type != AuthServerClaimsTypes.AppRole)
                    this[type] = claims.Where(c => c.Type == type)
                                             .Select(v => v.Value)
                                             .FirstOrDefault();

            }
        }
        private MicmUserRoles[] Roles { get; set; }
        private object this[string propertyName]
        {
            get { return this.GetType().GetProperty(propertyName).GetValue(this, null); }
            set { this.GetType().GetProperty(propertyName).SetValue(this, value, null); }
        }
        public override string ToString() => JsonConvert.SerializeObject(this);
        public string DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentCode { get; set; }
        public string UserID { get; set; }
        public string FirstName { get; set; }
        public string FirstLastName { get; set; }
        public string SecondName { get; set; }
        public string SecondLastName { get; set; }
        public string Email { get; set; }
        public string FullName
        {
            get
            {
                return FirstName + " " + SecondName + " " + FirstLastName + " " + SecondName;
            }
        }
        public string[] GetRoles(string ApplicationID)
        {
            if (Roles?.Length < 1) return new string[] { };

            else return Roles.Where(r => r.AppID == ApplicationID).Select(r => r.RoleID).ToArray();
        }
        public bool HasRole(string RoleID, string AppID)
        {
            var res = Roles?.Any(r => r.RoleID == RoleID && r.AppID == AppID);
            return (bool)res;
        }
    }
}