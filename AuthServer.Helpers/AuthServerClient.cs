﻿using IdentityModel.Client;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;
namespace AuthServer.Helpers
{
    public enum ApplicationStatuses
    {
        online = 2,
        offline = 3,
        onMaintenance = 4,
        Deactivated = 5
    }
    public enum UserStatuses { Normal = 2, Deactivated = 3, PendingPasswordReset }
    public class UserGetDTO
    {
        public string UserID { get; set; }
        public int? DepartmentID { get; set; }
        public int? JobTitleID { get; set; }
        public string Email { get; set; }
        public string FirstLastName { get; set; }
        public string FirstName { get; set; }
        public string FullName { get; set; }
        public bool isServerAdmin { get; set; }
        public string DepartmentName { get; set; }
        public string JobTitleName { get; set; }
        public UserStatuses Status { get; set; }
    }
    public class AuthServerClient
    {
        public AuthServerClient(string APP_ID, string SCGU_URI)
        {
            ApplicationID = APP_ID;
            this.SCGU_URI = SCGU_URI;
        }
        private readonly string ApplicationID;
        private readonly string SCGU_URI;
        private HttpClient _SCGU_CLIENT { get; set; }

        private async Task<HttpClient> CreateSCGUClient()
        {
            if (_SCGU_CLIENT == null)
            {
                var discoClient = new DiscoveryClient(this.SCGU_URI);

                discoClient.Policy.RequireHttps = false;

                var disco = await discoClient.GetAsync();

                
                if (disco.IsError) throw new Exception(disco.Error);
                // request token
               
                var tokenClient = new TokenClient(disco.TokenEndpoint, "SCGU_$APPLICATION$", "$CGU_$APPLICATION$ecure");

                var tokenResponse = await tokenClient.RequestClientCredentialsAsync();

                _SCGU_CLIENT = new HttpClient();
                _SCGU_CLIENT.SetBearerToken(tokenResponse.AccessToken);

            }
            return _SCGU_CLIENT;
        }
        public async Task<ApplicationStatuses> GetApplicationStatusAsync()
        {
            var client = await CreateSCGUClient();

            string route = this.SCGU_URI + $"/api/Applications/GetApplicationStatus/{ApplicationID}";

            var response = await client.GetAsync(route);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                var ApplicationStatus = JsonConvert.DeserializeObject<ApplicationStatuses>(responseContent);
                return ApplicationStatus;
            }
            throw new Exception("The SCGU Server responded with an http status code " + response.StatusCode);
        }

        /// <summary>
        /// Returns the users that have the given Role.
        /// </summary>
        /// <param name="ApplicationID"></param>
        /// <param name="AppRoleID"></param>
        /// <returns></returns>
        public async Task<UserGetDTO[]> GetUsersByRoleAsync(string ApplicationID, int AppRoleID)
        {
            var client = await CreateSCGUClient();

            string route = this.SCGU_URI + $"/api/Applications/GetApplicationUsersByRole?ApplicationID={ApplicationID}&RoleID={AppRoleID}";

            var response = await client.GetAsync(route);

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                var users = JsonConvert.DeserializeObject<UserGetDTO[]>(responseContent);
                return users;
            }
            throw new Exception("The SCGU Server responded with an http status code " + response.StatusCode);
        }
        /// <summary>
        /// Returns the users that have access to given application
        /// </summary>
        /// <param name="ApplicationID"></param>
        /// <returns></returns>
        public async Task<UserGetDTO[]> GetApplicationUsers(string ApplicationID)
        {
            var client = await CreateSCGUClient();
            var response = await client.GetAsync(this.SCGU_URI + $"/api/Applications/GetApplicationUsers/{ApplicationID}");

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                var users = JsonConvert.DeserializeObject<UserGetDTO[]>(responseContent);
                return users;
            }
            throw new Exception("The SCGU Server responded with an http status code " + response.StatusCode);
        }

        public async Task<UserGetDTO[]> GetUsersAsync(params string[] IDs)
        {
            var client = await CreateSCGUClient();

            string userIds = string.Join(",", IDs);


            var response = await client.GetAsync(this.SCGU_URI + $"/api/Users/GetUsersList?UserIds={userIds}");

            if (response.IsSuccessStatusCode)
            {
                var responseContent = await response.Content.ReadAsStringAsync();
                var users = JsonConvert.DeserializeObject<UserGetDTO[]>(responseContent);
                return users;
            }
            throw new Exception("The SCGU Server responded with an http status code " + response.StatusCode);
        }

    }
}
