﻿using IdentityModel.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
namespace AuthServerHelpers
{
    //public class AplicationUserRoleDTO
    //{
    //    public int RoleID { get; set; }
    //    public string AppID { get; set; }
    //}
    public class AuthServerClaimsTypes
    {
        public const string UserID = "UserID";
        public const string FirstName = "FirstName";
        public const string SecondName = "SecondName";
        public const string FirstLastName = "FirstLastName";
        public const string SecondLastName = "SecondLastName";
        public const string Email = "Email";
        public const string DepartmentID = "DepartmentID";
        public const string DepartmentName = "DepartmentName";
        public const string DepartmentCode = "DepartmentCode";
        //public const string AppRole = "AppRole";
        public static string[] GetAll()
        {
            return new string[] {
                    UserID,
                    FirstName ,
                    SecondName ,
                    FirstLastName ,
                    SecondLastName,
                    Email,
                    DepartmentID,
                    DepartmentName,
                    DepartmentCode
                };
        }
    }
    public class AplicationUserRoleDTO
    {
        public int RoleID { get; set; }
        public string AppID { get; set; }
        public string RoleName { get; set; }
    }
    public class MicmUserOnApp : MicmUser
    {
        public MicmUserOnApp(IEnumerable<Claim> claims) : base(claims)
        {
        }
        public string ApplicationID { get; set; }
        public MicmUserWithSCGUProxy SetAuthority(string SCGU_URI)
        {
            var SERIALIZED = JsonConvert.SerializeObject(this);
            var user = JsonConvert.DeserializeObject<MicmUserWithSCGUProxy>(SERIALIZED);
            user.SGU_URI = SCGU_URI;
            return user;
        }
    }
    public class MicmUserWithSCGUProxy : MicmUserOnApp
    {
        private HttpClient _SCGU_CLIENT { get; set; }
        public MicmUserWithSCGUProxy(IEnumerable<Claim> claims) : base(claims)
        {
        }
        public string SGU_URI { get; set; }

        private List<AplicationUserRoleDTO> Roles { get; set; }
        /// <summary>
        /// Makes a request ASYNCHRONOUSLY to the SCGU Server to get the User's Roles for the given Application
        /// </summary>
        /// <returns></returns>
        public async Task<List<AplicationUserRoleDTO>> GetRolesAsync()
        {
            if (Roles == null)
                await LoadRolesFromSCGUEndpoint();
            return this.Roles;
        }
        public async Task<bool> HasRoleAsync(int RoleID)
        {
            var cliente = await CreateSCGUClient();

            string ReqPath = $"{SGU_URI}/api/Roles/HasRole?AppId={ApplicationID}&UserID={UserID}&RoleID={RoleID}";

            var res = await cliente.GetAsync(ReqPath);

            bool Result = JsonConvert.DeserializeObject<bool>(await res.Content.ReadAsStringAsync());

            return Result;
        }
        private async Task LoadRolesFromSCGUEndpoint()
        {
            HttpClient client = await CreateSCGUClient();

            string ReqURI = SGU_URI + $"/api/Roles/GetRoles?UserID={UserID}&ApplicationID={ApplicationID}";
            var response = await client.GetAsync(ReqURI);

            var result = await response.Content.ReadAsStringAsync();
            var Roles = JsonConvert.DeserializeObject<List<AplicationUserRoleDTO>>(result);

            //var _Roles = Roles.Select(r => new AplicationUserRoleDTO
            //{
            //    AppID = r.AppID,
            //    RoleID = r.RoleID,
            //    RoleName = r.RoleName
            //    //AppID = r.AppID,
            //    //RoleID = r.RoleID
            //}).ToList();
            this.Roles = Roles;
        }

        private async Task<HttpClient> CreateSCGUClient()
        {
            if (_SCGU_CLIENT == null)
            {
                var discoClient = new DiscoveryClient(this.SGU_URI);
                discoClient.Policy.RequireHttps = false;
                var disco = await discoClient.GetAsync();
                if (disco.IsError) throw new Exception(disco.Error);
                // request token
                var tokenClient = new TokenClient(disco.TokenEndpoint, "SCGU_$APPLICATION$", "$CGU_$APPLICATION$ecure");

                var tokenResponse = await tokenClient.RequestClientCredentialsAsync();

                _SCGU_CLIENT = new HttpClient();
                _SCGU_CLIENT.SetBearerToken(tokenResponse.AccessToken);

            }
            return _SCGU_CLIENT;
        }
    }



    public class MicmUser
    {
        private IEnumerable<Claim> Claims { get; set; }
        public MicmUser(IEnumerable<Claim> claims)
        {
            var ClaimTypes = AuthServerClaimsTypes.GetAll();

            this.Claims = claims;

            foreach (var type in ClaimTypes)
            {
                try
                {
                    this[type] = claims.Where(c => c.Type == type)
                                             .Select(v => v.Value)
                                             .FirstOrDefault();
                }
                catch
                {
                    // Silent shhhhh
                }
            }
        }

        private object this[string propertyName]
        {
            get { return this.GetType().GetProperty(propertyName).GetValue(this, null); }
            set { this.GetType().GetProperty(propertyName).SetValue(this, value, null); }
        }

        public MicmUserOnApp SetApp(string ApplicationID)
        {

            var Serialized = JsonConvert.SerializeObject(this);
            var user = JsonConvert.DeserializeObject<MicmUserOnApp>(Serialized);
            //var user = this as MicmUserOnApp;
            user.ApplicationID = ApplicationID;
            return user;
        }
        public override string ToString() => JsonConvert.SerializeObject(this);
        public string DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentCode { get; set; }
        public string UserID { get; set; }
        public string FirstName { get; set; }
        public string FirstLastName { get; set; }
        public string SecondName { get; set; }
        public string SecondLastName { get; set; }
        public string Email { get; set; }
        public string FullName
        {
            get
            {
                return string.Join(" ", new string[] { FirstName, SecondName, FirstLastName, SecondName });
            }
        }

    }
}