﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.DAL.Contexts.AuthServer.Migrations
{
    public partial class upsfulltextlookuptouserstable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FULLTEXT INDEX ON Users", true);

            migrationBuilder.Sql(@"
ALTER TABLE Users
DROP COLUMN FULLTEXTLOOKUP", true);
            migrationBuilder.Sql(@"
ALTER TABLE Users
ADD FULLTEXTLOOKUP AS ([UserID] + ' ' + [FirstName] + ' ' + ISNULL([SecondName],'') + ' ' +[FirstLastName] + ' ' + ISNULL([SecondLastName],'') + ' ' +[Email])", true);
            migrationBuilder.Sql(@"CREATE FULLTEXT INDEX ON [Users]([FULLTEXTLOOKUP])
              KEY INDEX  [PK_Users]
              ON USERS_FT WITH CHANGE_TRACKING AUTO;", true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
