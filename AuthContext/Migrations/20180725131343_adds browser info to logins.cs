﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.DAL.Contexts.AuthServer.Migrations
{
    public partial class addsbrowserinfotologins : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Browser",
                table: "UserLogin",
                maxLength: 350,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Browser_Version",
                table: "UserLogin",
                maxLength: 350,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IP",
                table: "UserLogin",
                maxLength: 350,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Language",
                table: "UserLogin",
                maxLength: 350,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OS",
                table: "UserLogin",
                maxLength: 350,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OS_Version",
                table: "UserLogin",
                maxLength: 350,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Screen",
                table: "UserLogin",
                maxLength: 350,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Browser",
                table: "UserLogin");

            migrationBuilder.DropColumn(
                name: "Browser_Version",
                table: "UserLogin");

            migrationBuilder.DropColumn(
                name: "IP",
                table: "UserLogin");

            migrationBuilder.DropColumn(
                name: "Language",
                table: "UserLogin");

            migrationBuilder.DropColumn(
                name: "OS",
                table: "UserLogin");

            migrationBuilder.DropColumn(
                name: "OS_Version",
                table: "UserLogin");

            migrationBuilder.DropColumn(
                name: "Screen",
                table: "UserLogin");
        }
    }
}
