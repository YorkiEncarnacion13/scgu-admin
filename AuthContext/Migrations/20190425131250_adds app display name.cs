﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.DAL.Contexts.AuthServer.Migrations
{
    public partial class addsappdisplayname : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DisplayName",
                table: "Applications",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DisplayName",
                table: "Applications");
        }
    }
}
