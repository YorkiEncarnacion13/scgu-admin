﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.DAL.Contexts.AuthServer.Migrations
{
    public partial class Addfulltextlookuptousers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
  ALTER TABLE [Users]
  ADD FULLTEXTLOOKUP AS ( [UserID] + ' ' + [Address] + ' ' + ISNULL([BirthCountryCountryIso2Code],'') + ' ' +[Email] + ' ' +[FirstLastName] + ' ' +
						  [FirstName] + ' ' + ISNULL([SecondLastName],'') + ' ' + ISNULL([SecondName],''))");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
