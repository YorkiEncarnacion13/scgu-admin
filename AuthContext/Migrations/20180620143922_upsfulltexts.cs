﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.DAL.Contexts.AuthServer.Migrations
{
    public partial class upsfulltexts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            CREATE FULLTEXT CATALOG JOBTITLES_FT; 
            CREATE FULLTEXT INDEX ON [JobTitles]([JobTitleName]) 
            KEY INDEX  [PK_JobTitles]
            ON JOBTITLES_FT WITH CHANGE_TRACKING AUTO;", true);
            migrationBuilder.Sql(@"
            CREATE FULLTEXT CATALOG DEPARTMENTS_FT; 
            CREATE FULLTEXT INDEX ON [Departments]([DepartmentName]) 
            KEY INDEX  [PK_Departments]
            ON DEPARTMENTS_FT WITH CHANGE_TRACKING AUTO;", true);
            migrationBuilder.Sql(@"
            CREATE FULLTEXT CATALOG COUNTRIES_FT; 
            CREATE FULLTEXT INDEX ON [Countries]([CountryName]) 
            KEY INDEX  [PK_Countries]
            ON COUNTRIES_FT WITH CHANGE_TRACKING AUTO;", true);
            migrationBuilder.Sql(@"
            CREATE FULLTEXT CATALOG Municipalities_FT; 
            CREATE FULLTEXT INDEX ON [Municipalities]([MunicipalityName]) 
            KEY INDEX [PK_Municipalities] 
            ON Municipalities_FT WITH CHANGE_TRACKING AUTO;", true);
            migrationBuilder.Sql(@"
            CREATE FULLTEXT CATALOG PROVINCES_FT; 
            CREATE FULLTEXT INDEX ON [Provinces]([ProvinceName]) 
            KEY INDEX  [PK_Provinces]
            ON PROVINCES_FT WITH CHANGE_TRACKING AUTO;", true);
            migrationBuilder.Sql(@"
            CREATE FULLTEXT CATALOG SECTORS_FT; 
            CREATE FULLTEXT INDEX ON [Sectors]([SectorName]) 
            KEY INDEX  [PK_Sectors]
            ON SECTORS_FT WITH CHANGE_TRACKING AUTO;", true);
            migrationBuilder.Sql(@"
              CREATE FULLTEXT CATALOG USERS_FT; 
              CREATE FULLTEXT INDEX ON [Users]([FullName])
              KEY INDEX  [PK_Users]
              ON USERS_FT WITH CHANGE_TRACKING AUTO;", true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
