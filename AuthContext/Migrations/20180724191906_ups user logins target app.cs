﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.DAL.Contexts.AuthServer.Migrations
{
    public partial class upsuserloginstargetapp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TargetApp_ApplicationID",
                table: "UserLogin",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserLogin_TargetApp_ApplicationID",
                table: "UserLogin",
                column: "TargetApp_ApplicationID");

            migrationBuilder.AddForeignKey(
                name: "FK_UserLogin_Applications_TargetApp_ApplicationID",
                table: "UserLogin",
                column: "TargetApp_ApplicationID",
                principalTable: "Applications",
                principalColumn: "ApplicationID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserLogin_Applications_TargetApp_ApplicationID",
                table: "UserLogin");

            migrationBuilder.DropIndex(
                name: "IX_UserLogin_TargetApp_ApplicationID",
                table: "UserLogin");

            migrationBuilder.DropColumn(
                name: "TargetApp_ApplicationID",
                table: "UserLogin");
        }
    }
}
