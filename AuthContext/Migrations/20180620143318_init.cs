﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.DAL.Contexts.AuthServer.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Applications",
                columns: table => new
                {
                    ApplicationID = table.Column<string>(maxLength: 140, nullable: false),
                    UsersCount = table.Column<int>(nullable: false),
                    IconUrl = table.Column<string>(nullable: true),
                    Uri = table.Column<string>(nullable: false),
                    RedirectUri = table.Column<string>(nullable: true),
                    PostLogoutRedirectUri = table.Column<string>(nullable: true),
                    Description = table.Column<string>(maxLength: 400, nullable: true),
                    GrantTypes = table.Column<int>(nullable: false),
                    IntroductionDate = table.Column<DateTime>(nullable: false),
                    ApplicationStatus = table.Column<int>(nullable: false),
                    RequiresConcent = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Applications", x => x.ApplicationID);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    CountryIso2Code = table.Column<string>(maxLength: 40, nullable: false),
                    Iso3Code = table.Column<string>(maxLength: 40, nullable: true),
                    CountryName = table.Column<string>(maxLength: 100, nullable: false),
                    NationalityMasculine = table.Column<string>(maxLength: 100, nullable: true),
                    NationalityFemenine = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.CountryIso2Code);
                });

            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    DepartmentID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DepartmentName = table.Column<string>(maxLength: 400, nullable: false),
                    Codigo = table.Column<string>(maxLength: 40, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.DepartmentID);
                });

            migrationBuilder.CreateTable(
                name: "JobTitles",
                columns: table => new
                {
                    JobTitleID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    JobTitleName = table.Column<string>(maxLength: 400, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobTitles", x => x.JobTitleID);
                });

            migrationBuilder.CreateTable(
                name: "Locations",
                columns: table => new
                {
                    LocationID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LocationName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locations", x => x.LocationID);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    UserRoleID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserRoleName = table.Column<string>(maxLength: 100, nullable: true),
                    Description = table.Column<string>(maxLength: 300, nullable: true),
                    ApplicationID = table.Column<string>(maxLength: 140, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => x.UserRoleID);
                    table.ForeignKey(
                        name: "FK_UserRoles_Applications_ApplicationID",
                        column: x => x.ApplicationID,
                        principalTable: "Applications",
                        principalColumn: "ApplicationID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Provinces",
                columns: table => new
                {
                    ProvinceID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProvinceName = table.Column<string>(nullable: true),
                    CountryIso2Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Provinces", x => x.ProvinceID);
                    table.ForeignKey(
                        name: "FK_Provinces_Countries_CountryIso2Code",
                        column: x => x.CountryIso2Code,
                        principalTable: "Countries",
                        principalColumn: "CountryIso2Code",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Municipalities",
                columns: table => new
                {
                    MunicipalityID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MunicipalityName = table.Column<string>(nullable: true),
                    ProvinceID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Municipalities", x => x.MunicipalityID);
                    table.ForeignKey(
                        name: "FK_Municipalities_Provinces_ProvinceID",
                        column: x => x.ProvinceID,
                        principalTable: "Provinces",
                        principalColumn: "ProvinceID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sectors",
                columns: table => new
                {
                    SectorID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SectorName = table.Column<string>(nullable: true),
                    MunicipalityID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sectors", x => x.SectorID);
                    table.ForeignKey(
                        name: "FK_Sectors_Municipalities_MunicipalityID",
                        column: x => x.MunicipalityID,
                        principalTable: "Municipalities",
                        principalColumn: "MunicipalityID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserID = table.Column<string>(maxLength: 100, nullable: false),
                    FirstName = table.Column<string>(nullable: false),
                    SecondName = table.Column<string>(nullable: true),
                    FirstLastName = table.Column<string>(nullable: false),
                    SecondLastName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: false),
                    Gender = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    BirthDate = table.Column<DateTime>(nullable: false),
                    BirthCountryCountryIso2Code = table.Column<string>(nullable: true),
                    JobTitleID = table.Column<int>(nullable: true),
                    DepartmentID = table.Column<int>(nullable: true),
                    LocationID = table.Column<int>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    SecurityKey = table.Column<byte[]>(nullable: true),
                    SectorID = table.Column<int>(nullable: true),
                    MunicipalityID = table.Column<int>(nullable: true),
                    ProvinceID = table.Column<int>(nullable: true),
                    Address = table.Column<string>(maxLength: 400, nullable: true),
                    isServerAdmin = table.Column<bool>(nullable: false),
                    PasswordResetToken = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(maxLength: 420, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserID);
                    table.ForeignKey(
                        name: "FK_Users_Countries_BirthCountryCountryIso2Code",
                        column: x => x.BirthCountryCountryIso2Code,
                        principalTable: "Countries",
                        principalColumn: "CountryIso2Code",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Departments_DepartmentID",
                        column: x => x.DepartmentID,
                        principalTable: "Departments",
                        principalColumn: "DepartmentID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_JobTitles_JobTitleID",
                        column: x => x.JobTitleID,
                        principalTable: "JobTitles",
                        principalColumn: "JobTitleID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Locations_LocationID",
                        column: x => x.LocationID,
                        principalTable: "Locations",
                        principalColumn: "LocationID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Municipalities_MunicipalityID",
                        column: x => x.MunicipalityID,
                        principalTable: "Municipalities",
                        principalColumn: "MunicipalityID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Provinces_ProvinceID",
                        column: x => x.ProvinceID,
                        principalTable: "Provinces",
                        principalColumn: "ProvinceID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Sectors_SectorID",
                        column: x => x.SectorID,
                        principalTable: "Sectors",
                        principalColumn: "SectorID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ApplicationUsers",
                columns: table => new
                {
                    ApplicationUserID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApplicationID = table.Column<string>(maxLength: 140, nullable: true),
                    UserID = table.Column<string>(maxLength: 100, nullable: true),
                    RegistrationDate = table.Column<DateTime>(nullable: false),
                    SharePhones = table.Column<bool>(nullable: false),
                    ShareEmail = table.Column<bool>(nullable: false),
                    ShareDepartmentInfo = table.Column<bool>(nullable: false),
                    ShareGender = table.Column<bool>(nullable: false),
                    ShareAddress = table.Column<bool>(nullable: false),
                    SharePersonalInfo = table.Column<bool>(nullable: false),
                    AppUserStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationUsers", x => x.ApplicationUserID);
                    table.ForeignKey(
                        name: "FK_ApplicationUsers_Applications_ApplicationID",
                        column: x => x.ApplicationID,
                        principalTable: "Applications",
                        principalColumn: "ApplicationID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApplicationUsers_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserPhones",
                columns: table => new
                {
                    PhoneID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserID = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPhones", x => x.PhoneID);
                    table.ForeignKey(
                        name: "FK_UserPhones_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ApplicationUserRoles",
                columns: table => new
                {
                    ApplicationUserRoleID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleUserRoleID = table.Column<int>(nullable: true),
                    ApplicationUserID = table.Column<int>(nullable: true),
                    RoleAssignmentDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationUserRoles", x => x.ApplicationUserRoleID);
                    table.ForeignKey(
                        name: "FK_ApplicationUserRoles_ApplicationUsers_ApplicationUserID",
                        column: x => x.ApplicationUserID,
                        principalTable: "ApplicationUsers",
                        principalColumn: "ApplicationUserID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ApplicationUserRoles_UserRoles_RoleUserRoleID",
                        column: x => x.RoleUserRoleID,
                        principalTable: "UserRoles",
                        principalColumn: "UserRoleID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUserRoles_ApplicationUserID",
                table: "ApplicationUserRoles",
                column: "ApplicationUserID");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUserRoles_RoleUserRoleID",
                table: "ApplicationUserRoles",
                column: "RoleUserRoleID");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUsers_ApplicationID",
                table: "ApplicationUsers",
                column: "ApplicationID");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUsers_UserID",
                table: "ApplicationUsers",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Municipalities_ProvinceID",
                table: "Municipalities",
                column: "ProvinceID");

            migrationBuilder.CreateIndex(
                name: "IX_Provinces_CountryIso2Code",
                table: "Provinces",
                column: "CountryIso2Code");

            migrationBuilder.CreateIndex(
                name: "IX_Sectors_MunicipalityID",
                table: "Sectors",
                column: "MunicipalityID");

            migrationBuilder.CreateIndex(
                name: "IX_UserPhones_UserID",
                table: "UserPhones",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_ApplicationID",
                table: "UserRoles",
                column: "ApplicationID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_BirthCountryCountryIso2Code",
                table: "Users",
                column: "BirthCountryCountryIso2Code");

            migrationBuilder.CreateIndex(
                name: "IX_Users_DepartmentID",
                table: "Users",
                column: "DepartmentID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_JobTitleID",
                table: "Users",
                column: "JobTitleID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_LocationID",
                table: "Users",
                column: "LocationID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_MunicipalityID",
                table: "Users",
                column: "MunicipalityID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_ProvinceID",
                table: "Users",
                column: "ProvinceID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_SectorID",
                table: "Users",
                column: "SectorID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApplicationUserRoles");

            migrationBuilder.DropTable(
                name: "UserPhones");

            migrationBuilder.DropTable(
                name: "ApplicationUsers");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Applications");

            migrationBuilder.DropTable(
                name: "Departments");

            migrationBuilder.DropTable(
                name: "JobTitles");

            migrationBuilder.DropTable(
                name: "Locations");

            migrationBuilder.DropTable(
                name: "Sectors");

            migrationBuilder.DropTable(
                name: "Municipalities");

            migrationBuilder.DropTable(
                name: "Provinces");

            migrationBuilder.DropTable(
                name: "Countries");
        }
    }
}
