﻿using AutoMapper;
using AutoMapper.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Models.Auth;
using Models.System;
using Models.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.DAL.Contexts.AuthServer
{

    public class AuthContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<JobTitle> JobTitles { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<UserPhone> UserPhones { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Application> Applications { get; set; }
        //public DbSet<DepartmentLocation> DepartmentLocations { get; set; }
        public DbSet<ApplicationUserRole> ApplicationUserRoles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Sector> Sectors { get; set; }
        public DbSet<Municipality> Municipalities { get; set; }
        public DbSet<Province> Provinces { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            var constrbldr = new SqlConnectionStringBuilder
                ("Data Source=mic-svr-db;Initial Catalog=MICM_SCGU;Connect Timeout=1000;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            constrbldr.UserID = "SCGU";
            constrbldr.Password = "disque_theSCGUPassword";

            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(
                    // "Data Source=JPDTIC22;Initial Catalog=SCGU;Integrated Security=True;Connect Timeout=1000;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"
                    // "Data Source=JPDTIC22;Initial Catalog=Auth100;Integrated Security=True;Connect Timeout=1000;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"
                    constrbldr.ConnectionString
                    );
            }
        }
    }

    #region Helpers
    public static class Ext
    {
        public static void DetachLocal<T>(this DbContext context, T Entry) where T : class
        {
            var local = context.Set<T>()
                .Local
                .FirstOrDefault(entry => entry.Equals(Entry));
            if (local != null)
            {
                context.Entry(local).State = EntityState.Detached;
            }
            context.Entry(Entry).State = EntityState.Modified;
        }
    }
    public class CustomTypeSqlQuery<T> where T : class
    {
        private IMapper _mapper;

        public DatabaseFacade DatabaseFacade { get; set; }
        public string SQLQuery { get; set; }
        public SqlParameter[] Params { get; set; }
        public CustomTypeSqlQuery()
        {
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddDataReaderMapping();

                cfg.CreateMap<IDataRecord, T>();
            }).CreateMapper();
        }

        public async Task<IList<T>> ToListAsync()
        {
            IList<T> results = new List<T>();
            var conn = DatabaseFacade.GetDbConnection();
            try
            {
                await conn.OpenAsync();
                using (var command = conn.CreateCommand())
                {
                    command.CommandText = SQLQuery;

                    if (Params?.Length > 0)
                        command.Parameters.AddRange(Params);



                    DbDataReader reader = await command.ExecuteReaderAsync();

                    if (reader.HasRows)
                        results = _mapper.Map<IDataReader, IEnumerable<T>>(reader)
                                         .ToList();
                    reader.Dispose();

                    conn.Close();
                }
            }
            catch (Exception e)
            {
                conn.Close();
                System.Diagnostics.Debug.WriteLine(e);
                throw e;
            }
            return results;
        }

        public async Task<T> FirstOrDefaultAsync()
        {
            T result = null;
            var conn = DatabaseFacade.GetDbConnection();
            try
            {
                await conn.OpenAsync();
                using (var command = conn.CreateCommand())
                {

                    command.CommandText = SQLQuery;

                    if (Params?.Length > 0)
                        command.Parameters.AddRange(Params);

                    DbDataReader reader = await command.ExecuteReaderAsync();

                    if (reader.HasRows)
                    {
                        var results = _mapper.Map<IDataReader, IEnumerable<T>>(reader)
                                             .ToList();
                        result = results.FirstOrDefault();
                    }
                    reader.Dispose();
                }
            }
            finally
            {
                conn.Close();
            }
            return result;
        }
    }
    public static class DatabaseExtensions
    {
        public static CustomTypeSqlQuery<T> SqlQuery<T>(
                this DatabaseFacade database,
                string sqlQuery, params SqlParameter[] Parameters) where T : class
        {
            return new CustomTypeSqlQuery<T>()
            {
                DatabaseFacade = database,
                SQLQuery = sqlQuery,
                Params = Parameters
            };
        }
    }
    #endregion
}
