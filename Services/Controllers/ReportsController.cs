﻿using jsreport.Binary;
using jsreport.Local;
using jsreport.Types;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Services.Controllers
{
    public class RenderRequestParameters
    {
        public string HtmlContent { get; set; }
        public string Footer { get; set; }
        public string Header { get; set; }
        public string Margin { get; set; }
        public string FooterHeight { get; set; }
        public PhantomOrientation? Orientation { get; set; }
        public PhantomFormat? Format { get; set; }
        public Engine? Engine { get; set; }
    }
    public class ReportsController : Controller
    {
        ILocalWebServerReportingService _reportingService;
        public ReportsController()
        {
            _reportingService = new LocalReporting()
                                    .UseBinary(JsReportBinary.GetBinary())
                                    .AsWebServer()
                                    .Create();

            Task.WaitAll(_reportingService.StartAsync());

        }

        [HttpPost(template: "Render")]
        public async Task<IActionResult> Render([FromBody] RenderRequestParameters request)
        {

            var report = await _reportingService.RenderAsync(new RenderRequest
            {
                Template = new Template
                {
                    Content = request.HtmlContent,
                    Engine = request.Engine,
                    Recipe = Recipe.PhantomPdf,
                    Phantom = new Phantom
                    {
                        Margin = request.Margin,
                        Footer = request.Footer,
                        FooterHeight = request.FooterHeight,
                        Orientation = request.Orientation ?? PhantomOrientation.Portrait,
                        Format = request.Format,
                    }
                }
            });
            return File(report.Content, report.Meta.ContentType);
        }
    }
}
