﻿using System.ComponentModel.DataAnnotations;

namespace Models.Auth
{
    public class Country
    {
        [Key, StringLength(40)]
        public string CountryIso2Code { get; set; }
        [StringLength(40)]
        public string Iso3Code { get; set; }
        [StringLength(100), Required]
        public string CountryName { get; set; }
        [StringLength(100)]
        public string NationalityMasculine { get; set; }
        [StringLength(100)]
        public string NationalityFemenine { get; set; }
    }
}
