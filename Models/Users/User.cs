﻿using Models.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Auth
{
    public enum Genders { Masculine = 2, Femenine = 3, Other = 4 }
    public enum UserStatuses { Normal = 2, Deactivated = 3, PendingPasswordReset }
    public class User
    {
        [Key, StringLength(100)]
        public string UserID { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        [Required]
        public string FirstLastName { get; set; }
        public string SecondLastName { get; set; }
        [EmailAddress, Required]
        public string Email { get; set; }
        public Genders Gender { get; set; }
        public UserStatuses Status { get; set; }
        public DateTime BirthDate { get; set; }
        //public bool isActive { get; set; }
        public Country BirthCountry { get; set; }

        public int? JobTitleID { get; set; }
        [ForeignKey("JobTitleID")]
        public JobTitle JobTitle { get; set; }

        public int? DepartmentID { get; set; }
        [ForeignKey("DepartmentID")]
        public Department Department { get; set; }


        public int? LocationID { get; set; }
        [ForeignKey("LocationID")]
        public Location Location { get; set; }
        public IEnumerable<UserPhone> Phones { get; set; }
        // Hashed password
        public string Password { get; set; }
        // Stores the password's salt.
        public byte[] SecurityKey { get; set; }

        public int? SectorID { get; set; }
        [ForeignKey("SectorID")]
        public Sector Sector { get; set; }


        public int? MunicipalityID { get; set; }
        [ForeignKey("MunicipalityID")]
        public Municipality Municipality { get; set; }

        public int? ProvinceID { get; set; }
        [ForeignKey("ProvinceID")]
        public Province Province { get; set; }
        [StringLength(400)]
        public string Address { get; set; }
        /// <summary>
        /// Enables the user as a server Admin,
        /// which can register applications, disable them,
        /// register users and configures them etc...
        /// </summary>
        public bool isServerAdmin { get; set; }
        public string PasswordResetToken { get; set; }
        [StringLength(140 * 3)]
        public string FullName { get; set; }
    }
}
