﻿using Models.Users;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Toolbelt.ComponentModel.DataAnnotations.Schema;
namespace Models.Auth
{
    public class Department
    {
        [Key]
        public int DepartmentID { get; set; }
        [StringLength(400), Required]
        public string DepartmentName { get; set; }

        [StringLength(40), Index(IsUnique = true)]
        public string Codigo { get; set; }
        //public IEnumerable<DepartmentLocation> Locations { get; set; }
    }
}
