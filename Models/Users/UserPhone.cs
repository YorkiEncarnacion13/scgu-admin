﻿using System.ComponentModel.DataAnnotations;
namespace Models.Auth
{
    public enum UserPhoneTypes { Mobile, Home, WorkFleet }
    public class UserPhone
    {
        [Key]
        public int PhoneID { get; set; }
        public User User { get; set; }
        public string PhoneNumber { get; set; }
        public UserPhoneTypes PhoneType { get; set; }
    }
}
