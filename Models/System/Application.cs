﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Models.System
{
    public enum GrantTypes
    {
        Hybrid = 2, Implicit = 0, ClientCredentials = 3
    }
    public enum ApplicationStatuses
    {
        online = 2,
        offline = 3,
        onMaintenance = 4,
        Deactivated = 5
    }
    public class Application
    {
        [Key, StringLength(140)]
        public string ApplicationID { get; set; }
        //public string ApplicationName { get; set; }
        public int UsersCount { get; set; }
        public string IconUrl { get; set; }
        /// <summary>
        /// Base url of the application 
        /// EX: http://google.com
        /// </summary>
        [Required]
        public string Uri { get; set; }
        /// <summary>
        /// Where to redirect to after login
        /// </summary>
        //[Required]
        public string RedirectUri { get; set; }
        /// <summary>
        /// Where to redirect to after logout
        /// </summary>

        public string PostLogoutRedirectUri { get; set; }
        //public string GrantType { get; set; }
        [StringLength(400)]
        public string Description { get; set; }
        public GrantTypes GrantTypes { get; set; }
        public DateTime IntroductionDate { get; set; }
        public IEnumerable<ApplicationUser> AppUsers { get; set; }
        public List<UserRole> UserRoles { get; set; }
        public ApplicationStatuses ApplicationStatus { get; set; }
        public bool RequiresConcent { get; set; }
    }
}