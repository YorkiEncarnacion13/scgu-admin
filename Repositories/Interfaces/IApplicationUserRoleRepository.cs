﻿using Models.System;
using System.Threading.Tasks;

namespace Repositories.Interfaces
{
    public interface IApplicationUserRoleRepository
    {
        Task TurnOffRolesAsync(ApplicationUser applicationUser, params int[] RolesIDs);
        Task TurnOnRolesAsync(ApplicationUser applicationUser, params int[] RolesIDs);
    }
}
