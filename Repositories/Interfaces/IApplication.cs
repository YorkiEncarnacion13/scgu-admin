﻿using AuthServer.Interfaces;
using Models.System;
using Repositories.DTOS.Creates;
using Repositories.DTOS.Gets;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories.Interfaces
{
    public interface IApplicationRepository : IRepository<Application>
    {
        Task<Application> CreateFromDTOAsync(ApplicationCreateDTO createDTO);
        Task<Application> GetByIdAsync(string ID);
        Task<IEnumerable<Application>> GetList(int page, int NumberOfElementsPerPage);
        Task CreateApplicationUserFromDTO(NewApplicationUserDTO NewAppUserDTO);
        Task<AppViewModel> GetAppViewModel(string ID);
        Task<AppManifest> GetManifest(string ID);
    }
}
