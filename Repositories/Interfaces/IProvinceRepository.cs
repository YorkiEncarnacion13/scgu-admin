﻿using Models.Users;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories.Interfaces
{
   public interface IProvinceRepository 
    {
        Task<IEnumerable<Province>> GetAllAsync();
        IEnumerable<Province> GetAll();
    }
}
