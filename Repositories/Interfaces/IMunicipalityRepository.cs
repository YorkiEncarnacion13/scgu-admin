﻿using Models.Users;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories.Interfaces
{
   public interface IMunicipalityRepository
    {
        Task<IEnumerable<Municipality>> GetAllAsync();
        IEnumerable<Municipality> GetAll();
        IEnumerable<Municipality> GetProvinceMunicipalities(int ProvinceID);
    }
}
