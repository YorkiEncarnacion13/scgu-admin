﻿using AuthServer.Interfaces;
using Models.Auth;
using Repositories.DTOS.Creates;
using Repositories.DTOS.Gets;
using Repositories.DTOS.Results;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        User FindByUserID(string ID);
        User FindByEmail(string email);
        Task<User> FindByEmailOrIDAsync(string identifier);
        Task<User> FindByUserIDAsync(string ID, bool IncludeDepartment = false);
        bool ValidateCredentials(string identifier, string password);
        Task<User> FindByEmailAsync(string email);
        UserCreationResult CreateUser(UserCreateDTO UserDTO);
        UserDTO GetUserDTO(string ID);
        bool Exists(string ID);
        bool ExistsByEmail(string Email);
        Task<IList<UserListDTO>> GetUserAsync(string SearchTerm);
    }
}
