﻿using AuthServer.Interfaces;
using Models.Auth;
using System.Collections.Generic;

namespace Repositories.Interfaces
{
    public interface ILocationRepository : IRepository<Location>
    {
        IEnumerable<Location> SearchFor(string SearchTermn);
        IEnumerable<Location> GetAll();
    }
}
