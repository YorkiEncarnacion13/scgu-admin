﻿using AuthServer.Interfaces;
using Models.Auth;
using Repositories.DTOS.Gets;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories.Interfaces
{
    public interface IJobTitleRepository : IRepository<JobTitle>
    {
        IEnumerable<JobTitle> GetAll();
        IEnumerable<JobTitle> SearchFor(string SearchTermn);
    }
}
