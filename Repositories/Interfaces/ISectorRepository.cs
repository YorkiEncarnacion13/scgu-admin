﻿using Models.Users;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories.Interfaces
{
   public interface ISectorRepository
    {
        Task<IEnumerable<Sector>> GetAllAsync();
        IEnumerable<Sector> GetAll();
        IEnumerable<Sector> GetMunicipalitySectors(int MunicipalityID);
    }
}
