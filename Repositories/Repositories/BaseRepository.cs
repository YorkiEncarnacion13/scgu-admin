﻿using AuthServer.Interfaces;
using ERP.DAL.Contexts.AuthServer;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public class BaseRepository<T> : IRepository<T> where T : class
    {
        private AuthContext context;
        private DbSet<T> entitySet;
        public BaseRepository(AuthContext Context)
        {
            context = Context;
            entitySet = context.Set<T>();
        }

        public virtual void Delete(object Id)
        {
            T entityToDelete = entitySet.Find(Id);
            Delete(entityToDelete);
        }
        public virtual async Task DeleteAsync(object Id)
        {
            T entityToDelete = entitySet.Find(Id);
            await DeleteAsync(entityToDelete);
        }

        public virtual void Delete(T entity)
        {
            if (context.Entry(entity).State == EntityState.Detached)
                entitySet.Attach(entity);
            entitySet.Remove(entity);
            context.SaveChanges();
        }

        public virtual T FindById(object key)
        {
            var res = entitySet.Find(key);
            return res;
        }
        public virtual async Task<T> FindByIdAsync(object key)
        {
            var res = await entitySet.FindAsync(key);
            return res;
        }
        public virtual async Task DeleteAsync(T entity)
        {
            if (context.Entry(entity).State == EntityState.Detached)
                entitySet.Attach(entity);

            entitySet.Remove(entity);
            await context.SaveChangesAsync();
        }

        public virtual T Insert(T entity)
        {
            entitySet.Add(entity);
            context.SaveChanges();
            return entity;
        }

        public virtual async Task<T> InsertAsync(T entity)
        {
            entitySet.Add(entity);
            await context.SaveChangesAsync();
            return entity;
        }

        public virtual T Update(T entity)
        {
            entitySet.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
            return entity;
        }

        public virtual async Task<T> UpdateAsync(T entity)
        {
            entitySet.Attach(entity);
            context.Entry(entity).State = EntityState.Modified;
            await context.SaveChangesAsync();
            return entity;
        }
    }
}
