﻿using ERP.DAL.Contexts.AuthServer;
using Microsoft.EntityFrameworkCore;
using Models.Auth;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Brawn.Helpers;

namespace Repositories.Repositories
{
    public class DepartmentRepository : BaseRepository<Department>, IDepartmentRepository
    {
        private AuthContext Context;
        public DepartmentRepository(AuthContext context):base(context) => Context = context ?? throw new ArgumentNullException("context", "The context object cannot be null.");

        public override void Delete(Department entity)
        {
            Context.Departments.RemoveRange(Context.Departments.Where(d => d.DepartmentID == entity.DepartmentID));
            Context.SaveChanges();
        }

        public override async Task DeleteAsync(Department entity)
        {
            Context.Departments.RemoveRange(Context.Departments.Where(d => d.DepartmentID == entity.DepartmentID));
            await Context.SaveChangesAsync();
        }

        public IEnumerable<Department> GetAll() => Context.Departments.ToArray();

        public async Task<IEnumerable<Department>> GetAllAsync() => await Context.Departments.ToArrayAsync();

        public IEnumerable<Department> SearchFor(string SearchTerm)
        {
            var Param = new SqlParameter("SearchTerm", SearchTerm.AsFullTextSearchTerm());
            var Results = Context.Departments.FromSql("SELECT TOP(10) * FROM Departments WHERE CONTAINS([DepartmentName],@SearchTerm)", Param).ToArray();
            return Results;
        }
    }
}
