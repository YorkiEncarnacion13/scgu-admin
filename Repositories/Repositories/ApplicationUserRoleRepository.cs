﻿using ERP.DAL.Contexts.AuthServer;
using Microsoft.EntityFrameworkCore;
using Models.System;
using Repositories.Interfaces;
using System;
using System.Linq;

namespace Repositories.Repositories
{
    public class ApplicationUserRoleRepository : BaseRepository<ApplicationUserRole>, IApplicationUserRoleRepository
    {
        private AuthContext Context;
        public ApplicationUserRoleRepository(AuthContext context) : base(context) => Context = context ?? throw new ArgumentNullException("context", "The context object cannot be null.");
        public async System.Threading.Tasks.Task TurnOnRolesAsync(ApplicationUser applicationUser, params int[] RolesIDs)
        {
            if (RolesIDs == null) throw new ArgumentNullException("RolesIDs", "The RolesIDs object cannot be null.");

            if (RolesIDs.Length < 1) throw new ArgumentException("RolesIDs cannot be empty.", "RolesIDs");

            var Roles = await Context.UserRoles.Where(r => RolesIDs.Contains(r.UserRoleID) && r.ApplicationID == applicationUser.ApplicationID).ToListAsync();

            var NewUserRoles = Roles.Select(ur => new ApplicationUserRole
            {
                ApplicationUser = applicationUser,
                Role = ur,
                RoleAssignmentDate = DateTime.UtcNow,
            }).ToList();

            using (var Tran = await Context.Database.BeginTransactionAsync())
            {
                Context.ApplicationUserRoles.AddRange(NewUserRoles);
                await Context.SaveChangesAsync();
                Tran.Commit();
            }
        }
        public async System.Threading.Tasks.Task TurnOffRolesAsync(ApplicationUser applicationUser, params int[] RolesIDs)
        {
            using (var Transaction = await Context.Database.BeginTransactionAsync())
            {
                var predicate =
                       Context.ApplicationUserRoles
                              .Where(ur => ur.ApplicationUser == applicationUser
                              && RolesIDs.Contains(ur.Role.UserRoleID)
                              );

                Context.ApplicationUserRoles.RemoveRange(predicate);
                await Context.SaveChangesAsync();
                Transaction.Commit();
            }
        }
    }
}