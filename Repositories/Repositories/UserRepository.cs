﻿using ERP.DAL.Contexts.AuthServer;
using Microsoft.EntityFrameworkCore;
using Models.Auth;
using Repositories.DTOS.Creates;
using Repositories.DTOS.Gets;
using Repositories.DTOS.Results;
using Repositories.Interfaces;
using Repositories.MISC;
using Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Brawn.Helpers;

namespace AuthServer.Repositories
{

    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        private AuthContext _context;
        public UserRepository(AuthContext context) : base(context)
        {
            _context = context ?? throw new ArgumentNullException("context", "The context object cannot be null.");
        }

        private static AuthContext CreateContext()
        {
            AuthContext authContext = new AuthContext();
            return authContext;
        }
        public UserRepository() : base(CreateContext())
        {
            _context = new AuthContext();

        }
        public async Task<User> FindByEmailAsync(string email)
        {
            return await _context.Users.Where(u => u.Email == email).FirstOrDefaultAsync();
        }
        public User FindByUserID(string subjectId)
        {
            var User = _context.Users.Where(u => u.UserID == subjectId).FirstOrDefault();
            return User;
        }

        private User ParseCreateDTO(UserCreateDTO UserDTO)
        {
            return new User
            {
                BirthDate = UserDTO.BirthDate,
                Email = UserDTO.Email,
                FirstLastName = UserDTO.FirstLastName,
                FirstName = UserDTO.FirstName,
                Gender = UserDTO.Gender,
                SecondLastName = UserDTO.SecondLastName,
                SecondName = UserDTO.SecondName,
                isServerAdmin = UserDTO.isServerAdmin,
                Status = UserDTO.Status,
                UserID = UserDTO.Cedula,
                Municipality = UserDTO.Municipality == null ? null : _context.Municipalities.Find(UserDTO.Municipality.MunicipalityID),
                Sector = UserDTO.Sector == null ? null : _context.Sectors.Find(UserDTO.Sector.SectorID),
                Province = UserDTO.Province == null ? null : _context.Provinces.Find(UserDTO.Province.ProvinceID),
                Address = UserDTO.Address,
                JobTitle = UserDTO.JobTitle == null ? null : _context.JobTitles.Find(UserDTO.JobTitle.JobTitleID),
                Location = UserDTO.Location == null ? null : _context.Locations.Find(UserDTO.Location.LocationID),
                Department = (UserDTO.Department == null) ? null : _context.Departments.Find(UserDTO.Department.DepartmentID),
                BirthCountry = (UserDTO.BirthCountry == null) ? null : _context.Countries.Find(UserDTO.BirthCountry.CountryIso2Code),
                FullName = string.Join(" ", new string[] { UserDTO.FirstName, UserDTO.SecondName, UserDTO.FirstLastName, UserDTO.SecondLastName }.Where(c => string.IsNullOrEmpty(c) == false))
            };
        }

        public async Task ChangePasswordAsync(User user, string NewPassword)
        {
            var PasswordsSet = PasswordsMannager.GenerateCredentials(Constants.Peppers, NewPassword);
            user.Password = PasswordsSet.Password;
            user.SecurityKey = PasswordsSet.Salt;
            await UpdateAsync(user);
        }
        public void  ChangePassword(User user, string NewPassword)
        {
            var PasswordsSet = PasswordsMannager.GenerateCredentials(Constants.Peppers, NewPassword);
            user.Password = PasswordsSet.Password;
            user.SecurityKey = PasswordsSet.Salt;
            Update(user);
        }

        public UserCreationResult CreateUser(UserCreateDTO UserDTO)
        {
            if (UserDTO == null) throw new ArgumentNullException("UserDTO", "The Provided UsersCreateDTO must not be null.");

            var ParsedUser = ParseCreateDTO(UserDTO);

            var NewUserPassword = PasswordsMannager.GenerateRandomPassword(length: 8);

            var PasswordsSet = PasswordsMannager.GenerateCredentials(Constants.Peppers, NewUserPassword);

            ParsedUser.Password = PasswordsSet.Password;

            ParsedUser.SecurityKey = PasswordsSet.Salt;

            ParsedUser.Status = UserStatuses.PendingPasswordReset;

            return new UserCreationResult
            {
                ClearTextPassword = NewUserPassword,

                CreatedUser = Insert(ParsedUser)
            };
        }
        public bool ValidateCredentials(string identifier, string password)
        {
            var StoredUserPassword = _context.Users
                                             .Where(u => u.Email == identifier || u.UserID == identifier)
                                             .Select(p => new PasswordsMannager.UserCredentials
                                             {
                                                 Password = p.Password,
                                                 Salt = p.SecurityKey
                                             }).FirstOrDefault();

            if (StoredUserPassword == null) return false;

            var isValid = PasswordsMannager.CheckRawPassword(password, StoredUserPassword.Password, StoredUserPassword.Salt, Constants.Peppers);

            return isValid;
        }
        public async Task<User> FindByUserIDAsync(string ID, bool IncludeDepartment = false)
        {
            var Queryable = _context.Users.Where(u => u.UserID == ID);

            if (IncludeDepartment)
                Queryable = Queryable.Include(u => u.Department);

            var User = await Queryable.FirstOrDefaultAsync();

            return User;
        }
        public override void Delete(User entity)
        {
            _context.Users.RemoveRange(_context.Users.Where(u => u.UserID == entity.UserID));
            _context.SaveChanges();
        }
        public User FindByEmail(string email)
        {
            var user = _context.Users.Where(u => u.Email == email).FirstOrDefault();
            return user;
        }

        public bool Exists(string ID) => _context.Users.Any(u => u.UserID == ID);

        public bool ExistsByEmail(string Email) => _context.Users.Any(u => u.Email == Email);

        public UserDTO GetUserDTO(string ID)
        {
            var user = _context.Users.Where(u => u.UserID == ID)
                   .Select(u => new UserDTO
                   {
                       FirstName = u.FirstName,
                       FirstLastName = u.FirstLastName,
                       UserID = ID,
                       SecondLastName = u.SecondLastName,
                       SecondName = u.SecondName,
                       DepartmentID = u.Department.DepartmentID,
                       DepartmentName = u.Department.DepartmentName,
                       JobTitleID = u.JobTitle.JobTitleID,
                       JobTitleName = u.JobTitle.JobTitleName,
                       Email = u.Email
                   }).FirstOrDefault();

            return user;
        }

        public async Task<User> FindByEmailOrIDAsync(string identifier)
        {
            var user = await _context.Users.Where(u => u.Email == identifier || u.UserID == identifier).FirstOrDefaultAsync();
            return user;
        }
        public User FindByEmailOrID(string identifier)
        {
            var user =  _context.Users.Where(u => u.Email == identifier || u.UserID == identifier).FirstOrDefault();
            return user;
        }
        public async Task<IList<UserListDTO>> GetUserAsync(string SearchTerm)
        {

            SqlParameter[] Params = new SqlParameter[]
            {
                new SqlParameter("SearchTerm",SearchTerm),
                new SqlParameter("FullTextSearchTerm", SearchTerm.AsFullTextSearchTerm())
            };

            string TSQL = @"
SELECT USERS.*, D.DepartmentName, J.JobTitleName
 FROM  (
 SELECT TOP(10) 
	   [UserID] 
      ,[DepartmentID]
      ,[Email]
      ,[FirstLastName]
      ,[FirstName]     
      ,[JobTitleID]   
      ,[Status]
      ,[isServerAdmin]
      ,[FullName] 
	  FROM [Users] WHERE CONTAINS([FullName],@SearchTerm) OR [UserID] LIKE @SearchTerm+'%' OR [Email] LIKE @SearchTerm+'%'
 ) USERS
  LEFT JOIN [dbo].[Departments] D 
 ON D.DepartmentID = USERS.[DepartmentID]
 JOIN [dbo].[JobTitles] J
 ON J.JobTitleID = USERS.[JobTitleID]
";

            var Result = await _context.Database.SqlQuery<UserListDTO>(TSQL, Params).ToListAsync();

            return Result;
            //var Results = await _context.Database.
            //return Results;
        }
    }
}
