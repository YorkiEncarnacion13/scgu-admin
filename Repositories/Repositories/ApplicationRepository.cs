﻿using AuthServer.Interfaces;
using ERP.DAL.Contexts.AuthServer;
using Microsoft.EntityFrameworkCore;
using Models.System;
using Repositories.DTOS.Creates;
using Repositories.DTOS.Gets;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repositories.Repositories
{

    public class ApplicationRepository : BaseRepository<Application>, IApplicationRepository
    {
        private AuthContext Context;
        public ApplicationRepository(AuthContext context) : base(context)
        {
            this.Context = context ?? throw new ArgumentNullException("context", "The context object cannot be null.");
        }

        public async Task<Application> CreateFromDTOAsync(ApplicationCreateDTO createDTO)
        {
            using (var Transaction = Context.Database.BeginTransaction())
            {
                var newApp = new Application
                {
                    ApplicationID = createDTO.ApplicationName,
                    Description = createDTO.Description,
                    IntroductionDate = DateTime.UtcNow,
                    Uri = createDTO.Uri,
                    PostLogoutRedirectUri = createDTO.PostLogoutRedirectUri,
                    RedirectUri = createDTO.RedirectUri,
                    RequiresConcent = createDTO.RequiresConcent,
                    ApplicationStatus = createDTO.isActive ? ApplicationStatuses.online : ApplicationStatuses.offline
                };
                Context.Applications.Add(newApp);
                await Context.SaveChangesAsync();
                if (createDTO.UserRoles?.Count > 0)
                {
                    newApp.UserRoles = createDTO.UserRoles.Select(role => new UserRole
                    {
                        Description = role.Description,
                        UserRoleName = role.Name
                    }).ToList();
                    await Context.SaveChangesAsync();
                }

                Transaction.Commit();
                return newApp;
            }
        }

        public override void Delete(Application entity)
        {
            Context.Applications.RemoveRange(Context.Applications.Where(app => app.ApplicationID == entity.ApplicationID));
            Context.SaveChanges();
        }

        public async Task<IEnumerable<Application>> GetList(int page, int NumberOfElementsPerPage = 10)
        {
            page = page - 1;
            var results = await Context.Applications.OrderByDescending(p => p.ApplicationID)
                                                    .Skip(NumberOfElementsPerPage * page)
                                                    .Take(NumberOfElementsPerPage)
                                                    .ToListAsync();
            return results;
        }

        public async Task<Application> GetByIdAsync(string ID)
        {
            var app = await Context.Applications
                .Include(c => c.UserRoles)
                .Where(a => a.ApplicationID == ID)
                .FirstOrDefaultAsync();
            return app;
        }

        public Application GetById(string ID)
        {
            var app = Context.Applications
               .Include(c => c.UserRoles)
               .Where(a => a.ApplicationID == ID)
               .FirstOrDefault();
            return app;
        }
        public IEnumerable<Application> SearchFor(Func<Application, bool> predicate)
        {
            var Queryable = Context.Applications.Where(predicate);
            return Queryable;
        }
        async Task IRepository<Application>.DeleteAsync(Application entity)
        {
            Context.Applications.RemoveRange(Context.Applications.Where(app => app.ApplicationID == entity.ApplicationID));
            await Context.SaveChangesAsync();
        }

        public async Task<AppViewModel> GetAppViewModel(string ID)
        {

            AppViewModel vm = new AppViewModel
            {
                Aplication = await Context.Applications
                                          .Include(c => c.UserRoles)
                                          .Where(a => a.ApplicationID == ID)
                                          .Select(app => new Application
                                          {
                                              ApplicationID = app.ApplicationID,

                                              ApplicationStatus = app.ApplicationStatus,
                                              AppUsers = null,
                                              Description = app.Description,
                                              GrantTypes = app.GrantTypes,
                                              IconUrl = app.IconUrl,
                                              IntroductionDate = app.IntroductionDate,
                                              PostLogoutRedirectUri = app.PostLogoutRedirectUri,
                                              RedirectUri = app.RedirectUri,
                                              Uri = app.Uri,
                                              UserRoles = app.UserRoles.Select(r => new UserRole
                                              {
                                                  UserRoleName = r.UserRoleName,
                                                  UserRoleID = r.UserRoleID,
                                                  Description = r.Description

                                              }).ToList(),
                                              UsersCount = app.UsersCount,

                                          }).FirstOrDefaultAsync(),
                users = await Context.ApplicationUsers.Where(p => p.ApplicationID == ID).OrderByDescending(c => c.RegistrationDate).Take(10).Select(d => new
                {
                    d.ApplicationUserID,
                    d.AppUserStatus,
                    d.User.FirstName,
                    d.User.FirstLastName,
                    d.User.SecondName,
                    d.User.SecondLastName,
                    d.User.Email,
                    UserRoles = d.ApplicationUserRoles.Select(r => new
                    {
                        ApplicationUserRoleID = r.ApplicationUserRoleID,
                        Role = new
                        {
                            UserRoleName = r.Role.UserRoleName,
                            UserRoleID = r.Role.UserRoleID
                        }
                    }).ToList(),
                    d.User.UserID,
                    d.User.JobTitle.JobTitleName,
                    d.User.Department.DepartmentName
                }).ToListAsync()
            };
            return vm;
        }

        public async Task CreateApplicationUserFromDTO(NewApplicationUserDTO NewAppUserDTO)
        {
            using (var Transaction = await Context.Database.BeginTransactionAsync())
            {
                var newUser = new ApplicationUser
                {
                    ApplicationID = NewAppUserDTO.ApplicationID,
                    UserID = NewAppUserDTO.UserID,
                    RegistrationDate = DateTime.UtcNow,
                    AppUserStatus = ApplicationUserStatus.Normal
                };

                Context.ApplicationUsers.Add(newUser);

                var app = await Context.Applications
                                       .Where(ap => ap.ApplicationID == NewAppUserDTO.ApplicationID)
                                       .FirstOrDefaultAsync();

                app.UsersCount = app.UsersCount + 1;

                await Context.SaveChangesAsync();

                foreach (int RoleID in NewAppUserDTO.UserRolesIds)
                {
                    var AppUserRole = new ApplicationUserRole
                    {
                        ApplicationUser = newUser,
                        Role = Context.UserRoles.Where(r => r.ApplicationID == NewAppUserDTO.ApplicationID && r.UserRoleID == RoleID).FirstOrDefault(),
                        RoleAssignmentDate = DateTime.UtcNow
                    };
                    Context.ApplicationUserRoles.Add(AppUserRole);
                }
                await Context.SaveChangesAsync();
                Transaction.Commit();
            }
        }

        public async Task<AppManifest> GetManifest(string ID)
        {
            var Manifest = await Context.Applications.Where(app => app.ApplicationID == ID).Select(d => new AppManifest
            {
                ClientId = ID,
                ClientName = d.Description,
                Roles = d.UserRoles.Select(r => new AppManifestRoles
                {
                    RoleID = r.UserRoleID,
                    RoleName = r.UserRoleName
                }).ToList()
            })
            .FirstOrDefaultAsync();
            return Manifest;
        }
    }
}
