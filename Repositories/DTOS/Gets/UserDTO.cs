﻿namespace Repositories.DTOS.Gets
{
    public class UserDTO
    {
        public string UserID { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string FirstLastName { get; set; }
        public string SecondLastName { get; set; }
        public int JobTitleID { get; set; }
        public string JobTitleName { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string Email { get; set; }

    }
}
