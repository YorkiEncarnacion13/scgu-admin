﻿using Models.Auth;

namespace Repositories.DTOS.Gets
{
    public class UserListDTO
    {
        public string UserID { get; set; }
        public int DepartmentID { get; set; }
        public int JobTitleID { get; set; }
        public string Email { get; set; }
        public string FirstLastName { get; set; }
        public string FirstName { get; set; }
        public string FullName { get; set; }
        public bool isServerAdmin { get; set; }
        public string DepartmentName { get; set; }
        public string JobTitleName { get; set; }
        public UserStatuses Status { get; set; }
    }
}
