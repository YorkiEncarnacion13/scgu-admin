﻿using System.Collections.Generic;

namespace Repositories.DTOS.Creates
{
    public class ApplicationCreateDTO
    {
        public string ApplicationName { get; set; }
        public string Description { get; set; }
        public bool isActive { get; set; }
        public List<UserRolesCreateDTO> UserRoles { get; set; }
        public List<int> UserAdmins { get; set; }
        public string Uri { get; set; }
        public string RedirectUri { get; set; }
        public string PostLogoutRedirectUri { get; set; }
        public bool RequiresConcent { get; set; }
    }
}
