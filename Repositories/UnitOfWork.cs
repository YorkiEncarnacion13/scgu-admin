﻿using AuthServer.Repositories;
using ERP.DAL.Contexts.AuthServer;
using Models.System;
using Repositories.Repositories;
using System;

namespace ERP.DAL.Repositories
{
    public class UnitOfWork : IDisposable
    {
        private AuthContext _context;
        private AuthContext context
        {
            get
            {
                if (_context == null) _context = new AuthContext();

                return _context;
            }
        }
        private BaseRepository<UserRole> userRoleRepository;
        private ApplicationUserRepository applicationUserRepository;
        private ApplicationUserRoleRepository applicationUserRole;
        private ApplicationRepository applicationRepository;
        private UserRepository userRepository;
        private ProvinceRepository provinceRepository;
        private MunicipalityRepository municipalityRepository;
        private SectorRepository sectorRepository;
        private CountryRepository countryRepository;
        private JobTitleRepository jobTitleRepository;
        private DepartmentRepository departmentRepository;
        private LocationRepository locationRepository;
        private UserPhoneRepository userPhoneRepository;
        public BaseRepository<UserRole> UserRoleRepository
        {
            get
            {
                if (userRoleRepository == null) userRoleRepository = new BaseRepository<UserRole>(context);
                return userRoleRepository;
            }
        }
        public ApplicationUserRepository ApplicationUserRepository
        {
            get
            {
                if (applicationUserRepository == null) applicationUserRepository = new ApplicationUserRepository(context);
                return applicationUserRepository;
            }
        }
        public ApplicationUserRoleRepository ApplicationUserRoleRepository
        {
            get
            {
                if (applicationUserRole == null) applicationUserRole = new ApplicationUserRoleRepository(context);
                return applicationUserRole;
            }
        }
        public UserPhoneRepository UserPhoneRepository
        {
            get
            {
                if (userPhoneRepository == null) userPhoneRepository = new UserPhoneRepository(context);
                return userPhoneRepository;
            }
        }
        public LocationRepository LocationRepository
        {
            get
            {
                if (locationRepository == null) locationRepository = new LocationRepository(context);
                return locationRepository;
            }
        }
        public DepartmentRepository DepartmentRepository
        {
            get
            {
                if (departmentRepository == null) departmentRepository = new DepartmentRepository(context);
                return departmentRepository;
            }
        }
        public JobTitleRepository JobTitleRepository
        {
            get
            {
                if (jobTitleRepository == null) jobTitleRepository = new JobTitleRepository(context);
                return jobTitleRepository;
            }
        }
        public CountryRepository CountryRepository
        {
            get
            {
                if (countryRepository == null) countryRepository = new CountryRepository(context);
                return countryRepository;
            }
        }
        public SectorRepository SectorRepository
        {
            get
            {
                if (sectorRepository == null) sectorRepository = new SectorRepository(context);
                return sectorRepository;
            }
        }
        public MunicipalityRepository MunicipalityRepository
        {
            get
            {
                if (municipalityRepository == null) municipalityRepository = new MunicipalityRepository(context);
                return municipalityRepository;
            }
        }
        public ProvinceRepository ProvinceRepository
        {
            get
            {
                if (provinceRepository == null)
                    provinceRepository = new ProvinceRepository(context);
                return provinceRepository;
            }
        }
        public UserRepository UsersRepository
        {
            get
            {
                if (userRepository == null) userRepository = new UserRepository(context);
                return userRepository;
            }
        }
        public ApplicationRepository ApplicationRepository
        {
            get
            {
                if (applicationRepository == null) applicationRepository = new ApplicationRepository(context);
                return applicationRepository;
            }
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
