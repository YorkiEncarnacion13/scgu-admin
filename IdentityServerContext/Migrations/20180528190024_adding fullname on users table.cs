﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace IdentityServerContext.Migrations
{
    public partial class addingfullnameonuserstable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "Users",
                maxLength: 420,
                nullable: true);
            migrationBuilder.Sql(@"
              CREATE FULLTEXT CATALOG USERS_FT; 
              CREATE FULLTEXT INDEX ON [Users]([FullName])
              KEY INDEX  [PK_Users]
              ON USERS_FT WITH CHANGE_TRACKING AUTO;", true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullName",
                table: "Users");
        }
    }
}
