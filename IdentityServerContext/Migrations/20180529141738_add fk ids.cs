﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace IdentityServerContext.Migrations
{
    public partial class addfkids : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Municipalities_MunicipalityID",
                table: "Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Provinces_ProvinceID",
                table: "Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Sectors_SectorID",
                table: "Users");

            migrationBuilder.AlterColumn<int>(
                name: "SectorID",
                table: "Users",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ProvinceID",
                table: "Users",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MunicipalityID",
                table: "Users",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Municipalities_MunicipalityID",
                table: "Users",
                column: "MunicipalityID",
                principalTable: "Municipalities",
                principalColumn: "MunicipalityID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Provinces_ProvinceID",
                table: "Users",
                column: "ProvinceID",
                principalTable: "Provinces",
                principalColumn: "ProvinceID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Sectors_SectorID",
                table: "Users",
                column: "SectorID",
                principalTable: "Sectors",
                principalColumn: "SectorID",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Municipalities_MunicipalityID",
                table: "Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Provinces_ProvinceID",
                table: "Users");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Sectors_SectorID",
                table: "Users");

            migrationBuilder.AlterColumn<int>(
                name: "SectorID",
                table: "Users",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ProvinceID",
                table: "Users",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "MunicipalityID",
                table: "Users",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Municipalities_MunicipalityID",
                table: "Users",
                column: "MunicipalityID",
                principalTable: "Municipalities",
                principalColumn: "MunicipalityID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Provinces_ProvinceID",
                table: "Users",
                column: "ProvinceID",
                principalTable: "Provinces",
                principalColumn: "ProvinceID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Sectors_SectorID",
                table: "Users",
                column: "SectorID",
                principalTable: "Sectors",
                principalColumn: "SectorID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
