﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.Extensions.DependencyInjection;
using Repositories.DTOS.Gets;
using System;

namespace AuthServer.Core
{
    public static class Extensions
    {
        public static AuthenticationBuilder AddSCGUAuth(this AuthenticationBuilder builder, string authenticationScheme, Action<OpenIdConnectOptions> configureOptions, AppManifest Manifest)
        {
            builder.AddOpenIdConnect("oidc", options =>
             {
                 options.SignInScheme = "Cookies";
                 options.Authority = "http://localhost:5000";
                 options.RequireHttpsMetadata = false;
                 options.ClientId = Manifest.ClientId;
                 options.SaveTokens = true;
             });
            return builder;
        }
    }
}