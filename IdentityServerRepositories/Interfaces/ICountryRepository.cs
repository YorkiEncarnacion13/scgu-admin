﻿using AuthServer.Interfaces;
using Models.Auth;
using Repositories.DTOS.Gets;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories.Interfaces
{
    public interface ICountryRepository : IRepository<Country>
    {
        Task<IEnumerable<Country>> GetAllAsync();
        IEnumerable<Country> GetAll();
        IEnumerable<CountryDTO> GetCountriesDTOS();
        Task<IEnumerable<CountryDTO>> GetCountriesDTOSAsync();
    }
}
