﻿using AuthServer.Interfaces;
using Models.Auth;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories.Interfaces
{
    public interface IDepartmentRepository : IRepository<Department>
    {
        IEnumerable<Department> GetAll();
        Task<IEnumerable<Department>> GetAllAsync();
        IEnumerable<Department> SearchFor(string SearchTerm);
    }
}
