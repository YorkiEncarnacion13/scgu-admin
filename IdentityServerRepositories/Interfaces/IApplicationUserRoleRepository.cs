﻿using Models.System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories.Interfaces
{
    public interface IApplicationUserRoleRepository
    {
        Task TurnOffRolesAsync(ApplicationUser applicationUser, params int[] RolesIDs);
        Task TurnOnRolesAsync(ApplicationUser applicationUser, params int[] RolesIDs);
        Task<List<AplicationUserRoleDTO>> GetRoles(string UserID, string ApplicationID);
        Task<bool> HasRole(string AppID, string UserID, int RoleID);
    }
}
