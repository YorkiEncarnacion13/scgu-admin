﻿using System.Threading.Tasks;

namespace AuthServer.Interfaces
{
    public interface IRepository<T>
    {
        Task<T> InsertAsync(T entity);
        Task<T> UpdateAsync(T entity);
        Task DeleteAsync(T entity);
        T Insert(T entity);
        void Delete(T entity);
        T Update(T entity);
        void SaveChanges();
        Task SaveChangesAsync();
    }
}
