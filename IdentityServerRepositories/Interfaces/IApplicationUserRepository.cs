﻿using Models.System;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Repositories.Interfaces
{
    public class AplicationUserRoleDTO
    {
        public int RoleID { get; set; }
        public string AppID { get; set; }
        public string RoleName { get; set; }
    }
    interface IApplicationUserRepository
    {
        Task<ApplicationUser> GetAsync(string ApplicationID, string UserID);
        Task<ApplicationUser> GetAsync(int ApplicationUserID);

        Task<List<AplicationUserRoleDTO>> GetUsersRoles(string UserID);

    }
}
