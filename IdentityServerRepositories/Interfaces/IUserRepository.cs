﻿using AuthServer.Interfaces;
using IdentityServerRepositories.DTOS.Results;
using Models.Auth;
using Repositories.DTOS.Creates;
using Repositories.DTOS.Gets;
using Repositories.DTOS.Results;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        User FindByUserID(string ID);
        User FindByEmail(string email);
        Task<User> FindByEmailOrIDAsync(string identifier);
        Task<User> FindByUserIDAsync(string ID, bool IncludeDepartment = false);
        bool ValidateCredentials(string identifier, string password);
        Task<User> FindByEmailAsync(string email);
        UserCreationResult CreateUser(UserCreateDTO UserDTO);
        UserDTO GetUserDTO(string ID);
        bool Exists(string ID);
        bool ExistsByEmail(string Email);
        Task<IList<UserGetDTO>> GetUser(string SearchTerm);
        Task<VirtualCollectionResult<UserGetDTO>> GetUsersFilteredAsync(int Page, int Qntity, string Filter);
        Task<VirtualCollectionResult<UserGetDTO>> GetUsersAsync(int Page, int Qntity);
        Task<IEnumerable<UserGetDTO>> GetUsersList(List<string> IDs);
    }
}
