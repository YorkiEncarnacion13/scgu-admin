﻿using AuthServer.Interfaces;
using Models.System;
using Repositories.DTOS.Creates;
using Repositories.DTOS.Gets;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories.Interfaces
{
    public class GetApplicationResult
    {
        public IEnumerable<Application> Data { get; set; }
        public int Qntity { get; set; }
    }

    public interface IApplicationRepository : IRepository<Application>
    {
        Task<Application> CreateFromDTOAsync(ApplicationCreateDTO createDTO);
        Task<Application> GetByIdAsync(string ID);
        //Task<IEnumerable<Application>> GetList(int page, int NumberOfElementsPerPage);
        Task<GetApplicationResult> GetList(int page, int NumberOfElementsPerPage);
        Task CreateApplicationUserFromDTO(NewApplicationUserDTO NewAppUserDTO);
        Task<AppViewModel> GetAppViewModel(string ID);
        Task<AppManifest> GetManifest(string ID);
        Task<IEnumerable<UserGetDTO>> GetApplicationUsers(string ApplicationID);
        Task<IEnumerable<UserGetDTO>> GetApplicationUsersByRole(string ApplicationID, int RoleID);
        Task<ApplicationStatuses> GetApplicationStatus(string ApplicationID);
        Task<string> GetApplicationDisplayNameAsync(string ApplicationID);
    }
}
