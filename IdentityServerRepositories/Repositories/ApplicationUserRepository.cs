﻿using ERP.DAL.Contexts.AuthServer;
using Microsoft.EntityFrameworkCore;
using Models.System;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public class ApplicationUserRepository : BaseRepository<ApplicationUser>, IApplicationUserRepository
    {
        
        private AuthContext Context;
        public ApplicationUserRepository(AuthContext context) : base(context) => Context = context ?? throw new ArgumentNullException("context", "The context object cannot be null.");

        public async System.Threading.Tasks.Task<ApplicationUser> GetAsync(string ApplicationID, string UserID)
        {
            var appuser = await Context.ApplicationUsers.Where(u => u.ApplicationID == ApplicationID && u.UserID == UserID).FirstOrDefaultAsync();
            return appuser;
        }
        public async System.Threading.Tasks.Task<ApplicationUser> GetAsync(int ApplicationUserID)
        {
            var AppUser = await Context.ApplicationUsers.Where(ap => ap.ApplicationUserID == ApplicationUserID).FirstOrDefaultAsync();
            return AppUser;
        }

        public async Task<List<AplicationUserRoleDTO>> GetUsersRoles(string UserID)
        {
            var Result = await Context.ApplicationUserRoles.Where(au => au.ApplicationUser.User.UserID == UserID)
                .Select(ur => new AplicationUserRoleDTO
                {
                    AppID = ur.ApplicationUser.Application.ApplicationID,
                    RoleID = ur.Role.UserRoleID,

                }).ToListAsync();

            return Result;
        }
    }
}