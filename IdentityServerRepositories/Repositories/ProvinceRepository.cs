﻿using ERP.DAL.Contexts.AuthServer;
using Microsoft.EntityFrameworkCore;
using Models.Users;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public class ProvinceRepository : BaseRepository<Province>, IProvinceRepository
    {
        private AuthContext Context;
        public ProvinceRepository(AuthContext context) : base(context) => this.Context = context ?? throw new ArgumentNullException("context", "The context object cannot be null.");
        public IEnumerable<Province> GetAll() => Context.Provinces.OrderBy(p => p.ProvinceName).ToList();

        public async Task<IEnumerable<Province>> GetAllAsync() => await Context.Provinces.OrderBy(p=>p.ProvinceName)
            .Select(p => new Province
            {
                ProvinceID = p.ProvinceID,
                ProvinceName = p.ProvinceName,
            }).ToListAsync();
    }
}
