﻿using ERP.DAL.Contexts.AuthServer;
using Models.Auth;

namespace Repositories.Repositories
{
    public class UserPhoneRepository : BaseRepository<UserPhone>
    {
        public UserPhoneRepository(AuthContext Context) : base(Context)
        {
        }
    }
}
