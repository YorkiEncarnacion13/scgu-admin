﻿using ERP.DAL.Contexts.AuthServer;
using Models.Auth;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public class LocationRepository :BaseRepository<Location>, ILocationRepository
    {
        private AuthContext context;
        public LocationRepository(AuthContext Context):base(Context) => context = Context ?? throw new ArgumentNullException("Context", "The context object cannot be null.");

        public override void Delete(Location entity)
        {
            context.Locations.RemoveRange(context.Locations.Where(l => l.LocationID == entity.LocationID));
            context.SaveChanges();
        }

        public override async Task DeleteAsync(Location entity)
        {
            context.Locations.RemoveRange(context.Locations.Where(l => l.LocationID == entity.LocationID));
            await context.SaveChangesAsync();
        }

        public IEnumerable<Location> GetAll() => context.Locations.ToArray();

        public IEnumerable<Location> SearchFor(string SearchTermn) => context.Locations.Where(l => l.LocationName.Contains(SearchTermn)).Take(10).ToArray();

    }
}