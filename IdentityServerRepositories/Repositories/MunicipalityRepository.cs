﻿using ERP.DAL.Contexts.AuthServer;
using Models.Users;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public class MunicipalityRepository : BaseRepository<Municipality>, IMunicipalityRepository
    {
        private AuthContext Context;
        public MunicipalityRepository(AuthContext context): base(context) => this.Context = context ?? throw new ArgumentNullException("context", "The context object cannot be null.");
        public IEnumerable<Municipality> GetAll() => Context.Municipalities.ToList();

        public async Task<IEnumerable<Municipality>> GetAllAsync() => await Context.Municipalities.ToListAsync();

        public IEnumerable<Municipality> GetProvinceMunicipalities(int ProvinceID) => Context.Municipalities
            .Where(m => m.ProvinceID == ProvinceID)
            .Select(pm=>new Municipality
            {
                MunicipalityID = pm.MunicipalityID,
                MunicipalityName = pm.MunicipalityName,
                ProvinceID = pm.ProvinceID,
            })
            .ToArray();
    }
}
