﻿using ERP.DAL.Contexts.AuthServer;
using Models.Users;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public class SectorRepository : BaseRepository<Sector>, ISectorRepository
    {
        private AuthContext Context;
        public SectorRepository(AuthContext context) : base(context) => this.Context = context ?? throw new ArgumentNullException("context", "The context object cannot be null.");
        public IEnumerable<Sector> GetAll() => Context.Sectors.ToList();
        public async Task<IEnumerable<Sector>> GetAllAsync() => await Context.Sectors.OrderBy(s => s.SectorName).ToListAsync();
        public IEnumerable<Sector> GetMunicipalitySectors(int MunicipalityID) => Context.Sectors.Where(s => s.MunicipalityID == MunicipalityID).
           OrderBy(m => m.SectorName)
            .Select(m => new Sector
            {
                MunicipalityID = m.MunicipalityID,
                SectorID = m.SectorID,
                SectorName = m.SectorName
            }).ToArray();
    }
}