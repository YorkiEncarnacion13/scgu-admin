﻿using AuthServer.Interfaces;
using ERP.DAL.Contexts.AuthServer;
using Microsoft.EntityFrameworkCore;
using Models.System;
using Repositories.DTOS.Creates;
using Repositories.DTOS.Gets;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repositories.Repositories
{

    public class ApplicationRepository : BaseRepository<Application>, IApplicationRepository
    {
        private AuthContext context;
        public ApplicationRepository(AuthContext context) : base(context)
        {
            this.context = context ?? throw new ArgumentNullException("context", "The context object cannot be null.");
        }

        public async Task<Application> CreateFromDTOAsync(ApplicationCreateDTO createDTO)
        {
            using (var Transaction = context.Database.BeginTransaction())
            {

                if (createDTO.Uri.EndsWith("/") == false)
                    createDTO.Uri = createDTO.Uri + "/";



                var newApp = new Application
                {
                    ApplicationID = createDTO.ApplicationName,
                    DisplayName = createDTO.DisplayName,
                    Description = createDTO.Description,
                    IntroductionDate = DateTime.UtcNow,
                    Uri = createDTO.Uri,
                    PostLogoutRedirectUri = createDTO.Uri + "signout-callback-oidc",
                    RedirectUri = createDTO.Uri + "signin-oidc",
                    RequiresConcent = createDTO.RequiresConcent,
                    ApplicationStatus = createDTO.isActive ? ApplicationStatuses.online : ApplicationStatuses.offline
                };

                context.Applications.Add(newApp);

                await context.SaveChangesAsync();

                if (createDTO.UserRoles?.Count > 0)
                {
                    newApp.UserRoles = createDTO.UserRoles.Select(role => new UserRole
                    {
                        Description = role.Description,
                        UserRoleName = role.Name
                    }).ToList();
                    await context.SaveChangesAsync();
                }

                Transaction.Commit();
                return newApp;
            }
        }

        public override void Delete(Application entity)
        {
            context.Applications.RemoveRange(context.Applications.Where(app => app.ApplicationID == entity.ApplicationID));
            context.SaveChanges();
        }

        public async Task<GetApplicationResult> GetList(int page, int NumberOfElementsPerPage = 10)
        {
            page = page - 1;
            var results = await context.Applications.OrderByDescending(p => p.ApplicationID)
                                                    .Skip(NumberOfElementsPerPage * page)
                                                    .Take(NumberOfElementsPerPage)
                                                    .ToListAsync();

            var resultsTotal = context.Applications.Count();

            return new GetApplicationResult
            {
                Data = results,
                Qntity = resultsTotal
            };
        }

        public async Task<Application> GetByIdAsync(string ID)
        {
            var app = await context.Applications
                .Include(c => c.UserRoles)
                .Where(a => a.ApplicationID == ID)
                .FirstOrDefaultAsync();
            return app;
        }

        public Application GetById(string ID)
        {
            var app = context.Applications
               .Include(c => c.UserRoles)
               .Where(a => a.ApplicationID == ID)
               .FirstOrDefault();
            return app;
        }
        public IEnumerable<Application> SearchFor(Func<Application, bool> predicate)
        {
            var Queryable = context.Applications.Where(predicate);
            return Queryable;
        }
        async Task IRepository<Application>.DeleteAsync(Application entity)
        {
            context.Applications.RemoveRange(context.Applications.Where(app => app.ApplicationID == entity.ApplicationID));
            await context.SaveChangesAsync();
        }

        public async Task<AppViewModel> GetAppViewModel(string ID)
        {

            AppViewModel vm = new AppViewModel
            {
                Aplication = await context.Applications
                                          .Include(c => c.UserRoles)
                                          .Where(a => a.ApplicationID == ID)
                                          .Select(app => new Application
                                          {
                                              ApplicationID = app.ApplicationID,

                                              ApplicationStatus = app.ApplicationStatus,
                                              AppUsers = null,
                                              Description = app.Description,
                                              GrantTypes = app.GrantTypes,
                                              IconUrl = app.IconUrl,
                                              IntroductionDate = app.IntroductionDate,
                                              PostLogoutRedirectUri = app.PostLogoutRedirectUri,
                                              RedirectUri = app.RedirectUri,
                                              Uri = app.Uri,
                                              DisplayName = app.DisplayName,
                                              RequiresConcent = app.RequiresConcent,
                                              UserRoles = app.UserRoles.Select(r => new UserRole
                                              {
                                                  UserRoleName = r.UserRoleName,
                                                  UserRoleID = r.UserRoleID,
                                                  Description = r.Description

                                              }).ToList(),
                                              UsersCount = app.UsersCount,

                                          }).FirstOrDefaultAsync(),
                users = await context.ApplicationUsers.Where(p => p.ApplicationID == ID).OrderByDescending(c => c.RegistrationDate).Select(d => new
                {
                    d.ApplicationUserID,
                    d.AppUserStatus,
                    d.User.FirstName,
                    d.User.FirstLastName,
                    d.User.SecondName,
                    d.User.SecondLastName,
                    d.User.Email,
                    UserRoles = d.ApplicationUserRoles.Select(r => new
                    {
                        ApplicationUserRoleID = r.ApplicationUserRoleID,
                        Role = new
                        {
                            UserRoleName = r.Role.UserRoleName,
                            UserRoleID = r.Role.UserRoleID
                        }
                    }).ToList(),
                    d.User.UserID,
                    d.User.JobTitle.JobTitleName,
                    d.User.Department.DepartmentName
                }).ToListAsync()
            };
            return vm;
        }

        public async Task CreateApplicationUserFromDTO(NewApplicationUserDTO NewAppUserDTO)
        {
            if (await context.ApplicationUsers.AnyAsync(au => au.UserID == NewAppUserDTO.UserID && au.ApplicationID == NewAppUserDTO.ApplicationID))
                return;

            using (var Transaction = await context.Database.BeginTransactionAsync())
            {
                var newUser = new ApplicationUser
                {
                    ApplicationID = NewAppUserDTO.ApplicationID,
                    UserID = NewAppUserDTO.UserID,
                    RegistrationDate = DateTime.UtcNow,
                    AppUserStatus = ApplicationUserStatus.Normal
                };

                context.ApplicationUsers.Add(newUser);

                var app = await context.Applications
                                       .Where(ap => ap.ApplicationID == NewAppUserDTO.ApplicationID)
                                       .FirstOrDefaultAsync();

                app.UsersCount = app.UsersCount + 1;

                await context.SaveChangesAsync();

                foreach (int RoleID in NewAppUserDTO.UserRolesIds)
                {
                    var AppUserRole = new ApplicationUserRole
                    {
                        ApplicationUser = newUser,
                        Role = context.UserRoles.Where(r => r.ApplicationID == NewAppUserDTO.ApplicationID && r.UserRoleID == RoleID).FirstOrDefault(),
                        RoleAssignmentDate = DateTime.UtcNow
                    };
                    context.ApplicationUserRoles.Add(AppUserRole);
                }
                await context.SaveChangesAsync();
                Transaction.Commit();
            }
        }

        public async Task<AppManifest> GetManifest(string ID)
        {
            var Manifest = await context.Applications.Where(app => app.ApplicationID == ID).Select(d => new AppManifest
            {
                ClientId = ID,
                ClientName = d.Description,
                Roles = d.UserRoles.Select(r => new AppManifestRoles
                {
                    RoleID = r.UserRoleID,
                    RoleName = r.UserRoleName
                }).ToList()
            })
            .FirstOrDefaultAsync();
            return Manifest;
        }

        public async Task<IEnumerable<UserGetDTO>> GetApplicationUsers(string ApplicationID)
        {
            var results = await context.ApplicationUsers
                                       .Where(a => a.ApplicationID == ApplicationID)
                                       .Select(u => u.User)
                                       .Select(u => new UserGetDTO
                                       {
                                           DepartmentID = u.DepartmentID,
                                           DepartmentName = u.Department.DepartmentName,
                                           JobTitleID = u.JobTitleID,
                                           isServerAdmin = u.isServerAdmin,
                                           Email = u.Email,
                                           FirstLastName = u.FirstLastName,
                                           FirstName = u.FirstName,
                                           FullName = u.FullName,
                                           JobTitleName = u.JobTitle.JobTitleName,
                                           Status = u.Status,
                                           UserID = u.UserID
                                       }).ToArrayAsync();
            return results;
        }
        public async Task<IEnumerable<UserGetDTO>> GetApplicationUsersByRole(string ApplicationID, int RoleID)
        {
            var results = await context.ApplicationUserRoles
                                       .Where(a => a.Role.UserRoleID == RoleID && a.ApplicationUser.ApplicationID == ApplicationID)
                                       .Select(u => u.ApplicationUser.User)
                                       .Select(u => new UserGetDTO
                                       {
                                           DepartmentID = u.DepartmentID,
                                           DepartmentName = u.Department.DepartmentName,
                                           JobTitleID = u.JobTitleID,
                                           isServerAdmin = u.isServerAdmin,
                                           Email = u.Email,
                                           FirstLastName = u.FirstLastName,
                                           FirstName = u.FirstName,
                                           FullName = u.FullName,
                                           JobTitleName = u.JobTitle.JobTitleName,
                                           Status = u.Status,
                                           UserID = u.UserID
                                       }).ToArrayAsync();
            return results;
        }

        public async Task<ApplicationStatuses> GetApplicationStatus(string ApplicationID)
        {
            var status = await context.Applications.Where(a => a.ApplicationID == ApplicationID).Select(s => s.ApplicationStatus).FirstOrDefaultAsync();
            return status;
        }

        public async Task<string> GetApplicationDisplayNameAsync(string ApplicationID)
        {
            var displayName = await context.Applications.Where(c => c.ApplicationID == ApplicationID).Select(p => p.DisplayName).FirstOrDefaultAsync();
            return displayName;
        }
    }
}
