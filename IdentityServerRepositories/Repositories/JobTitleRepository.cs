﻿using ERP.DAL.Contexts.AuthServer;
using Microsoft.EntityFrameworkCore;
using Models.Auth;
using Repositories.Interfaces;
using Repositories.MISC;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Brawn.Helpers;
namespace Repositories.Repositories
{
    public class JobTitleRepository : BaseRepository<JobTitle>, IJobTitleRepository
    {
        private AuthContext Context;
        public JobTitleRepository(AuthContext context) : base(context) => Context = context ?? throw new ArgumentNullException("context", "The context object cannot be null.");

        public override void Delete(JobTitle entity)
        {
            if (entity == null) return;
            Context.JobTitles.RemoveRange(Context.JobTitles.Where(jt => jt.JobTitleID == entity.JobTitleID));
            Context.SaveChanges();
        }
        public override async Task DeleteAsync(JobTitle entity)
        {
            if (entity == null) return;
            Context.JobTitles.RemoveRange(Context.JobTitles.Where(jt => jt.JobTitleID == entity.JobTitleID));
            await Context.SaveChangesAsync();
        }
        public IEnumerable<JobTitle> GetAll() => Context.JobTitles.ToArray();
        public IEnumerable<JobTitle> SearchFor(string SearchTermn)
        {
            SqlParameter SearchTermParam = new SqlParameter("SearchTermn", SearchTermn.AsFullTextSearchTerm());
            var elements = Context.JobTitles
                                  .FromSql("SELECT TOP(10) * FROM JobTitles WHERE CONTAINS([JobTitleName], @SearchTermn)", SearchTermParam)
                                  .ToArray();
            return elements;
        }
    }
}