﻿using ERP.DAL.Contexts.AuthServer;
using Microsoft.EntityFrameworkCore;
using Models.Auth;
using Repositories.DTOS.Gets;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public class CountryRepository : BaseRepository<Country>, ICountryRepository
    {
        private AuthContext Context;
        public CountryRepository(AuthContext context) : base(context) => Context = context ?? throw new ArgumentNullException("context", "The context object cannot be null.");

        public override void Delete(Country entity)
        {
            if (entity == null) return;
            Context.Countries.RemoveRange(Context.Countries.Where(c => c.CountryIso2Code == entity.CountryIso2Code));
            Context.SaveChanges();
        }

        public override async Task DeleteAsync(Country entity)
        {
            if (entity == null) return;
            Context.Countries.RemoveRange(Context.Countries.Where(c => c.CountryIso2Code == entity.CountryIso2Code));
            await Context.SaveChangesAsync();
        }

        public IEnumerable<Country> GetAll() => Context.Countries.ToList();

        public IEnumerable<CountryDTO> GetCountriesDTOS() => Context.Countries.Select(c => new CountryDTO { CountryIso2Code = c.CountryIso2Code, CountryName = c.CountryName }).ToArray();

        public async Task<IEnumerable<Country>> GetAllAsync() => await Context.Countries.ToListAsync();

        public async Task<IEnumerable<CountryDTO>> GetCountriesDTOSAsync() => await Context.Countries.Select(c => new CountryDTO { CountryIso2Code = c.CountryIso2Code, CountryName = c.CountryName }).ToArrayAsync();
    }
}