﻿using ERP.DAL.Contexts.AuthServer;
using Microsoft.EntityFrameworkCore;
using Models.System;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public class ApplicationUserRoleRepository : BaseRepository<ApplicationUserRole>, IApplicationUserRoleRepository
    {
        private AuthContext Context;
        public ApplicationUserRoleRepository(AuthContext context) : base(context) => Context = context ?? throw new ArgumentNullException("context", "The context object cannot be null.");
        public async System.Threading.Tasks.Task TurnOnRolesAsync(ApplicationUser applicationUser, params int[] RolesIDs)
        {
            if (RolesIDs == null) throw new ArgumentNullException("RolesIDs", "The RolesIDs object cannot be null.");

            if (RolesIDs.Length < 1) throw new ArgumentException("RolesIDs cannot be empty.", "RolesIDs");

            var Roles = await Context.UserRoles.Where(r => RolesIDs.Contains(r.UserRoleID) && r.ApplicationID == applicationUser.ApplicationID).ToListAsync();

            var NewUserRoles = Roles.Select(ur => new ApplicationUserRole
            {
                ApplicationUser = applicationUser,
                Role = ur,
                RoleAssignmentDate = DateTime.UtcNow,
            }).ToList();

            using (var Tran = await Context.Database.BeginTransactionAsync())
            {
                Context.ApplicationUserRoles.AddRange(NewUserRoles);
                await Context.SaveChangesAsync();
                Tran.Commit();
            }
        }
        public async Task<List<AplicationUserRoleDTO>> GetRoles(string UserID, string ApplicationID)
        {
            var Roles = await Context.ApplicationUserRoles
                    .Where(au => au.ApplicationUser.ApplicationID == ApplicationID
                    && au.ApplicationUser.UserID == UserID)
                    .Select(r => new AplicationUserRoleDTO
                    {
                        AppID = r.ApplicationUser.ApplicationID,
                        RoleID = r.Role.UserRoleID,
                        RoleName = r.Role.UserRoleName
                    }).ToListAsync();
            return Roles;
        }
        public async System.Threading.Tasks.Task TurnOffRolesAsync(ApplicationUser applicationUser, params int[] RolesIDs)
        {
            using (var Transaction = await Context.Database.BeginTransactionAsync())
            {
                var predicate =
                       Context.ApplicationUserRoles
                              .Where(ur => ur.ApplicationUser == applicationUser
                              && RolesIDs.Contains(ur.Role.UserRoleID)
                              );

                Context.ApplicationUserRoles.RemoveRange(predicate);
                await Context.SaveChangesAsync();
                Transaction.Commit();
            }
        }

        public async Task<bool> HasRole(string AppID, string UserID, int RoleID)
        {
            var result = await Context.ApplicationUserRoles
                                .AnyAsync(aur => aur.ApplicationUser.User.UserID == UserID
                                && aur.ApplicationUser.ApplicationID == AppID
                                && aur.Role.UserRoleID == RoleID
                                );
            return result;
        }
    }
}