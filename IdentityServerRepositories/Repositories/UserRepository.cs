﻿using Brawn.Helpers;
using ERP.DAL.Contexts.AuthServer;
using IdentityServerRepositories.DTOS.Results;
using Microsoft.EntityFrameworkCore;
using Models.Auth;
using Repositories.DTOS.Creates;
using Repositories.DTOS.Gets;
using Repositories.DTOS.Results;
using Repositories.Interfaces;
using Repositories.MISC;
using Repositories.Repositories;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace AuthServer.Repositories
{

    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        private AuthContext _context;
        public UserRepository(AuthContext context) : base(context)
        {
            _context = context ?? throw new ArgumentNullException("context", "The context object cannot be null.");
        }

        private static AuthContext CreateContext()
        {
            AuthContext authContext = new AuthContext();
            return authContext;
        }
        public UserRepository() : base(CreateContext())
        {
            _context = new AuthContext();

        }
        public async Task<User> FindByEmailAsync(string email)
        {
            return await _context.Users.Where(u => u.Email == email).FirstOrDefaultAsync();
        }
        public User FindByUserID(string subjectId)
        {
            var User = _context.Users.Where(u => u.UserID == subjectId).FirstOrDefault();
            return User;
        }

        private User ParseCreateDTO(UserCreateDTO UserDTO)
        {
            if (UserDTO.JobTitle == null)
                throw new ArgumentNullException("Jobtitle", "JobTitle cannot be null");
            if (UserDTO.Department == null)
                throw new ArgumentNullException("Department", "Department cannot be null");
            if (UserDTO.Location == null)
                throw new ArgumentNullException("Location", "Location cannot be null");
            if (UserDTO.Municipality == null)
                throw new ArgumentNullException("Municipality", "Municipality cannot be null");
            if (UserDTO.Department == null)
                throw new ArgumentNullException("Department", "Department cannot be null");
            return new User
            {
                BirthDate = UserDTO.BirthDate,
                Email = UserDTO.Email.FirstCharToUpper(),
                FirstLastName = UserDTO.FirstLastName.FirstCharToUpper(),
                FirstName = UserDTO.FirstName.FirstCharToUpper(),
                Gender = UserDTO.Gender,
                Ext = UserDTO.Ext,
                SecondLastName = (UserDTO.SecondLastName ?? "").FirstCharToUpper(),
                SecondName = (UserDTO.SecondName ?? "").FirstCharToUpper(),
                isServerAdmin = UserDTO.isServerAdmin,
                Status = UserDTO.Status,
                UserID = UserDTO.Cedula.Replace("-", "").Replace(" ", ""),
                Municipality = UserDTO.Municipality == null ? null : _context.Municipalities.Find(UserDTO.Municipality.MunicipalityID),
                Sector = UserDTO.Sector == null ? null : _context.Sectors.Find(UserDTO.Sector.SectorID),
                Province = UserDTO.Province == null ? null : _context.Provinces.Find(UserDTO.Province.ProvinceID),
                Address = UserDTO.Address,
                JobTitle = UserDTO.JobTitle == null ? null : _context.JobTitles.Find(UserDTO.JobTitle.JobTitleID),
                Location = UserDTO.Location == null ? null : _context.Locations.Find(UserDTO.Location.LocationID),
                Department = (UserDTO.Department == null) ? null : _context.Departments.Find(UserDTO.Department.DepartmentID),
                BirthCountry = (UserDTO.BirthCountry == null) ? null : _context.Countries.Find(UserDTO.BirthCountry.CountryIso2Code),
                FullName = string.Join(" ", new string[] { UserDTO.FirstName, UserDTO.SecondName, UserDTO.FirstLastName, UserDTO.SecondLastName }.Where(c => string.IsNullOrEmpty(c) == false))
            };
        }

        public async Task ChangePasswordAsync(User user, string NewPassword)
        {
            var PasswordsSet = PasswordsMannager.GenerateCredentials(Constants.Peppers, NewPassword);
            user.Password = PasswordsSet.Password;
            user.SecurityKey = PasswordsSet.Salt;
            await UpdateAsync(user);
        }
        public void ChangePassword(User user, string NewPassword)
        {
            var PasswordsSet = PasswordsMannager.GenerateCredentials(Constants.Peppers, NewPassword);
            user.Password = PasswordsSet.Password;
            user.SecurityKey = PasswordsSet.Salt;
            Update(user);
        }
        public override Task<User> UpdateAsync(User entity)
        {
            return base.UpdateAsync(entity);
        }
        public UserCreationResult CreateUser(UserCreateDTO UserDTO)
        {
            if (UserDTO == null) throw new ArgumentNullException("UserDTO", "The Provided UsersCreateDTO must not be null.");

            var ParsedUser = ParseCreateDTO(UserDTO);

            var NewUserPassword = PasswordsMannager.GenerateRandomPassword(length: 8);

            var PasswordsSet = PasswordsMannager.GenerateCredentials(Constants.Peppers, NewUserPassword);

            ParsedUser.Password = PasswordsSet.Password;

            ParsedUser.SecurityKey = PasswordsSet.Salt;

            ParsedUser.Status = UserStatuses.PendingPasswordReset;

            return new UserCreationResult
            {
                ClearTextPassword = NewUserPassword,

                CreatedUser = Insert(ParsedUser)
            };
        }
        public bool ValidateCredentials(string identifier, string password)
        {
            var cedula = identifier.Replace("-", "");
            var StoredUserPassword = _context.Users
                                             .Where(u => u.Email == identifier || u.UserID == cedula)
                                             .Select(p => new PasswordsMannager.UserCredentials
                                             {
                                                 Password = p.Password,
                                                 Salt = p.SecurityKey
                                             }).FirstOrDefault();

            if (StoredUserPassword == null) return false;

            var isValid = PasswordsMannager.CheckRawPassword(password, StoredUserPassword.Password, StoredUserPassword.Salt, Constants.Peppers);

            return isValid;
        }
        public override User Update(User entity)
        {
            var local = _context.Set<User>()
                                .Local
                                .Where(entry => entry.UserID == entity.UserID)
                                .FirstOrDefault();
            if (local != null)
            {
                _context.Entry(local).State = EntityState.Detached;
            }

            _context.Update(entity);
            _context.SaveChanges();

            return entity;
        }
        public async Task<User> FindByUserIDAsync(string ID, bool IncludeDepartment = false)
        {

            ID = ID.Replace("-", "");


            var Queryable = _context.Users.Where(u => u.UserID == ID);

            if (IncludeDepartment)
                Queryable = Queryable.Include(u => u.Department);

            var User = await Queryable.FirstOrDefaultAsync();

            return User;
        }
        public override void Delete(User entity)
        {
            _context.Users.RemoveRange(_context.Users.Where(u => u.UserID == entity.UserID));
            _context.SaveChanges();
        }
        public User FindByEmail(string email)
        {
            var user = _context.Users.Where(u => u.Email == email).FirstOrDefault();
            return user;
        }

        public bool Exists(string ID) => _context.Users.Any(u => u.UserID == ID);

        public bool ExistsByEmail(string Email) => _context.Users.Any(u => u.Email == Email);

        public UserDTO GetUserDTO(string ID)
        {
            ID = ID.Replace("-", "");
            var user = _context.Users.Where(u => u.UserID == ID)
                   .Select(u => new UserDTO
                   {
                       FirstName = u.FirstName,
                       FirstLastName = u.FirstLastName,
                       UserID = ID,
                       SecondLastName = u.SecondLastName,
                       SecondName = u.SecondName,
                       DepartmentID = u.Department.DepartmentID,
                       DepartmentName = u.Department.DepartmentName,
                       JobTitleID = u.JobTitle.JobTitleID,
                       JobTitleName = u.JobTitle.JobTitleName,
                       Email = u.Email
                   }).FirstOrDefault();

            return user;
        }

        public async Task<User> FindByEmailOrIDAsync(string identifier)
        {
            string Cedula = identifier.Replace("-", "");
            var user = await _context.Users.Where(u => u.Email == identifier || u.UserID == Cedula).FirstOrDefaultAsync();
            return user;
        }
        public User FindByEmailOrID(string identifier)
        {
            string Cedula = identifier.Replace("-", "");
            var user = _context.Users.Where(u => u.Email == identifier || u.UserID == Cedula).FirstOrDefault();
            return user;
        }
        public async Task<IList<UserGetDTO>> GetUser(string SearchTerm)
        {

            SqlParameter[] Params = new SqlParameter[]
            {
                new SqlParameter("SearchTerm",SearchTerm),
                new SqlParameter("FullTextSearchTerm", SearchTerm.AsFullTextSearchTerm())
            };

            string TSQL = @"
SELECT USERS.*, D.DepartmentName, J.JobTitleName
 FROM  (
 SELECT TOP(10) 
	   [UserID] 
      ,[DepartmentID]
      ,[Email]
      ,[FirstLastName]
      ,[FirstName]     
      ,[JobTitleID]   
      ,[Status]
      ,[isServerAdmin]
      ,[FullName] 
	  FROM [Users] WHERE CONTAINS([FullName],@SearchTerm) OR [UserID] LIKE @SearchTerm+'%' OR [Email] LIKE @SearchTerm+'%'
 ) USERS
  LEFT JOIN [dbo].[Departments] D 
 ON D.DepartmentID = USERS.[DepartmentID]
 JOIN [dbo].[JobTitles] J
 ON J.JobTitleID = USERS.[JobTitleID]
";

            var Result = await _context.Database.SqlQuery<UserGetDTO>(TSQL, Params).ToListAsync();

            return Result;
            //var Results = await _context.Database.
            //return Results;
        }
        public async Task<VirtualCollectionResult<UserGetDTO>> GetUsersAsync(int Page, int Qntity)
        {
            Page--;


            var result = await _context.Users
                                       .Where(u => u.UserID != "admin")
                                       .OrderBy(u => u.FullName)
                                       .Skip(Qntity * Page)
                                       .Take(Qntity)
                                       .Select(u => new UserGetDTO
                                       {
                                           DepartmentID = u.Department.DepartmentID,
                                           DepartmentName = u.Department.DepartmentName,
                                           Email = u.Email,
                                           isServerAdmin = u.isServerAdmin,
                                           FirstLastName = u.FirstLastName,
                                           FirstName = u.FirstName,
                                           FullName = u.FullName,
                                           JobTitleID = u.JobTitle.JobTitleID,
                                           JobTitleName = u.JobTitle.JobTitleName,
                                           Status = u.Status,
                                           UserID = u.UserID
                                       }).ToListAsync();

            var usersCount = await _context.Users.Where(u => u.UserID != "admin").CountAsync();

            return new VirtualCollectionResult<UserGetDTO>
            {
                Items = result,
                VirtualRowCount = usersCount
            };
        }

        public class UserListDTOVirtualResultset : UserGetDTO
        {
            public int COUNT { get; set; }
        }
        public async Task<VirtualCollectionResult<UserGetDTO>> GetUsersFilteredAsync(int Page, int Qntity, string Filter)
        {
            string STATEMENT = $@"
WITH RESULTSET AS (
    SELECT [UserID],
        D.[DepartmentID],
        [Email],
        [FirstLastName],
        [FirstName],
        J.[JobTitleID],
        [Status],
        [isServerAdmin],
        [FullName],
        d.DepartmentName,
        j.JobTitleName
    FROM [Users] 
    LEFT JOIN [dbo].[Departments] D 
    ON D.DepartmentID = [Users].[DepartmentID]
    JOIN [dbo].[JobTitles] J
    ON J.JobTitleID = [Users].[JobTitleID]
    WHERE CONTAINS([FULLTEXTLOOKUP],@SearchTerm) 
 )
 SELECT *, CAST( (SELECT COUNT(*) FROM RESULTSET) AS INT) AS [COUNT]
 FROM RESULTSET
 ORDER BY [FirstName]
 OFFSET {(Page - 1) * Qntity} ROWS
 FETCH NEXT {Qntity} ROWS ONLY;";
            string Termn = Filter.AsFullTextSearchTerm();
            var Resultset = await _context.Database.SqlQuery<UserListDTOVirtualResultset>(STATEMENT, new SqlParameter("SearchTerm", Termn)).ToListAsync();
            return new VirtualCollectionResult<UserGetDTO>
            {
                Items = Resultset.Select(r => new UserGetDTO
                {
                    DepartmentID = r.DepartmentID,
                    DepartmentName = r.DepartmentName,
                    isServerAdmin = r.isServerAdmin,
                    Email = r.Email,
                    FirstLastName = r.FirstLastName,
                    FirstName = r.FirstName,
                    FullName = r.FullName,
                    JobTitleID = r.JobTitleID,
                    JobTitleName = r.JobTitleName,
                    Status = r.Status,
                    UserID = r.UserID
                }).ToArray(),
                VirtualRowCount = Resultset.Select(d => d.COUNT).FirstOrDefault()
            };
        }
        public async Task<IEnumerable<UserGetDTO>> GetUsersList(List<string> IDs)
        {
            var results = await _context.Users
                                        .Where(us => IDs.Contains(us.UserID))
                                        .Select(u => new UserGetDTO
                                        {
                                            DepartmentID = u.DepartmentID,
                                            DepartmentName = u.Department.DepartmentName,
                                            Email = u.Email,
                                            FirstLastName = u.FirstLastName,
                                            FirstName = u.FirstName,
                                            FullName = u.FullName,
                                            isServerAdmin = u.isServerAdmin,
                                            JobTitleID = u.JobTitleID,
                                            JobTitleName = u.JobTitle.JobTitleName,
                                            Status = u.Status,
                                            UserID = u.UserID,
                                        }).ToListAsync();
            return results;
        }

        public UserCreationResult ChangePasswordGenerate(User UserDTO)
        {

            var ParsedUser = _context.Users.Where(u => u.UserID == UserDTO.UserID).FirstOrDefault();

            var NewUserPassword = PasswordsMannager.GenerateRandomPassword(length: 8);

            var PasswordsSet = PasswordsMannager.GenerateCredentials(Constants.Peppers, NewUserPassword);

            ParsedUser.Password = PasswordsSet.Password;

            ParsedUser.SecurityKey = PasswordsSet.Salt;

            ParsedUser.Status = UserStatuses.PendingPasswordReset;

            return new UserCreationResult
            {
                ClearTextPassword = NewUserPassword,

                CreatedUser = Update(ParsedUser)
            };



        }
    }
}