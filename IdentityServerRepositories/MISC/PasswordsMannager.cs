﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
namespace Repositories.MISC
{
    public class PasswordsMannager
    { /// <summary>
      /// Creates a SHA512 hash based on the provided string
      /// </summary>
      /// <param name="HashArg"></param>
      /// <returns></returns>
      /// 


        public static string GenerateRandomPassword(int length)
        {
            const string alphanumericCharacters =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "abcdefghijklmnopqrstuvwxyz" +
                "0123456789";
            return GetRandomString(length, alphanumericCharacters);
        }

        private static string GetRandomString(int length, IEnumerable<char> characterSet)
        {
            if (length < 0)
                throw new ArgumentException("length must not be negative", "length");
            if (length > int.MaxValue / 8) // 250 million chars ought to be enough for anybody
                throw new ArgumentException("length is too big", "length");
            if (characterSet == null)
                throw new ArgumentNullException("characterSet");
            var characterArray = characterSet.Distinct().ToArray();
            if (characterArray.Length == 0)
                throw new ArgumentException("characterSet must not be empty", "characterSet");

            var bytes = new byte[length * 8];
            new RNGCryptoServiceProvider().GetBytes(bytes);
            var result = new char[length];
            for (int i = 0; i < length; i++)
            {
                ulong value = BitConverter.ToUInt64(bytes, i * 8);
                result[i] = characterArray[value % (uint)characterArray.Length];
            }
            return new string(result);
        }

        public static string CreateSHA512Hash(string HashArg)
        {
            var HashArg_bytes = ASCIIEncoding.ASCII.GetBytes(HashArg);
            SHA512 SHA = new SHA512Managed();
            // Hashing the password and the salt
            var hashed_arr = SHA.ComputeHash(HashArg_bytes);
            string hash_result = Convert.ToBase64String(hashed_arr);
            return hash_result;
        }
        public class UserCredentials
        {
            public byte[] Salt { get; set; }
            public string Password { get; set; }
        }
        public static UserCredentials GenerateCredentials(string[] Peppers, string password)
        {
            // Gets a random byte array to use as a salt
            var RNGProvider = new RNGCryptoServiceProvider();
            byte[] salt = new byte[20];
            RNGProvider.GetBytes(salt);
            Random randomNumber = new Random();

            string pepper = Peppers[randomNumber.Next(Peppers.Length - 1)];
            password = pepper + password;
            // converts the password to an array of bytes
            var password_bytes = ASCIIEncoding.ASCII.GetBytes(password);

            byte[] password_and_salt = new byte[password_bytes.Length + salt.Length];

            // Filling the password and salt array with the password_bytes data
            for (int i = 0; i < password_bytes.Length; i++)
            {
                password_and_salt[i] = password_bytes[i];
            }
            // Adding the salt data to the array
            for (int i = 0; i < salt.Length; i++)
            {
                password_and_salt[i + password_bytes.Length] = salt[i];
            }
            // Initializes an instance of the SHA512 provider
            SHA512 SHA = new SHA512Managed();
            // Hashing the password and the salt
            var hashed_arr = SHA.ComputeHash(password_and_salt);

            string hash_result = Convert.ToBase64String(hashed_arr);

            return new UserCredentials { Salt = salt, Password = hash_result };
        }
        public static string CondimentPassword(string pepper, string password, byte[] salt)
        {
            password = pepper + password;
            var password_bytes = ASCIIEncoding.ASCII.GetBytes(password);
            byte[] password_and_salt = new byte[password_bytes.Length + salt.Length];
            // Filling the password and salt array with the password_bytes data
            for (int i = 0; i < password_bytes.Length; i++)
            {
                password_and_salt[i] = password_bytes[i];
            }
            // Adding the salt data to the array
            for (int i = 0; i < salt.Length; i++)
            {
                password_and_salt[i + password_bytes.Length] = salt[i];
            }
            // Initializes an instance of the SHA512 provider
            SHA512 SHA = new SHA512Managed();
            // Hashing the password and the salt
            var hashed_arr = SHA.ComputeHash(password_and_salt);
            string hash_result = Convert.ToBase64String(hashed_arr);
            return hash_result;
        }

        /// <summary>
        /// Checks wether if a raw password mashes a hashed one
        /// </summary>
        /// <param name="RawPassword"></param>
        /// <param name="HashedPassword"></param>
        /// <param name="salt"></param>
        /// <param name="Peppers"></param>
        /// <returns></returns>
        public static bool CheckRawPassword(string RawPassword, string HashedPassword, byte[] salt, string[] Peppers)
        {
            foreach (var pepper in Peppers)
                if (CondimentPassword(pepper, RawPassword, salt) == HashedPassword)
                    return true;

            return false;
        }
    }
}
