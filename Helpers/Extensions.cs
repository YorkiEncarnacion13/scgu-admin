﻿using System;
using System.Linq;

namespace Brawn.Helpers
{
    public static class Extensions
    {
        public static string FirstCharToUpper(this string input)
        {
            switch (input)
            {
                case null: return null;
                case "": return "";
                default: return input.First().ToString().ToUpper() + input.Substring(1);
            }
        }
        public static bool isNull(this object obj) => (obj == null);
        public static bool isNotNull(this object obj) => (obj != null);
        public static string AsFullTextSearchTerm(this string searchterm)
        {
            char[] dummy = null;

            var words = searchterm.Split(dummy, StringSplitOptions.RemoveEmptyEntries);

            string searchTerm = "";

            if (words.Length == 1) searchTerm = "'" + words[0] + "*'";

            else searchTerm = "'" + words[0] + "'";

            if (words.Length > 1)
            {
                for (int i = 1; i < words.Length; i++)
                {
                    if (i != words.Length - 1)
                        searchTerm = searchTerm + " AND '" + words[i] + "'";
                    else searchTerm = searchTerm + " AND '" + words[i] + "*'";
                }
            }
            searchTerm = searchTerm.Replace("'", "\"");
            return searchTerm;
        }
    }
}
