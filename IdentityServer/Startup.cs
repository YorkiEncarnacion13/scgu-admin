﻿using AuthServer.Services;
using ERP.DAL.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repositories.MISC;
using System;
using System.IdentityModel.Tokens.Jwt;

namespace AuthServer_IdentityServer
{
    public class Startup
    {
     
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMvc()
                 .AddJsonOptions(options =>
                 {
                     options.SerializerSettings.ReferenceLoopHandling =
                         Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                 });

            //services.Configure<IISOptions>(options =>
            //{
            //    options.ForwardClientCertificate = false;
            //});

            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            services
                .AddIdentityServer(c =>
                {
                    c.UserInteraction.CustomRedirectReturnUrlParameter = "uid";
                })
                .AddDeveloperSigningCredential()
                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                .AddInMemoryApiResources(Config.GetApiResources())
                .AddClientStore<ClientsStore>()
                .AddCustomUserStore();
            services.AddTransient<ViewRenderService>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = "Cookies";
                options.DefaultChallengeScheme = "oidc";
            })
          .AddCookie("Cookies")
          .AddOpenIdConnect("oidc", options =>
          {
              options.SignInScheme = "Cookies";
              options.Authority = Config.SGU_URI;
              options.RequireHttpsMetadata = false;
              options.ClientId = Config.AppID;
              options.SaveTokens = true;
          });

            //services.AddHttpsRedirection(options =>
            //{
            //    options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
            //    options.HttpsPort = 443;
            //});
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();

            if (env.IsDevelopment())
            {


            }
            else
            {
                //app.UseHttpsRedirection();
            }
            app.UseIdentityServer();
            app.UseStaticFiles();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                      name: "default",
                      template: "{controller}/{action=Index}/{id?}");
            });
            // Checks if the Auth server client has beeen created in the repository, 
            // if not, creates the default one.

            EnsureClientExistence(env);
        }

        private static void EnsureClientExistence(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                       .SetBasePath(env.ContentRootPath)
                       .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            var config = builder.Build();
            using (var unit = new UnitOfWork())
            {
                var AppSettings = config.GetSection("AppSettings");
                string AppId = AppSettings["SCGU_ClientID"];
                string AppBaseRoute = AppSettings["AppBaseRoute"];

                if (AppBaseRoute.EndsWith("/"))
                    AppBaseRoute = AppBaseRoute.Remove(AppBaseRoute.Length - 1);

                if (unit.ApplicationRepository.FindById(AppId) == null)
                {
                    var IdentityServerApp = unit.ApplicationRepository.Insert(new Models.System.Application
                    {
                        ApplicationID = AppId,
                        Description = "Sistema de Tokens del Ministerio de Industria, Comercio y Mipymes",
                        ApplicationStatus = Models.System.ApplicationStatuses.online,
                        IntroductionDate = DateTime.Now,
                        RequiresConcent = true,
                        PostLogoutRedirectUri = $"{AppBaseRoute}/signout-callback-oidc",
                        GrantTypes = Models.System.GrantTypes.Implicit,
                        Uri = $"{AppBaseRoute}",
                        RedirectUri = $"{AppBaseRoute}/signin-oidc",
                    });

                    var PasswordComposite = PasswordsMannager.GenerateCredentials(Constants.Peppers, "admin");

                    var user = unit.UsersRepository.Insert(new Models.Auth.User
                    {
                        Address = null,
                        BirthCountry = null,
                        BirthDate = DateTime.UtcNow,
                        isServerAdmin = true,
                        Department = null,
                        Email = "admin@admin.org",
                        Gender = Models.Auth.Genders.Other,
                        FirstLastName = "ADMIN",
                        FirstName = "SCGU",
                        Status = Models.Auth.UserStatuses.PendingPasswordReset,
                        UserID = "admin",
                        Password = PasswordComposite.Password,
                        SecurityKey = PasswordComposite.Salt,
                    });

                    unit.ApplicationUserRepository.Insert(new Models.System.ApplicationUser
                    {
                        Application = IdentityServerApp,
                        User = user,
                        RegistrationDate = DateTime.Now,
                        AppUserStatus = Models.System.ApplicationUserStatus.Normal
                    });
                }
            }
        }
    }
}