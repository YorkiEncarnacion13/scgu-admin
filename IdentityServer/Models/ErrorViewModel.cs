﻿using IdentityServer4.Models;

namespace AuthServer.ViewModels
{
    public class ErrorViewModel
    {
        public ErrorMessage Error { get; set; }
    }
}
