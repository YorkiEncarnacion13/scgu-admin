﻿namespace AuthServer.ViewModels
{
    public class LoginVM
    {
        public string ReturnURL { get; set; }
        public string ClientID { get; set; }
        public string DisplayName { get; set; }
    }
}
