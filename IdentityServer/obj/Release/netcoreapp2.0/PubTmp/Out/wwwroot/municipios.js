﻿[
    {
        "ProvinceID": 1,
        "MunicipalityID": 101,
        "MunicipalityName": "SANTO DOMINGO CENTRO (DN)"
    },
    {
        "ProvinceID": 71,
        "MunicipalityID": 201,
        "MunicipalityName": "AZUA"
    },
    {
        "ProvinceID": 71,
        "MunicipalityID": 202,
        "MunicipalityName": "LAS CHARCAS"
    },
    {
        "ProvinceID": 71,
        "MunicipalityID": 203,
        "MunicipalityName": "LAS YAYAS DE VIAJAMA"
    },
    {
        "ProvinceID": 71,
        "MunicipalityID": 204,
        "MunicipalityName": "PADRE LAS CASAS"
    },
    {
        "ProvinceID": 71,
        "MunicipalityID": 205,
        "MunicipalityName": "PERALTA"
    },
    {
        "ProvinceID": 71,
        "MunicipalityID": 206,
        "MunicipalityName": "SABANA YEGUA"
    },
    {
        "ProvinceID": 71,
        "MunicipalityID": 207,
        "MunicipalityName": "PUEBLO VIEJO"
    },
    {
        "ProvinceID": 71,
        "MunicipalityID": 208,
        "MunicipalityName": "TABARA ARRIBA"
    },
    {
        "ProvinceID": 71,
        "MunicipalityID": 209,
        "MunicipalityName": "GUAYABAL"
    },
    {
        "ProvinceID": 71,
        "MunicipalityID": 210,
        "MunicipalityName": "ESTEBANIA"
    },
    {
        "ProvinceID": 82,
        "MunicipalityID": 301,
        "MunicipalityName": "NEIBA"
    },
    {
        "ProvinceID": 82,
        "MunicipalityID": 302,
        "MunicipalityName": "GALVAN"
    },
    {
        "ProvinceID": 82,
        "MunicipalityID": 303,
        "MunicipalityName": "TAMAYO"
    },
    {
        "ProvinceID": 82,
        "MunicipalityID": 304,
        "MunicipalityName": "VILLA JARAGUA"
    },
    {
        "ProvinceID": 82,
        "MunicipalityID": 305,
        "MunicipalityName": "LOS RIOS"
    },
    {
        "ProvinceID": 81,
        "MunicipalityID": 401,
        "MunicipalityName": "BARAHONA"
    },
    {
        "ProvinceID": 81,
        "MunicipalityID": 402,
        "MunicipalityName": "CABRAL"
    },
    {
        "ProvinceID": 81,
        "MunicipalityID": 403,
        "MunicipalityName": "ENRIQUILLO"
    },
    {
        "ProvinceID": 81,
        "MunicipalityID": 404,
        "MunicipalityName": "PARAISO"
    },
    {
        "ProvinceID": 81,
        "MunicipalityID": 405,
        "MunicipalityName": "VICENTE NOBLE"
    },
    {
        "ProvinceID": 81,
        "MunicipalityID": 406,
        "MunicipalityName": "EL PEÑON"
    },
    {
        "ProvinceID": 81,
        "MunicipalityID": 407,
        "MunicipalityName": "LA CIENAGA"
    },
    {
        "ProvinceID": 81,
        "MunicipalityID": 408,
        "MunicipalityName": "FUNDACION"
    },
    {
        "ProvinceID": 81,
        "MunicipalityID": 409,
        "MunicipalityName": "LAS SALINAS"
    },
    {
        "ProvinceID": 81,
        "MunicipalityID": 410,
        "MunicipalityName": "POLO"
    },
    {
        "ProvinceID": 81,
        "MunicipalityID": 411,
        "MunicipalityName": "JAQUIMEYES"
    },
    {
        "ProvinceID": 63,
        "MunicipalityID": 501,
        "MunicipalityName": "DAJABON"
    },
    {
        "ProvinceID": 63,
        "MunicipalityID": 502,
        "MunicipalityName": "LOMA DE CABRERA"
    },
    {
        "ProvinceID": 63,
        "MunicipalityID": 503,
        "MunicipalityName": "PARTIDO"
    },
    {
        "ProvinceID": 63,
        "MunicipalityID": 504,
        "MunicipalityName": "RESTAURACION"
    },
    {
        "ProvinceID": 63,
        "MunicipalityID": 505,
        "MunicipalityName": "EL PINO"
    },
    {
        "ProvinceID": 31,
        "MunicipalityID": 601,
        "MunicipalityName": "SAN FRANCISCO DE MACORIS"
    },
    {
        "ProvinceID": 31,
        "MunicipalityID": 602,
        "MunicipalityName": "ARENOSO"
    },
    {
        "ProvinceID": 31,
        "MunicipalityID": 603,
        "MunicipalityName": "CASTILLO"
    },
    {
        "ProvinceID": 31,
        "MunicipalityID": 604,
        "MunicipalityName": "PIMENTEL"
    },
    {
        "ProvinceID": 31,
        "MunicipalityID": 605,
        "MunicipalityName": "VILLA RIVA"
    },
    {
        "ProvinceID": 31,
        "MunicipalityID": 606,
        "MunicipalityName": "LAS GUARANAS"
    },
    {
        "ProvinceID": 31,
        "MunicipalityID": 607,
        "MunicipalityName": "EUGENIO MARIA DE HOSTOS"
    },
    {
        "ProvinceID": 73,
        "MunicipalityID": 701,
        "MunicipalityName": "COMENDADOR"
    },
    {
        "ProvinceID": 73,
        "MunicipalityID": 702,
        "MunicipalityName": "BANICA"
    },
    {
        "ProvinceID": 73,
        "MunicipalityID": 703,
        "MunicipalityName": "EL LLANO"
    },
    {
        "ProvinceID": 73,
        "MunicipalityID": 704,
        "MunicipalityName": "HONDO VALLE"
    },
    {
        "ProvinceID": 73,
        "MunicipalityID": 705,
        "MunicipalityName": "PEDRO SANTANA"
    },
    {
        "ProvinceID": 73,
        "MunicipalityID": 706,
        "MunicipalityName": "JUAN SANTIAGO"
    },
    {
        "ProvinceID": 24,
        "MunicipalityID": 801,
        "MunicipalityName": "EL SEIBO"
    },
    {
        "ProvinceID": 24,
        "MunicipalityID": 802,
        "MunicipalityName": "MICHES"
    },
    {
        "ProvinceID": 56,
        "MunicipalityID": 901,
        "MunicipalityName": "MOCA"
    },
    {
        "ProvinceID": 56,
        "MunicipalityID": 902,
        "MunicipalityName": "CAYETANO GERMOSEN"
    },
    {
        "ProvinceID": 56,
        "MunicipalityID": 903,
        "MunicipalityName": "GASPAR HERNANDEZ"
    },
    {
        "ProvinceID": 56,
        "MunicipalityID": 904,
        "MunicipalityName": "JAMAO AL NORTE"
    },
    {
        "ProvinceID": 83,
        "MunicipalityID": 1001,
        "MunicipalityName": "JIMANI"
    },
    {
        "ProvinceID": 83,
        "MunicipalityID": 1002,
        "MunicipalityName": "DUVERGE"
    },
    {
        "ProvinceID": 83,
        "MunicipalityID": 1003,
        "MunicipalityName": "LA DESCUBIERTA"
    },
    {
        "ProvinceID": 83,
        "MunicipalityID": 1004,
        "MunicipalityName": "POSTRER RIO"
    },
    {
        "ProvinceID": 83,
        "MunicipalityID": 1005,
        "MunicipalityName": "CRISTOBAL"
    },
    {
        "ProvinceID": 83,
        "MunicipalityID": 1006,
        "MunicipalityName": "MELLA"
    },
    {
        "ProvinceID": 23,
        "MunicipalityID": 1101,
        "MunicipalityName": "HIGUEY"
    },
    {
        "ProvinceID": 23,
        "MunicipalityID": 1102,
        "MunicipalityName": "SAN RAFAEL DEL YUMA"
    },
    {
        "ProvinceID": 22,
        "MunicipalityID": 1201,
        "MunicipalityName": "LA ROMANA"
    },
    {
        "ProvinceID": 22,
        "MunicipalityID": 1202,
        "MunicipalityName": "GUAYMATE"
    },
    {
        "ProvinceID": 22,
        "MunicipalityID": 1203,
        "MunicipalityName": "VILLA HERMOSA"
    },
    {
        "ProvinceID": 41,
        "MunicipalityID": 1301,
        "MunicipalityName": "LA VEGA"
    },
    {
        "ProvinceID": 41,
        "MunicipalityID": 1302,
        "MunicipalityName": "CONSTANZA"
    },
    {
        "ProvinceID": 41,
        "MunicipalityID": 1303,
        "MunicipalityName": "JARABACOA"
    },
    {
        "ProvinceID": 41,
        "MunicipalityID": 1304,
        "MunicipalityName": "JIMA ABAJO"
    },
    {
        "ProvinceID": 33,
        "MunicipalityID": 1401,
        "MunicipalityName": "NAGUA"
    },
    {
        "ProvinceID": 33,
        "MunicipalityID": 1402,
        "MunicipalityName": "CABRERA"
    },
    {
        "ProvinceID": 33,
        "MunicipalityID": 1403,
        "MunicipalityName": "EL FACTOR"
    },
    {
        "ProvinceID": 33,
        "MunicipalityID": 1404,
        "MunicipalityName": "RIO SAN JUAN"
    },
    {
        "ProvinceID": 62,
        "MunicipalityID": 1501,
        "MunicipalityName": "MONTE CRISTI"
    },
    {
        "ProvinceID": 62,
        "MunicipalityID": 1502,
        "MunicipalityName": "CASTAÃ‘UELAS"
    },
    {
        "ProvinceID": 62,
        "MunicipalityID": 1503,
        "MunicipalityName": "GUAYUBIN"
    },
    {
        "ProvinceID": 62,
        "MunicipalityID": 1504,
        "MunicipalityName": "LAS MATAS DE SANTA CRUZ"
    },
    {
        "ProvinceID": 62,
        "MunicipalityID": 1505,
        "MunicipalityName": "PEPILLO SALCEDO"
    },
    {
        "ProvinceID": 62,
        "MunicipalityID": 1506,
        "MunicipalityName": "VILLA VASQUEZ"
    },
    {
        "ProvinceID": 84,
        "MunicipalityID": 1601,
        "MunicipalityName": "PEDERNALES"
    },
    {
        "ProvinceID": 84,
        "MunicipalityID": 1602,
        "MunicipalityName": "OVIEDO"
    },
    {
        "ProvinceID": 94,
        "MunicipalityID": 1701,
        "MunicipalityName": "BANI"
    },
    {
        "ProvinceID": 94,
        "MunicipalityID": 1702,
        "MunicipalityName": "NIZAO"
    },
    {
        "ProvinceID": 57,
        "MunicipalityID": 1801,
        "MunicipalityName": "PUERTO PLATA"
    },
    {
        "ProvinceID": 57,
        "MunicipalityID": 1802,
        "MunicipalityName": "ALTAMIRA"
    },
    {
        "ProvinceID": 57,
        "MunicipalityID": 1803,
        "MunicipalityName": "GUANANICO"
    },
    {
        "ProvinceID": 57,
        "MunicipalityID": 1804,
        "MunicipalityName": "IMBERT"
    },
    {
        "ProvinceID": 57,
        "MunicipalityID": 1805,
        "MunicipalityName": "LOS HIDALGOS"
    },
    {
        "ProvinceID": 57,
        "MunicipalityID": 1806,
        "MunicipalityName": "LUPERON"
    },
    {
        "ProvinceID": 57,
        "MunicipalityID": 1807,
        "MunicipalityName": "SOSUA"
    },
    {
        "ProvinceID": 57,
        "MunicipalityID": 1808,
        "MunicipalityName": "VILLA ISABELA"
    },
    {
        "ProvinceID": 57,
        "MunicipalityID": 1809,
        "MunicipalityName": "VILLA MONTELLANO"
    },
    {
        "ProvinceID": 34,
        "MunicipalityID": 1901,
        "MunicipalityName": "SALCEDO"
    },
    {
        "ProvinceID": 34,
        "MunicipalityID": 1902,
        "MunicipalityName": "TENARES"
    },
    {
        "ProvinceID": 34,
        "MunicipalityID": 1903,
        "MunicipalityName": "VILLA TAPIA"
    },
    {
        "ProvinceID": 32,
        "MunicipalityID": 2001,
        "MunicipalityName": "SAMANA"
    },
    {
        "ProvinceID": 32,
        "MunicipalityID": 2002,
        "MunicipalityName": "SANCHEZ"
    },
    {
        "ProvinceID": 32,
        "MunicipalityID": 2003,
        "MunicipalityName": "LAS TERRENAS"
    },
    {
        "ProvinceID": 91,
        "MunicipalityID": 2101,
        "MunicipalityName": "SAN CRISTOBAL"
    },
    {
        "ProvinceID": 91,
        "MunicipalityID": 2102,
        "MunicipalityName": "SABANA GRANDE DE PALENQUE"
    },
    {
        "ProvinceID": 91,
        "MunicipalityID": 2103,
        "MunicipalityName": "BAJOS DE HAINA"
    },
    {
        "ProvinceID": 91,
        "MunicipalityID": 2104,
        "MunicipalityName": "CAMBITA GARABITOS"
    },
    {
        "ProvinceID": 91,
        "MunicipalityID": 2105,
        "MunicipalityName": "VILLA ALTAGRACIA"
    },
    {
        "ProvinceID": 91,
        "MunicipalityID": 2106,
        "MunicipalityName": "YAGUATE"
    },
    {
        "ProvinceID": 91,
        "MunicipalityID": 2107,
        "MunicipalityName": "SAN GREGORIO DE NIGUA"
    },
    {
        "ProvinceID": 91,
        "MunicipalityID": 2108,
        "MunicipalityName": "LOS CACAOS"
    },
    {
        "ProvinceID": 72,
        "MunicipalityID": 2201,
        "MunicipalityName": "SAN JUAN"
    },
    {
        "ProvinceID": 72,
        "MunicipalityID": 2202,
        "MunicipalityName": "BOHECHIO"
    },
    {
        "ProvinceID": 72,
        "MunicipalityID": 2203,
        "MunicipalityName": "EL CERCADO"
    },
    {
        "ProvinceID": 72,
        "MunicipalityID": 2204,
        "MunicipalityName": "JUAN DE HERRERA"
    },
    {
        "ProvinceID": 72,
        "MunicipalityID": 2205,
        "MunicipalityName": "LAS MATAS DE FARFAN"
    },
    {
        "ProvinceID": 72,
        "MunicipalityID": 2206,
        "MunicipalityName": "VALLEJUELO"
    },
    {
        "ProvinceID": 21,
        "MunicipalityID": 2301,
        "MunicipalityName": "SAN PEDRO DE MACORIS"
    },
    {
        "ProvinceID": 21,
        "MunicipalityID": 2302,
        "MunicipalityName": "LOS LLANOS"
    },
    {
        "ProvinceID": 21,
        "MunicipalityID": 2303,
        "MunicipalityName": "RAMON SANTANA"
    },
    {
        "ProvinceID": 21,
        "MunicipalityID": 2304,
        "MunicipalityName": "CONSUELO"
    },
    {
        "ProvinceID": 21,
        "MunicipalityID": 2305,
        "MunicipalityName": "QUISQUEYA"
    },
    {
        "ProvinceID": 21,
        "MunicipalityID": 2306,
        "MunicipalityName": "GUAYACANES"
    },
    {
        "ProvinceID": 43,
        "MunicipalityID": 2401,
        "MunicipalityName": "COTUI"
    },
    {
        "ProvinceID": 43,
        "MunicipalityID": 2402,
        "MunicipalityName": "CEVICOS"
    },
    {
        "ProvinceID": 43,
        "MunicipalityID": 2403,
        "MunicipalityName": "FANTINO"
    },
    {
        "ProvinceID": 43,
        "MunicipalityID": 2404,
        "MunicipalityName": "LA MATA"
    },
    {
        "ProvinceID": 51,
        "MunicipalityID": 2501,
        "MunicipalityName": "SANTIAGO"
    },
    {
        "ProvinceID": 51,
        "MunicipalityID": 2502,
        "MunicipalityName": "BISONO"
    },
    {
        "ProvinceID": 51,
        "MunicipalityID": 2503,
        "MunicipalityName": "JANICO"
    },
    {
        "ProvinceID": 51,
        "MunicipalityID": 2504,
        "MunicipalityName": "LICEY AL MEDIO"
    },
    {
        "ProvinceID": 51,
        "MunicipalityID": 2505,
        "MunicipalityName": "SAN JOSE DE LAS MATAS"
    },
    {
        "ProvinceID": 51,
        "MunicipalityID": 2506,
        "MunicipalityName": "TAMBORIL"
    },
    {
        "ProvinceID": 51,
        "MunicipalityID": 2507,
        "MunicipalityName": "VILLA GONZALEZ"
    },
    {
        "ProvinceID": 51,
        "MunicipalityID": 2508,
        "MunicipalityName": "PUÑAL"
    },
    {
        "ProvinceID": 51,
        "MunicipalityID": 2509,
        "MunicipalityName": "SABANA IGLESIA"
    },
    {
        "ProvinceID": 64,
        "MunicipalityID": 2601,
        "MunicipalityName": "SAN IGNACIO DE SABANETA"
    },
    {
        "ProvinceID": 64,
        "MunicipalityID": 2602,
        "MunicipalityName": "VILLA LOS ALMACIGOS"
    },
    {
        "ProvinceID": 64,
        "MunicipalityID": 2603,
        "MunicipalityName": "MONCION"
    },
    {
        "ProvinceID": 61,
        "MunicipalityID": 2701,
        "MunicipalityName": "MAO"
    },
    {
        "ProvinceID": 61,
        "MunicipalityID": 2702,
        "MunicipalityName": "ESPERANZA"
    },
    {
        "ProvinceID": 61,
        "MunicipalityID": 2703,
        "MunicipalityName": "LAGUNA SALADA"
    },
    {
        "ProvinceID": 42,
        "MunicipalityID": 2801,
        "MunicipalityName": "BONAO"
    },
    {
        "ProvinceID": 42,
        "MunicipalityID": 2802,
        "MunicipalityName": "MAIMON"
    },
    {
        "ProvinceID": 42,
        "MunicipalityID": 2803,
        "MunicipalityName": "PIEDRA BLANCA"
    },
    {
        "ProvinceID": 92,
        "MunicipalityID": 2901,
        "MunicipalityName": "MONTE PLATA"
    },
    {
        "ProvinceID": 92,
        "MunicipalityID": 2902,
        "MunicipalityName": "BAYAGUANA"
    },
    {
        "ProvinceID": 92,
        "MunicipalityID": 2903,
        "MunicipalityName": "SABANA GRANDE DE BOYA"
    },
    {
        "ProvinceID": 92,
        "MunicipalityID": 2904,
        "MunicipalityName": "YAMASA"
    },
    {
        "ProvinceID": 92,
        "MunicipalityID": 2905,
        "MunicipalityName": "PERALVILLO"
    },
    {
        "ProvinceID": 25,
        "MunicipalityID": 3001,
        "MunicipalityName": "HATO MAYOR"
    },
    {
        "ProvinceID": 25,
        "MunicipalityID": 3002,
        "MunicipalityName": "SABANA DE LA MAR"
    },
    {
        "ProvinceID": 25,
        "MunicipalityID": 3003,
        "MunicipalityName": "EL VALLE"
    },
    {
        "ProvinceID": 93,
        "MunicipalityID": 3101,
        "MunicipalityName": "SAN JOSE DE OCOA"
    },
    {
        "ProvinceID": 93,
        "MunicipalityID": 3102,
        "MunicipalityName": "SABANA LARGA"
    },
    {
        "ProvinceID": 93,
        "MunicipalityID": 3103,
        "MunicipalityName": "RANCHO ARRIBA"
    },
    {
        "ProvinceID": 1,
        "MunicipalityID": 3201,
        "MunicipalityName": "SANTO DOMINGO ESTE"
    },
    {
        "ProvinceID": 1,
        "MunicipalityID": 3202,
        "MunicipalityName": "SANTO DOMINGO OESTE"
    },
    {
        "ProvinceID": 1,
        "MunicipalityID": 3203,
        "MunicipalityName": "SANTO DOMINGO NORTE"
    },
    {
        "ProvinceID": 1,
        "MunicipalityID": 3204,
        "MunicipalityName": "BOCA CHICA"
    },
    {
        "ProvinceID": 1,
        "MunicipalityID": 3205,
        "MunicipalityName": "SAN ANTONIO DE GUERRA"
    },
    {
        "ProvinceID": 1,
        "MunicipalityID": 3206,
        "MunicipalityName": "LOS ALCARRIZOS"
    },
    {
        "ProvinceID": 1,
        "MunicipalityID": 3207,
        "MunicipalityName": "PEDRO BRAND"
    }
]