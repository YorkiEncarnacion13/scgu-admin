﻿(function () {
    'use strict';

    angular
        .module('AuthServerApp')
        .controller('UserSearchController', UserSearchController);

    UserSearchController.$inject = ['$location', 'API'];

    function UserSearchController($location, API) {
        const View = this;
       

        View.GetUsers = (Page, Limit) => {
            debugger;
            if (View.SearchTerm) {
                API.Users.GetUsersFiltered(Page, Limit,View.SearchTerm).then((r) => {
                    debugger;
                    console.log('results', r)
                    View.UsersList = r.data.items;
                    View.total = r.data.virtualRowCount;
                    View.Limit = Limit;
                    View.Page = Page;
                });
            } else {
                API.Users.GetUsersPaginated(Page, Limit).then((r) => {
                    debugger;
                    console.log('users =>', r);
                    View.UsersList = r.data.items;
                    View.total = r.data.virtualRowCount;
                    View.Limit = Limit;
                    View.Page = Page;
                })
            }
        }
        View.OnReorder = (order, q) => {
            console.log(order, q)
            switch (order) {
                case 'fullName':
                    View.UsersList = _.sortBy(View.UsersList, 'fullName');
                    break;
                case '-fullName':
                    View.UsersList = _.sortBy(View.UsersList, 'fullName').reverse();
                    break;
                case 'userID':
                    View.UsersList = _.sortBy(View.UsersList, 'userID');
                    break;
                case '-userID':
                    View.UsersList = _.sortBy(View.UsersList, 'userID').reverse();
                    break;
            }
        }



        activate();

        function activate() {
            View.GetUsers(1, 5)
        }
    }
})();
