﻿(function () {
    'use strict';
    angular
        .module('AuthServerApp')
        .controller('NewUserController', NewUserController);
    NewUserController.$inject = ['ValidatorsService', 'API', '$mdDialog', '$sce', '$http', '$scope'];
    function NewUserController(ValidatorsService, API, $mdDialog, $sce, $http, $scope) {
        const View = this;

        const Colors = { Red: '#ff0000', Green: '#5FBD00' };

        const ControllerStates = { Submitting: 1, Normal: 2, Submitted: 3 };

        View.AppStates = ControllerStates;
        View.AppState = ControllerStates.Normal;

        const onErrorOccurred = (e) => {
            console.error('Ha ocurrido un error', e);
            $.sweetModal({
                content: 'Ha ocurrido un error',
                icon: $.sweetModal.ICON_WARNING
            });
            View.AppState = ControllerStates.Normal;
        }

        View.NewUser = {
            ProvinceID: null
        };
        View.OnEmailChange = (email) => {
            if (ValidatorsService.isEmail(email)) {
                API.Users.IsEmailValid(email).then((r) => {
                    console.log(r)
                    if (r.data.isEmailValid) {
                        View.EmailValidation = {
                            IsValid: true,
                            Message: 'Excelente',
                            Color: Colors.Green
                        }
                    } else View.EmailValidation = {
                        IsValid: false,
                        Message: 'Oops! Este correo esta en uso.',
                        Color: Colors.Red
                    }
                }, (r) => onErrorOccurred(r));
            }
            else View.EmailValidation = {
                IsValid: false,
                Message: 'Esto no parece ser un email válido',
                Color: Colors.Red
            }
        }
        View.onIDChange = (ID) => {
            if (ValidatorsService.isCedula(ID)) {
                API.Users
                    .IsIDValid(ID)
                    .then((r) => {
                        if (r.data.isIDValid) {
                            View.IDValidation = {
                                IsValid: true,
                                Message: 'Excelente',
                                Color: Colors.Green
                            }

                        } else View.IDValidation = {
                            IsValid: false,
                            Message: 'Oops! al parecer este usuario ya esta registrado en el sistema.',
                            Color: Colors.Red
                        }
                    }, (e) => onErrorOccurred(e));

            } else View.IDValidation = {
                IsValid: false,
                Message: 'Esto no parece ser una cedula válida',
                Color: Colors.Red
            }
        }
        API.Countries.Get().then((r) => {
            View.Countries = r.data;
            let RD = _.find(View.Countries, { 'countryIso2Code': 'DO' });
            View.NewUser.BirthCountry = RD;
        });
        API.Provinces.Get().then((r) => {
            console.log(r);
            View.Provinces = r.data;
        }, (r) => onErrorOccurred(r));

        View.LoadMunicipios = (provinceID) => {
            View.Municipalities = [];
            API.Municipalities.GetProvinceMunicipalities(provinceID)
                .then((r) => {
                    console.log(r);
                    View.Municipalities = r.data;
                }, (r) => {
                    console.error('error', r);
                })
        }
        View.LoadSectors = (MunicipalityID) => {
            View.Sectors = [];
            API.Sectors.GetMunicipalitySectors(MunicipalityID).then((r) => {
                console.log('sectors', r);
                View.Sectors = r.data;
            }, (r) => onErrorOccurred(r))
        }
        View.isCedula = ValidatorsService.isCedula;
        View.printCurrentForm = () => console.log(View.NewUser);
        View.JobTitle = {
            JobTitles: [],
            onTextChange: (searchTermn) => {
                API.JobTitles.GetJobTitles(searchTermn).then((r) => {
                    console.warn('r', r)
                    View.JobTitle.JobTitles = r.data;
                }, (e) => onErrorOccurred(e));
            },
            LookUpText: '',
            cacheDisabled: true,
            selectedTitle: null
        };
        View.Locations = {
            Locations: [],
            onTextChange: (searchTermn) => {
                API.Locations.GetLocations(searchTermn).then((r) => {
                    console.warn('r', r)
                    View.Locations.Locations = r.data;
                }, (e) => onErrorOccurred(e));
            },
            LookUpText: '',
            cacheDisabled: true,
        };
        View.Departments = {
            Departments: [],
            onTextChange: (searchTermn) => {
                API.Departments.GetDepartments(searchTermn).then((r) => {
                    console.warn('r', r)
                    View.Departments.Departments = r.data;

                }, (e) => onErrorOccurred(e));
            },
            LookUpText: '',
            cacheDisabled: true,
            selectedTitle: null
        };

        const REPORT_DIALOG_CONTROLLER = (REPORT_DIALOG_CONTROLLER$scope, $sce) => {
            REPORT_DIALOG_CONTROLLER$scope.loading = true;
          
            const REQUEST = {
                responseType: 'arraybuffer',
                url: 'Reports/NewUserReport',
                method: "POST",
                data: {
                    Email: View.NewUser.Email,
                    Cedula: View.NewUser.Cedula,
                    Password: View.NewUser.Password
                }
            }
            $http(REQUEST).then((r) => {
                const file = new Blob([r.data], { type: 'application/pdf' });
                const FileUrl = URL.createObjectURL(file);
                REPORT_DIALOG_CONTROLLER$scope.Content = $sce.trustAsResourceUrl(FileUrl);
                REPORT_DIALOG_CONTROLLER$scope.loading = false;
            }, (e) => {
                alert('Ha ocurrido un error,' + JSON.stringify(e))
            });
        }
        REPORT_DIALOG_CONTROLLER.$inject = ['$scope', '$sce'];
        const REPORT_DIALOG_CONTROLLERTemplate = `
<md-dialog style="width: 67%!important" layout="column" id="reportModal" layout-align="center center">
    <md-toolbar>
        <div class="md-toolbar-tools">
            <h2>Reporte</h2>
            <span flex></span>
            <md-button class="md-icon-button" ng-click="cancel()">
                <md-icon md-svg-src="img/icons/ic_close_24px.svg" aria-label="Close dialog"></md-icon>
            </md-button>
        </div>
    </md-toolbar>
    <md-dialog-content style="width: 100%;">
        <div ng-if="loading" style="padding:229px!important">
            <md-progress-circular style="height:200px!important" md-diameter="100"></md-progress-circular>
        </div>
        <div ng-if="!loading" style="width:100%">
            <iframe src="{{Content}}" width="95%" height="700" type="application/pdf"></iframe>
        </div>
    </md-dialog-content>
</md-dialog>
`;
        View.GenerateReport = (ev) => {
            $mdDialog.show({
                controller: REPORT_DIALOG_CONTROLLER,
                template: REPORT_DIALOG_CONTROLLERTemplate,
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: true // Only for -xs, -sm breakpoints.
            });
        }
        View.Register = (NewUser) => {
            $.sweetModal.confirm('¿Registramos al usuario?', function () {
                $scope.$safeApply(RegisterLogic);
            });
            const RegisterLogic = () => {
                View.AppState = ControllerStates.Submitting;
                const valueOrNull = angular.AuthServer.valueOrNull;
                API.Users.CreateNewUser(NewUser).then((r) => {
                    console.log('NewUser', r);
                    View.AppState = ControllerStates.Normal;
                    View.NewUser.Password = r.data.clearTextPassword;
                    //                    const Template =
                    //                        `
                    //<div style="text-align: left;">
                    //<h3>¡El usuario ha sido registrado!</h3>
                    //    <span style="font-weight:bold">Email: </span><span>${NewUser.Email}</span>
                    //    <br />
                    //    <span style="font-weight:bold">Cedula: </span><span>${r.data.userID}</span>
                    //    <br />
                    //    <span style="font-weight:bold">Contrase&ntilde;a: </span><span>${r.data.clearTextPassword}</span>
                    //    <br />
                    //    <span style="color:red">OJO: </span><span style="font-size:smaller">Se le solicitar&aacute; el cambio de contraseña al usuario la primera vez que inicie sesi&oacute;n.</span>
                    //</div>
                    //`;
                    //                    $.sweetModal({
                    //                        content: Template,
                    //                        buttons: [{ label: 'Continuar', classes: 'blueB' }]
                    //                    });
                    View.AppState = ControllerStates.Submitted;
                }, (e) => onErrorOccurred(e))
            }
        }
    }
})();
