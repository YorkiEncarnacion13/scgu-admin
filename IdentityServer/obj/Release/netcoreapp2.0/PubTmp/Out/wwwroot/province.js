﻿[
    {
        "ProvinceID": 1,
        "ProvinceName": "Distrito Nacional"
    },
    {
        "ProvinceID": 21,
        "ProvinceName": "San Pedro de Macorís"
    },
    {
        "ProvinceID": 22,
        "ProvinceName": "La Romana"
    },
    {
        "ProvinceID": 23,
        "ProvinceName": "La Altagracia"
    },
    {
        "ProvinceID": 24,
        "ProvinceName": "El Seibo"
    },
    {
        "ProvinceID": 25,
        "ProvinceName": "Hato Mayor"
    },
    {
        "ProvinceID": 31,
        "ProvinceName": "Duarte"
    },
    {
        "ProvinceID": 32,
        "ProvinceName": "Samaná"
    },
    {
        "ProvinceID": 33,
        "ProvinceName": "Maria Trinidad Sánchez"
    },
    {
        "ProvinceID": 34,
        "ProvinceName": "Salcedo"
    },
    {
        "ProvinceID": 41,
        "ProvinceName": "La Vega"
    },
    {
        "ProvinceID": 42,
        "ProvinceName": "Monseñor Nouel"
    },
    {
        "ProvinceID": 43,
        "ProvinceName": "Sánchez Ramirez"
    },
    {
        "ProvinceID": 51,
        "ProvinceName": "Santiago"
    },
    {
        "ProvinceID": 56,
        "ProvinceName": "Espaillat"
    },
    {
        "ProvinceID": 57,
        "ProvinceName": "Puerto Plata"
    },
    {
        "ProvinceID": 61,
        "ProvinceName": "Valverde"
    },
    {
        "ProvinceID": 62,
        "ProvinceName": "Monte Cristi"
    },
    {
        "ProvinceID": 63,
        "ProvinceName": "Dajabónn"
    },
    {
        "ProvinceID": 64,
        "ProvinceName": "Santiago Rodríguez"
    },
    {
        "ProvinceID": 71,
        "ProvinceName": "Azua"
    },
    {
        "ProvinceID": 72,
        "ProvinceName": "San Juan de la Maguana"
    },
    {
        "ProvinceID": 73,
        "ProvinceName": "Elías Piña"
    },
    {
        "ProvinceID": 81,
        "ProvinceName": "Barahona"
    },
    {
        "ProvinceID": 82,
        "ProvinceName": "Bahoruco"
    },
    {
        "ProvinceID": 83,
        "ProvinceName": "Independencia"
    },
    {
        "ProvinceID": 84,
        "ProvinceName": "Perdenales"
    },
    {
        "ProvinceID": 91,
        "ProvinceName": "San Cristóbal"
    },
    {
        "ProvinceID": 92,
        "ProvinceName": "Monte Plata"
    },
    {
        "ProvinceID": 93,
        "ProvinceName": "San José de Ocoa"
    },
    {
        "ProvinceID": 94,
        "ProvinceName": "Peravia"
    }
]