﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;

namespace AuthServer_IdentityServer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Title = "IdentityServer";

            BuildWebHost(args)
                
                .Run();
        }
        //        var builder = new ConfigurationBuilder()
        //.SetBasePath(env.ContentRootPath)
        //.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
        //.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
        //.AddEnvironmentVariables();

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration((webHostBuilderContext, configurationbuilder) =>
            {
                var environment = webHostBuilderContext.HostingEnvironment;
                configurationbuilder.AddJsonFile("appsettings.json", optional: true);

                configurationbuilder.AddEnvironmentVariables();
            }).UseStartup<Startup>()
              .UseSetting("detailedErrors", "true")
              //.UseIISIntegration()
              .CaptureStartupErrors(true)
              .Build();
    }
}