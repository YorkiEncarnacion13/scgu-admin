﻿(function () {
    $('#logoutbtn').on('click', () => window.location.replace('Logout'));

    var context = angular.module("AuthServerApp",
        //Dependencies
        [
            'ngRoute',
            'ngSanitize',
            'ngMaterial',
            'ngMessages',
            'md.data.table'
        ]);


    context.filter('capitalize', function () {
        return function (s) {
            return (angular.isString(s) && s.length > 0) ? s[0].toUpperCase() + s.substr(1).toLowerCase() : s;
        }
    });
    context.service('Helpers', [function () {
        const Helpers = {
            FlattenObject: function (ob) {
                var toReturn = {};
                for (var i in ob) {
                    if (!ob.hasOwnProperty(i)) continue;
                    if ((typeof ob[i]) == 'object') {
                        var flatObject = Helpers.FlattenObject(ob[i]);
                        for (var x in flatObject) {
                            if (!flatObject.hasOwnProperty(x)) continue;
                            toReturn[x] = flatObject[x];
                        }
                    } else toReturn[i] = ob[i];
                }
                return toReturn;
            },
            /// Creates an unique universal identifier
            GenerateUUID: () => { // Public Domain/MIT
                var d = new Date().getTime();
                if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
                    d += performance.now(); //use high-precision timer if available
                }
                return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                    var r = (d + Math.random() * 16) % 16 | 0;
                    d = Math.floor(d / 16);
                    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
                });
            }
        };
        return Helpers;
    }]);
    context.config(function ($routeProvider, $locationProvider, $sceDelegateProvider, $httpProvider) {
        $httpProvider.defaults.withCredentials = true;
        $routeProvider
            .when("/",
            { redirectTo: '/Aplicaciones' })
            .when("/Usuarios",
            {
                Title: 'AuthServer',
                templateUrl: '/assets/App/Templates/Users.html'
            })
            .when("/Aplicaciones",
            {
                Title: 'AuthServer',
                templateUrl: '/assets/App/Templates/Apps/AppsList.html'
            })
            .when("/Aplicaciones/NuevaAplicacion",
            {
                Title: 'AuthServer',
                templateUrl: '/assets/App/Templates/Apps/NewApp.html'
            })
            .when("/Aplicaciones/:appID",
            {
                Title: 'AuthServer',
                templateUrl: '/assets/App/Templates/Apps/App.html'
            }).when("/Usuarios/NuevoUsuario",
            {
                Title: 'AuthServer',
                templateUrl: '/assets/App/Templates/Apps/NewUser.html'
            }).when("/Usuarios/Buscar",
            {
                Title: 'AuthServer',
                templateUrl: '/assets/App/Templates/Users/UserSearch.html'
            }).when("/Usuario/:userID",
            {
                Title: 'AuthServer',
                templateUrl: '/assets/App/Templates/Users/User.html'
            })
            // MUST BE PLACED AT THE END
            .otherwise({ redirectTo: '/Aplicaciones' });
        $locationProvider.html5Mode(false).hashPrefix('!');
    });
    context.run(['$rootScope', '$location', ($rootscope, $location) => {
        $rootscope.$on('$routeChangeSuccess', function (event, current, previous) {
            var defaultTitle = 'Auth';
            $rootscope.Title = (current.$$route.Title) ? current.$$route.Title : defaultTitle;
        });
    }]);
})();