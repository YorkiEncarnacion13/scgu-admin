﻿(function () {
    'use strict';
    angular
        .module('AuthServerApp')
        .controller('AppController', AppController);
    AppController.$inject = ['$scope', 'API', '$routeParams', 'Helpers', '$mdDialog'];
    function AppController($scope, API, $routeParams, Helpers, $mdDialog) {
        const VIEW = this;

        console.warn('Helpers', Helpers);
        $scope.AppID = $routeParams.appID;

        const APPLICATION_STATUSES = {
            online: 2,
            offline: 3,
            onMaintenance: 4,
            Deactivated: 5
        };

        VIEW.APPLICATION_STATUSES = APPLICATION_STATUSES;


        VIEW.GetStatusDisplayName = (status) => {
            return Object.keys(APPLICATION_STATUSES).filter(k => APPLICATION_STATUSES[k] === status)[0];
        };

        VIEW.UpdateApplication = (application, ev) => {


            var confirm = $mdDialog.confirm()
                .title('Confirmación')
                .textContent('¿Estás seguro de querer modificar esta aplicación?')
                .targetEvent(ev)
                .ok('OK')
                .cancel('CANCELAR');

            $mdDialog.show(confirm).then(function () {
                debugger
                API.$helpers.ShowLoader();

                const DTO = {
                    Description: application.description,
                    DisplayName: application.displayName,
                    Uri: application.uri,
                    ApplicationStatus: application.applicationStatus,
                    ApplicationID: application.applicationID
                };

                API.Applications.Update(DTO).then(r => {
                    API.$helpers.HideLoader();
                    Load();
                }, onHttpError);

            }, function () {
                $scope.status = 'You decided to keep your debt.';
            });



        };

        VIEW.DisableApp = (ev) => {
            var confirm = $mdDialog.confirm()
                .title('¿Estás seguro de querer desactivar esta aplicación?')
                .textContent('Ningun usuario podra acceder a este sistema.')
                .targetEvent(ev)
                .ok('DESACTIVAR')
                .cancel('CANCELAR');

            $mdDialog.show(confirm).then(function () {
                API.apps.UpdateAppStatus($scope.AppID)
            }, function () {
                $scope.status = 'You decided to keep your debt.';
            });
        };


        const AppStates = { Normal: 1, Submiting: 2, Loading: 3, SearchingUser: 4, CreatingUser: 5, UpdatingUserRoles: 6, LoadingVM: 7 }
        const onHttpError = (e) => {
            API.$helpers.HideLoader();
            console.error('Un error ha evitado que ejecutemos esta acción, aquí los detalles:', e);
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .clickOutsideToClose(true)
                    .title('Ha sucedido un error.')
                    .textContent(`Un error ha evitado que ejecutemos esta acción, visita la consola para mas información.`)
                    .ariaLabel('Alerta')
                    .ok('OK')
            );
        };
        $scope.AppUserStatuses = {
            Normal: 2,
            Disabled: 3
        };
        $scope.DisableUser = (user, ev) => {
            console.log(user);
            const confirm = $mdDialog.confirm()
                .title('¿Desactivamos el usuario?')
                .textContent(`${user.firstName} no podrá acceder al sistema "${$scope.App.applicationID}".`)
                .targetEvent(ev)
                .ok('Desactivar')
                .cancel('Cancelar');

            $mdDialog.show(confirm).then(function () {
                API.Applications.DisableUser(user.applicationUserID).then((e) => {
                    user.appUserStatus = $scope.AppUserStatuses.Disabled;
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.body))
                            .clickOutsideToClose(true)
                            .title('Listo.')
                            .textContent(`El estatus de la cuenta de ${user.firstName} ha sido actualizado, Este usuario no podrá acceder a los servicios de esta aplicación. Para revertir esta acción, selecciona el botón [activar] en el despliegue de usuarios de esta misma ventana.`)
                            .ariaLabel('Alerta')
                            .ok('Entendido')
                            .targetEvent(ev)
                    );
                }, onHttpError);

            });
        };
        $scope.EnableUser = (user, ev) => {
            console.log(user);
            const confirm = $mdDialog.confirm()
                .title('¿Reactivamos el usuario?')
                .textContent(`${user.firstName} podrá acceder al sistema "${$scope.App.applicationID}".`)
                .targetEvent(ev)
                .ok('Activar')
                .cancel('Cancelar');

            $mdDialog.show(confirm).then(function () {
                API.Applications.EnableUser(user.applicationUserID).then((e) => {
                    user.appUserStatus = $scope.AppUserStatuses.Normal;

                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.body))
                            .clickOutsideToClose(true)
                            .title('Listo.')
                            .textContent(`El estatus de la cuenta de ${user.firstName} ha sido actualizado, Este usuario podrá acceder a los servicios de esta aplicación.`)
                            .ariaLabel('Alerta')
                            .ok('Entendido')
                            .targetEvent(ev)
                    );
                }, onHttpError);

            });
        };
        $scope.GetManifest = () => {
            window.open('/api/Applications/GetManifest?AppID=' + $scope.AppID)
        };
        $scope.AppStates = AppStates;
        $scope.NewAppUser = {};
        $scope.createUid = Helpers.GenerateUUID;
        console.warn('API', API, '$scope.AppID', $scope.AppID)
        console.warn('$routeParams', $routeParams);
        $scope.state = AppStates.LoadingVM;

        $scope.CreateAppUser = (appUser) => {
            if (!appUser) return;
            $scope.state = AppStates.CreatingUser;
            const RolesIDs = [];

            // Parse Logic, to create the List of RoleIds for the DTO
            if (angular.isObject(appUser.roles)) {
                $.each(appUser.roles, (k, v) => {
                    if (v === true) {
                        const ID = Number.parseInt(k);
                        if (angular.isNumber(ID))
                            RolesIDs.push(ID);
                    }
                });
                console.warn('Roles to add: ', RolesIDs);
            }

            const CreationDTO = {
                UserID: appUser.userID,
                ApplicationID: $scope.AppID,
                UserRolesIds: RolesIDs
            };


            console.warn('appuser', appUser);
            console.warn('CreationDTO', CreationDTO);

            const Promise = API.Applications.CreateApplicationUser(CreationDTO);

            if (Promise) {
                Promise.then((r) => {
                    $scope.state = AppStates.Normal;
                    $scope.App.Users = $scope.App.Users || [];
                    $scope.App.Users.push(appUser);
                }, (e) => {

                });
            } else $scope.state = AppStates.Normal;

        };
        $scope.SetRolesModalUser = (user) => {
            console.log('USSSSSER', user);

            const SelectedUserRoles =
                $.map(user.userRoles, (elem) => {
                    return {
                        applicationUserRoleID: elem.applicationUserRoleID,
                        userRoleID: elem.role.userRoleID,
                        userRoleName: elem.role.userRoleName,
                    };
                });

            const AppRoles =
                $.map($scope.App.userRoles, (el) => {
                    return {

                        userRoleID: el.userRoleID,
                        userRoleName: el.userRoleName,

                    };
                });

            let $Roles = [].concat(AppRoles);

            $.each(SelectedUserRoles, (Key, Value) => {
                Value.Selected = true;
                $Roles = _.without($Roles, _.findWhere($Roles, { userRoleID: Value.userRoleID }));
            });

            $Roles = $Roles.concat(SelectedUserRoles);

            console.warn('$Roles', $Roles);

            $scope.RolesModal = {
                Roles: $Roles,
                UserID: user.userID
            };
            $scope.RolesModalRoles = $Roles;

            console.log('SelectedUserRoles', SelectedUserRoles);
            console.log('AppRoles', AppRoles);

        };
        $scope.AddRole = (newUserRole) => {
            newUserRole.applicationID = $scope.App.applicationID;
            API.Roles.CreateAppRole(newUserRole).then((r) => {

                newUserRole.userRoleID = r.data.userRoleID;

                $scope.App.userRoles = $scope.App.userRoles || [];

                $scope.App.userRoles.push(newUserRole);
            }, onHttpError);
        };
        $scope.UpdateUserRoles = (userId) => {

            $scope.state = AppStates.UpdatingUserRoles;

            const user = _.findWhere($scope.App.Users, { userID: userId });

            const SelectedUserRoles =
                $.map(user.userRoles, (elem) => {
                    return {
                        applicationUserRoleID: elem.applicationUserRoleID,
                        userRoleID: elem.role.userRoleID,
                        userRoleName: elem.role.userRoleName,
                    };
                });

            // Returns the roles that needs to be updated in the database,
            // Each element contains a property called Selected, which states whether if the role must be set on or off 
            // for the user.

            const $RolesToUpdate =
                _.filter($scope.RolesModalRoles, (element) => {

                    const WasNotSelected = _.findWhere(SelectedUserRoles, { userRoleID: element.userRoleID }) == null;

                    const WasSelected = !WasNotSelected;

                    const isNotSelected = element.Selected == false;

                    if (WasSelected && isNotSelected) return true;

                    else if (element.Selected == true && WasNotSelected) return true;

                    else return false;
                });

            const RolesToTurnOff = _.where($RolesToUpdate, { Selected: false });

            const RolesToTurnOn = _.where($RolesToUpdate, { Selected: true });

            const IdsOfRolesToTurnOff =
                $.map(RolesToTurnOff, (v, k) => {
                    return v.userRoleID;
                });

            const IdsOfRolesToTurnOn =
                $.map(RolesToTurnOn, (v, k) => {
                    return v.userRoleID;
                });


            API.Applications.UpdateUserRoles({
                ApplicationID: $scope.AppID,
                UserID: userId,
                RolesToTurnOnIDs: IdsOfRolesToTurnOn,
                RolesToTurnOffIDs: IdsOfRolesToTurnOff
            }).then((r) => {
                $scope.state = AppStates.Normal;
            }, onHttpError);


            console.warn('IdsOfRolesToTurnOff', IdsOfRolesToTurnOff);
            console.warn('IdsOfRolesToTurnOn', IdsOfRolesToTurnOn);

        };

        $scope.GetUser = (UserID) => {
            $scope.FoundUserIsAlreadyAdded = false;
            $scope.NewAppUser = {};
            $scope.state = AppStates.SearchingUser;
            const promise = API.Users.GetUser(UserID);
            if (promise)
                promise.then((r) => {
                    if (angular.isObject(r.data)) {

                        if (_.findWhere($scope.App.Users, { userID: r.data.userID })) {
                            $scope.FoundUserIsAlreadyAdded = true;
                            $scope.state = AppStates.Normal;
                            return;
                        }

                        console.log('Get User', r.data,
                            'App.Users', $scope.App.Users);
                        $scope.NewAppUser = r.data;
                        $scope.UserNotFound = false;
                        $scope.state = AppStates.Normal;
                        console.log('$scope.NewAppUser', $scope.NewAppUser);
                    }
                    else {
                        $scope.UserNotFound = true;
                        $scope.state = AppStates.Normal;
                    }
                }, onHttpError);
            else $scope.state = AppStates.Normal;
        };




        function Load() {
            API.$helpers.ShowLoader();
            API.Applications.Get($scope.AppID).then((r) => {
                $scope.App = r.data.aplication;
                $scope.App.Users = r.data.users;
                console.warn('$scope.App', $scope.App);
                $scope.state = AppStates.Normal;
                API.$helpers.HideLoader();
            }, (er) => {
                $scope.state = AppStates.Normal;
                $.sweetModal({
                    content: 'Ha ocurrido un error',
                    icon: $.sweetModal.ICON_WARNING
                });
                console.error('err', er);
                API.$helpers.HideLoader();
            });
        }
        Load();
    }
})();