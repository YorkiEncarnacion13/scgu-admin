﻿(function () {
    'use strict';

    angular
        .module('AuthServerApp')
        .controller('AppsListController', AppsListController);

    AppsListController.$inject = ['$scope', 'API'];

    function AppsListController($scope, API) {
        const ControllerStates = { Normal: 1, Loading: 2 }
        $scope.currPage = 1;

        $scope.state = ControllerStates.Loading;
        $scope.States = ControllerStates;
        $scope.Limit = 10;
        $scope.page = 1;
        API.Applications.GetList($scope.currPage).then((r) => {
            console.warn('APPS', r.data);
            $scope.Apps = r.data.data;
            $scope.Total = r.data.qntity;
            $scope.state = ControllerStates.Normal;
        },
            (e) => {
                $scope.state = ControllerStates.Normal;
            });

        $scope.GetList = (page, qntity) => {
            API.Applications.GetList(page, qntity).then((r) => {
                console.warn('APPS', r.data);
                $scope.Apps = r.data.data;
                $scope.Total = r.data.qntity;
                $scope.state = ControllerStates.Normal;
            }, (e) => {
                $scope.state = ControllerStates.Normal;
            });
        }

    }
})();
