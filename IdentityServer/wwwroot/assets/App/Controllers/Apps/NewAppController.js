﻿(function () {
    'use strict';
    angular
        .module('AuthServerApp')
        .controller('NewAppController', NewAppController);
    NewAppController.$inject = ['$scope', 'API', '$location', '$http'];
    function NewAppController($scope, API, $location, $http) {
        console.warn('API', API)
        const AppStates = { Normal: 1, Submiting: 2, Submited: 3 }
        $scope.state = AppStates.Normal;
        $scope.AppStates = AppStates;
        $http.get('/api/Applications').then((r) => console.log(r), (r) => console.error(r))
        $scope.NewApp = {
            applicationName: null,
            displayName: '',
            description: null,
            isActive: false,
            userRoles: [],
            userAdmins: []
        };

        $scope.AddRole = (role) => {
            $scope.NewApp.userRoles.push(role);
            $scope.NewRole = {};
        };

        $scope.DeleteRole = (role) => {
            $scope.NewApp.userRoles = _.without($scope.NewApp.userRoles, role);
            console.warn('Current Roles', $scope.NewApp.userRoles);
            console.warn('Remove', role);
        };

        $scope.Register = () => {
            $scope.state = AppStates.Submiting;
            API.Applications.Post($scope.NewApp).then((r) => {
                console.log('new app', r);
                $scope.state = AppStates.Normal;
                $scope.NewApp.applicationID = r.data.applicationID;
                $.sweetModal({
                    content: 'La aplicacion ha sido registrada',
                    icon: $.sweetModal.ICON_SUCCESS,
                    buttons: [
                        {
                            label: 'Configurar',
                            classes: 'greenB',
                            action: () => $location.path('/Aplicacion/' + r.data.applicationID)
                        }]
                });
            }, (r) => {
                console.warn('error', r);
                $scope.state = AppStates.Normal;
            });
        };

       
    }
})();
