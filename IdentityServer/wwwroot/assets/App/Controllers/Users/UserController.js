﻿(function () {
    'use strict';

    angular
        .module('AuthServerApp')
        .controller('UserController', UserController);

    UserController.$inject = ['API', '$routeParams', '$mdDialog', '$route'];

    function UserController(API, $routeParams, $mdDialog, $route) {
        /* jshint validthis:true */
        var View = this;
        View.UserID = $routeParams.userID;
        const AppStates = {
            retrivingInfo: 2,
            normal: 3
        }
        const UserStatuses = {
            Normal: 2, Deactivated: 3, PendingPasswordReset: 4
        }
        View.AppStates = AppStates;
        View.UserStatuses = UserStatuses;
        View.AppState = AppStates.retrivingInfo;

        View.FetchCedulaInfo = (cedula) => {
            $http.get('/api/JCE/GetCedulaInfo/' + cedula).then(r => {
                console.warn('CEDU', r);
                VIEW.NewUser.FirstName = r.data.nombres;
                VIEW.NewUser.FirstLastName = r.data.apellido1;
                VIEW.NewUser.SecondLastName = r.data.apellido2;
                VIEW.NewUser.Gender = _.findWhere(VIEW.Genders, { cod: r.data.sexo }).id;
                VIEW.cedPhoto = r.data.rutaFoto
            }, ShowErrorMessage)
        }

        const ToggleUserStatus = (SuccessCallback, ErrorCallback) => {
            API.Users.ToggleUserStatus(View.UserID).then((r) => {
                if (angular.isFunction(SuccessCallback))
                    SuccessCallback(r);
            }, (e) => {
                if (angular.isFunction(ErrorCallback))
                    ErrorCallback(e);
            });
        }


        View.Deactivate = (ev) => {
            const confirm = $mdDialog.confirm()
                .title('¿Desactivamos el usuario?')
                .textContent(`${View.user.firstName} no podrá acceder a ningun sistema que utilize este servidor de identidades.`)
                .targetEvent(ev)
                .ok('Desactivar')
                .cancel('Cancelar');

            $mdDialog.show(confirm).then(function () {
                ToggleUserStatus(() => {
                    View.user.status = UserStatuses.Deactivated;
                });
            }, function () {
            });
        };
        View.Activate = (ev) => {
            const confirm = $mdDialog.confirm()
                .title('¿Activamos el usuario?')
                .textContent(`${View.user.firstName} podrá acceder a los sistema que utilize este servidor de identidades y l@ tengan registrado como usuario.`)
                .targetEvent(ev)
                .ok('Activar')
                .cancel('Cancelar');

            $mdDialog.show(confirm).then(function () {
                ToggleUserStatus(() => {
                    View.user.status = UserStatuses.Normal;
                });
            }, function () {
            });
        };

        View.Update = (User) => {
            API.Users
                .Update(User)
                .then((r) => {
                    console.log('OK');
                    $route.reload()
                }, (e) => {
                    console.error('ERR', e)
                })
        }
        View.GetDepartments = (searchTermn) => {
            API.Departments.GetDepartments(searchTermn).then((r) => {
                console.warn('r', r)
                View.Departments = r.data;

            }, alert);
        }
        API.Users.GetCreatedUserViewModel(View.UserID).then((r) => {
            console.log('r', r)
            View.countries = r.data.countries;
            View.user = r.data.user;
            View.user.birthDate = moment(View.user.birthDate).toDate();
            View.municipalities = r.data.municipalities;
            View.provinces = r.data.provinces;
            View.sectors = r.data.sectors;
            View.AppState = AppStates.normal;

            View.Departments = [r.data.user.department];

        }, (e) => console.log(e))

        activate();

        function activate() { }
    }
})();
