﻿(function () {
    'use strict';
    angular
        .module('AuthServerApp')
        .factory('API', factory);
    factory.$inject = ['$http','$mdDialog'];
    function factory($http, $mdDialog) {

        function ShowLoader() {
            $mdDialog.show({
                template: ` 
<div layout="column"  
     layout-align="center center"
     style="padding-top: 20px;padding-right: 40px;padding-left: 40px;padding-bottom: 32px;">
     <h6>Cargando</h5>
     <md-divider></md-divider>
     <md-progress-circular md-mode="indeterminate"></md-progress-circular>
</div>`,
                parent: angular.element(document.body),
                clickOutsideToClose: false,
            });
        }
        function ShowError() {
            $mdDialog.show({
                template: ` 
<div layout="column"  
     layout-align="center center"
     style="padding-top: 20px;padding-right: 40px;padding-left: 40px;padding-bottom: 32px;">
     <h3>Ha ocurrido un error</h3>
     <md-divider></md-divider>
</div>`,
                parent: angular.element(document.body),
                clickOutsideToClose: false,
            });
        }
        function HideLoader() {
            $mdDialog.hide();
        }
        const showSavedChangesMessage = () => {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .clickOutsideToClose(true).title('¡Listo!')
                    .textContent('Todos los cambios han sido salvados.')
                    .ariaLabel('Alerta').ok('Ok')
            );
        }


        const API = {
            Users: {
                CreateNewUser: (UserDTO) => {
                    if (angular.isUndefined(UserDTO)) throw 'User Dto is invalid API->USERS->CreateNewUser';
                    return $http.post('api/Users/CreateNewUser', UserDTO);
                },
                Update: (User) => {
                    return $http.put('/api/Users/Update', User);
                },
                IsEmailValid: (email) => {
                    if (angular.isString(email))
                        return $http.get('api/Users/IsEmailValid/' + email);
                },
                IsIDValid: (ID) => {
                    if (angular.isString(ID))
                        return $http.get('api/Users/IsIDValid/' + ID);
                },
                GetUser: (UserId) => {
                    if (!UserId) return;
                    return $http.get(`api/Users/GetUser/${UserId}`)
                },
                GetUsersFiltered: (Page, Qntity, SearchTerm) => {
                    Page = Page || 1;
                    Qntity = Qntity || 5;

                    if (SearchTerm)
                        return $http.get(`api/Users/GetUsersFiltered?Page=${Page}&Qntity=${Qntity}&SearchTerm=${SearchTerm}`);
                },
                GetCreatedUserViewModel: (UserID) => {
                    if (UserID)
                        return $http.get('api/Users/GetCreatedUserViewModel/' + UserID)
                },
                ToggleUserStatus: (userID) => {
                    if (userID)
                        return $http.put(`api/Users/ToggleUserStatus/${userID}`)
                },
                GetUsersPaginated: (Page, Qntity) => {
                    Page = Page || 1;
                    Qntity = Qntity || 10;
                    const route = `api/Users/GetUsers?Page=${Page}&Qntity=${Qntity}`;
                    return $http.get(route);
                },
                //GetCedulaInfo : ()
            },
            JCE: {
                GetCedulaInfo: cedula => {
                    return $http.get('/api/JCE/GetCedulaInfo/' + cedula);
                }
            },

            Applications: {
                Post: (ApplicationCreateDTO) => $http.post('/api/Applications', ApplicationCreateDTO),
                Update: (app) => $http.put('/api/Applications', app),
                GetList: (page, qntity) => {
                    page = page || 1;
                    qntity = qntity || 10;
                    return $http.get(`/api/Applications/GetList?page=${page}&qntity=${qntity}`)
                },
                Get: (AppID) => {
                    if (!AppID) return;
                    return $http.get(`/api/Applications/GetAppInfo/${AppID}`)
                },
                CreateApplicationUser: (newApplicationUser) => {
                    if (!angular.isObject(newApplicationUser)) return;
                    return $http.post('/api/Applications/CreateApplicationUser', newApplicationUser);
                },
                EnableUser: (AppUserID) => $http.put('/api/Applications/EnableUser?AppUserID=' + AppUserID),
                DisableUser: (AppUserID) => $http.put('/api/Applications/DisableUser?AppUserID=' + AppUserID),
                TurnOffRoles: (Params) => Params ? $http.put('api/Applications/UserRoles/TurnOffRoles', Params) : null,
                TurnOnRoles: (Params) => Params ? $http.put('api/Applications/UserRoles/TurnOnRoles', Params) : null,
                UpdateUserRoles: (Params) => Params ? $http.put('api/Applications/UserRoles/UpdateUserRoles', Params) : null,
                UpdateAppStatus: (ApplicationID, applicationStatuses) =>
                    $http.put(`api/Applications/UpdateAppStatus?ApplicationID=${ApplicationID}&applicationStatuses=${applicationStatuses}`)
            },
            Provinces: {
                Get: () => $http.get('/api/Provinces')
            },
            Municipalities: {
                GetProvinceMunicipalities: (ProvinceID) => {
                    return $http.get('/api/Municipalities/GetProvinceMunicipalities/' + ProvinceID)
                }
            },
            Sectors: {
                GetMunicipalitySectors: (MunicipalityID) => {
                    return $http.get('/api/Sectors/GetMunicipalitySectors/' + MunicipalityID)
                }
            },
            Countries: {
                Get: () => $http.get('/api/Countries')
            },
            JobTitles: {
                GetJobTitles: (searchTermn) => {
                    if (angular.isUndefined(searchTermn)) return;
                    if (searchTermn.length > 2 && angular.isString(searchTermn))
                        return $http.get('/api/JobTitles/GetJobTitles/' + searchTermn);
                }
            },
            Departments: {
                Get: () => $http.get('/api/Departments'),
                GetDepartments: (searchTerm) => {
                    if (angular.isUndefined(searchTerm)) return;
                    if (searchTerm.length > 2 && angular.isString(searchTerm))
                        return $http.get('/api/Departments/GetDepartments/' + searchTerm);
                }
            },
            Locations: {
                Get: () => $http.get('/api/Locations'),
                GetLocations: (searchTermn) => {
                    if (angular.isUndefined(searchTermn)) return;
                    if (searchTermn.length > 2 && angular.isString(searchTermn))
                        return $http.get('/api/Locations/GetLocations/' + searchTermn)
                }
            },
            Roles: {
                CreateAppRole: (DTO) => {
                    if (!DTO) return;
                    return $http.post('api/Roles/CreateAppRole', DTO)
                }
            },
            $helpers: {
                showSavedChangesMessage,
                HideLoader,
                ShowLoader,
                ShowError
            },
        }
        console.log('API Definition', API);


       





        return API;


    }
})();