﻿(function () {
    'use strict';

    angular
        .module('AuthServerApp')
        .controller('RecuperarPasswordController', RecuperarPasswordController);

    RecuperarPasswordController.$inject = ['$location', '$http', 'ValidatorsService','$mdDialog'];

    function RecuperarPasswordController($location, $http, ValidatorsService, $mdDialog) {
        /* jshint validthis:true */
        const VIEW = this;
        function ShowLoader() {
            $mdDialog.show({
                template: ` 
                <div layout="column"  
                     layout-align="center center"
                     style="padding-top: 20px;padding-right: 40px;padding-left: 40px;padding-bottom: 32px;">
                     <h6>Cargando</h5>
                     <md-divider></md-divider>
                     <md-progress-circular md-mode="indeterminate"></md-progress-circular>
                </div>`,
                parent: angular.element(document.body),
                clickOutsideToClose: false,
            });
        }
        function HideLoader() {
            $mdDialog.hide();
        }
        const ShowErrorMessage = (ERROR) => {
            $mdDialog.hide();
            console.error('An error ocurred.', ERROR);
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .clickOutsideToClose(true).title('¡Oops!')
                    .textContent('Ha ocurrido un error al intentar procesar tu solicitud, comunicate con la ext 6409.')
                    .ariaLabel('Alerta').ok('Ok')
            );
        }
     
        VIEW.NewUser = {};
        VIEW.title = 'SignupController';
        console.warn('ValidatorsService', ValidatorsService)
        activate();
        const Genders = [
            { cod: 'M', Nombre: 'Masculino', id: 2 },
            { cod: 'F', Nombre: 'Femenino', id: 3 }
        ];
        VIEW.Genders = Genders;
        VIEW.isCedula = ValidatorsService.isCedula;


        $http.get('/api/Provinces').then((r) => {
            console.log(r);
            VIEW.Provinces = r.data;
        }, (r) => alert(r));

        VIEW.LoadMunicipios = (provinceID) => {
            VIEW.Municipalities = [];
            $http.get('/api/Municipalities/GetProvinceMunicipalities/' + provinceID)
                .then((r) => {
                    console.log(r);
                    VIEW.Municipalities = r.data;
                }, (r) => {
                    ShowErrorMessage(r)
                    console.error('error', r);
                })
        }
        
        $http.get('api/Locations').then(r => {
            VIEW.Locations = r.data;
            console.log('VIEW.Locations', VIEW.Locations)
        });
        VIEW.LoadSectors = (MunicipalityID) => {
            VIEW.Sectors = [];
            $http.get('/api/Sectors/GetMunicipalitySectors/' + MunicipalityID).then((r) => {
                console.log('sectors', r);
                VIEW.Sectors = r.data;
            }, (r) => ShowErrorMessage(r))
        }

        VIEW.FetchCedulaInfo = (cedula) => {
            $http.get('/api/JCE/GetCedulaInfo/' + cedula).then(r => {
                console.warn('CEDU', r);
                VIEW.NewUser.FirstName = r.data.nombres;
                VIEW.NewUser.FirstLastName = r.data.apellido1;
                VIEW.NewUser.SecondLastName = r.data.apellido2;
                VIEW.NewUser.Gender = _.findWhere(VIEW.Genders, { cod: r.data.sexo }).id;
                VIEW.cedPhoto = r.data.rutaFoto
            });
        }
        VIEW.JobTitle = {
            JobTitles: [],
            onTextChange: (searchTermn) => {
                $http.get('/api/JobTitles/GetJobTitles/' + searchTermn).then((r) => {
                    console.warn('r', r)
                    VIEW.JobTitle.JobTitles = r.data;
                }, (e) => console.warn(e));
            },
            LookUpText: '',
            cacheDisabled: true,
            selectedTitle: null
        };
        VIEW.LoadDepartments = () => {
            return $http.get('/api/Departments').then(r => {
                console.log('R', r);
                VIEW.Departments = r.data;
            }, ShowErrorMessage)
        }
        VIEW.LoadDepartments();

        //VIEW.Submit = DTO => {
        //    ShowLoader();
        //    $http.post('api/Users/CreateNewUser', DTO).then(r => {
        //        HideLoader();
        //        VIEW.Succesfull = true;
        //        $mdDialog.show(
        //            $mdDialog.alert()
        //                .parent(angular.element(document.body))
        //                .clickOutsideToClose(false).title('¡Solicitud Enviada!')
        //                .textContent(`Hemos registrado tu solicitud y estaremos procesandola. Tu contaseña temporal es: ${r.data.clearTextPassword}. Revisa tu correo electrónico para mas detalles.`)
        //                .ariaLabel('Alerta').ok('Ok')
        //        );
        //    }, ShowErrorMessage)
        //}


        VIEW.Submit = (RecuperarPassword) => {

            $http.get('api/Users/FindByEmailOrID/' + RecuperarPassword).then(r => {
                if (r.data != "") {
                    ShowLoader();
                    $http.post('api/Users/ResetPassword', r.data).then(r => {
                        HideLoader();
                        VIEW.Succesfull = false;
                        $mdDialog.show(
                            $mdDialog.alert()
                                .clickOutsideToClose(false).title('¡Solicitud Enviada!')
                                .textContent(`Hemos registrado tu solicitud y estaremos procesandola. Revisa tu correo electrónico para mas detalles.`)
                                .ariaLabel('Alerta').ok('Ok'));

                        VIEW.RecuperarPassword = ""

                         //window.location = "/RecuperarPassword";

                    }, ShowErrorMessage);
                    VIEW.LoginError = false;
                }
                else {
                    VIEW.LoginError = true;
                }
            });


        }

        function activate() { }
    }
})();
