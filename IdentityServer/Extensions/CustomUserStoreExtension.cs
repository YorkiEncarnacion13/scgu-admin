﻿using AuthServer.Repositories;
using AuthServer.Services;
using Repositories.Interfaces;

namespace Microsoft.Extensions.DependencyInjection
{

    public static class CustomIdentityServerBuilderExtensions
    {
        public static IIdentityServerBuilder AddCustomUserStore(this IIdentityServerBuilder builder)
        {
            builder.Services.AddSingleton<IUserRepository, UserRepository>();

            builder.AddProfileService<ProfileService>();

            builder.AddResourceOwnerValidator<ResourceOwnerPasswordValidator>();

            return builder;
        }
    }
}