﻿using IdentityServer4.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;

namespace AuthServer_IdentityServer
{
    public class Config
    {
        // PRODUCTION
          //public const string AppID = "MICM_IDENTITY_SERVER";
          //  public const string SGU_URI = "http://scgu.mic.gob.do/";


        //DEV
        public const string SGU_URI = "http://localhost:5000/";
        public const string AppID = "SCGU-Development";

        // scopes define the API resources in your system
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("api1", "My API")
            };
        }
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };
        }
        public static Random random = new Random();
        public static string GetLoginImage()
        {
            //string[] Images = {
            //    "/assets/static/images/LoginImages/loginImage.jpg",
            //    "/assets/static/images/LoginImages/BanderaDominicana.jpg",
            //    "/assets/static/images/LoginImages/palacio.jpg",
            //    "/assets/static/images/LoginImages/zonaColonial.jpg",
            //    "/assets/static/images/LoginImages/banderaPintura.jpg",
            //    "/assets/static/images/LoginImages/Dominican-Republic-Flag.jpg",
            //    "/assets/static/images/LoginImages/SD_Nocturno.jpg",
            //    "/assets/static/images/LoginImages/zcolonial.png",
            //     "/assets/static/images/LoginImages/santiagomonumento.jpg",
            //     "/assets/static/images/LoginImages/RD_Space.jpg",
            //       "/assets/static/images/LoginImages/REP-DOM-HD.jpg"
            //};
            string[] ChristmasImages = {
                //"/assets/static/images/LoginImages/christmas/ch1.jpg",
                "https://source.unsplash.com/collection/4652200"
                //"/assets/static/images/LoginImages/christmas/ch2.jpg"

            };


            if (DateTime.Now.Month == 12)
            {


            }

            return ChristmasImages[random.Next(0, ChristmasImages.Length)];
        }
        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "alice",
                    Password = "password"
                },
                new TestUser
                {
                    SubjectId = "2",
                    Username = "bob",
                    Password = "password"
                }
            };
        }
    }
}