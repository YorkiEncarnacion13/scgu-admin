﻿using AuthServerHelpers;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
namespace AuthServer.Services
{
    public class ProfileService : IProfileService
    {
        protected readonly IUserRepository _UserRepository;
        public ProfileService(IUserRepository userRepository)
        {
            _UserRepository = userRepository;
        }
        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            // Get the user Id
            var CurrentSubject = context.Subject.Claims.FirstOrDefault(c => c.Type == "sub");

            if (CurrentSubject != null)
            {
                // Get user from repository
                var user = await _UserRepository.FindByUserIDAsync(CurrentSubject.Value, true);




                // Adding the user's claims
                var claims = new List<Claim>
                {
                new Claim(AuthServerClaimsTypes.UserID,user.UserID ),
                new Claim(AuthServerClaimsTypes.FirstName,user.FirstName ),
                new Claim(AuthServerClaimsTypes.FirstLastName,user.FirstLastName ),
                new Claim(AuthServerClaimsTypes.Email,user.Email ),
                };

                if (string.IsNullOrEmpty(user.SecondName) == false)
                    claims.Add(new Claim(AuthServerClaimsTypes.SecondName, user.SecondName));

                if (string.IsNullOrEmpty(user.SecondLastName) == false)
                    claims.Add(new Claim(AuthServerClaimsTypes.SecondLastName, user.SecondLastName));



                //using (var UnitOfWork = new UnitOfWork())
                //{
                //    var AppsRoles = await UnitOfWork.ApplicationUserRepository.GetUsersRoles(user.UserID);

                //    if (AppsRoles?.Count > 0)
                //        claims.AddRange(AppsRoles.Select(ar => new Claim(type: AuthServerClaimsTypes.AppRole,
                //                                                         value: ar.RoleID.ToString() + "?" + ar.AppID)
                //                                         ).ToList());
                //}
                if (user.Department != null)
                {
                    claims.Add(new Claim(AuthServerClaimsTypes.DepartmentID, user.Department.DepartmentID.ToString()));

                    if (!string.IsNullOrEmpty(user.Department.DepartmentName))
                        claims.Add(new Claim(AuthServerClaimsTypes.DepartmentName, user.Department.DepartmentName));

                    if (!string.IsNullOrEmpty(user.Department.Codigo))
                        claims.Add(new Claim(AuthServerClaimsTypes.DepartmentCode, user.Department.Codigo));
                }
                context.IssuedClaims = claims;
            }


        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            try
            {
                //var CurrentSubjectID = context.Subject.FindFirst("UserID").Value;
                var userid = context.Subject.Claims.FirstOrDefault(c => c.Type == "sub");

                if (userid == null) context.IsActive = false;

                var user = await _UserRepository.FindByUserIDAsync(userid.Value);

                context.IsActive = user != null && user.Status != Models.Auth.UserStatuses.Deactivated;
            }
            catch (System.Exception)
            {
                context.IsActive = false;
            }
        }
    }
}
