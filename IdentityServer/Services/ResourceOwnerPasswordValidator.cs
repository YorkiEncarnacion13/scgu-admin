﻿using IdentityModel;
using IdentityServer4.Validation;
using Repositories.Interfaces;
using System.Threading.Tasks;

namespace AuthServer.Services
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        private readonly IUserRepository _UserRepository;
        public ResourceOwnerPasswordValidator(IUserRepository userRepository)
        {
            this._UserRepository = userRepository;
        }
        public Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            if (_UserRepository.ValidateCredentials(context.UserName, context.Password))
            {
                var User = _UserRepository.FindByUserID(context.UserName);
                context.Result = new GrantValidationResult(User.UserID, OidcConstants.AuthenticationMethods.Password);
            }
            return Task.FromResult(0);
        }
    }
}
