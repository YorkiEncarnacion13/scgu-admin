﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace AuthServer.Services
{
    public class JCERestfullClient
    {

        public class JCE_WEBSERVICE_RESULT
        {
            public string Cedula { get; set; }
            public string nombres { get; set; }
            public string apellido1 { get; set; }
            public string apellido2 { get; set; }
            public string RutaFoto { get; set; }
            public string EstadoCivil { get; set; }
            public string LugarNacimiento { get; set; }
            public string NombreCompleto { get; set; }
            public string Apellidos { get; set; }
            public string fecha_nac { get; set; }
            public string sexo { get; set; }
            public string direccion { get; set; }
            public string telefono { get; set; }
        }
        public static async Task<JCE_WEBSERVICE_RESULT> GetCedulaInfoAsync(string Identifier)
        {
            var client = new HttpClient();
            var route = $"http://mic-svr-app:2020/WebServices/Get_JCE_Info?Cedula={Identifier}";
            var Result = await client.GetAsync(route);

            if (!Result.IsSuccessStatusCode)
                return null;

            else
            {
                var stringContent = await Result.Content.ReadAsStringAsync();
                var serialized = JsonConvert.DeserializeObject<JCE_WEBSERVICE_RESULT>(stringContent);

                return serialized;
            }
        }
    }
}
