﻿using ERP.DAL.Repositories;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Stores;
using System.Threading.Tasks;
using Brawn.Helpers;
namespace AuthServer.Services
{

    public class ClientsStore : IClientStore
    {
        public async Task<Client> FindClientByIdAsync(string clientId)
        {
            //int AppID;
            //if (int.TryParse(clientId, out AppID) == false)
            //    throw new ArgumentException("The provided ClientID does not seem to be a propper AuthServer ApplicationID", "clientId");
            //List<Client> InMemoryClients = new List<Client>
            //{
            //     new Client
            //    {
            //        ClientId = "MICM_IDENTITY_SERVER",
            //        ClientName = "Sistema de Tokens del Ministerio de Industria, Comercio y Mipymes",
            //        AllowedGrantTypes = GrantTypes.Implicit,
            //        RequireConsent = false,
            //        RedirectUris = { "http://localhost:5000/signin-oidc" },
            //        PostLogoutRedirectUris = { "http://localhost:5000/signout-callback-oidc" },
            //        AllowedScopes =
            //        {
            //            IdentityServerConstants.StandardScopes.OpenId,
            //            IdentityServerConstants.StandardScopes.Profile
            //        }

            //    }
            //};
            //var inMemoryClient = Config.GetClients().SingleOrDefault(c => c.ClientId == "MICM_IDENTITY_SERVER");
            //if (inMemoryClient != null)
            //    return inMemoryClient;


            string SGU_APP_CLIENTID = "SCGU_$APPLICATION$";
            if (clientId == SGU_APP_CLIENTID)
            {
                return new Client
                {
                    ClientId = SGU_APP_CLIENTID,
                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    ClientSecrets =
                    {
                        new Secret("$CGU_$APPLICATION$ecure".Sha256())
                    },
                    AllowedScopes = { IdentityServerConstants.StandardScopes.OpenId, IdentityServerConstants.StandardScopes.Profile }
                };
            }

            UnitOfWork unitofwork = new UnitOfWork();

            var Client = await unitofwork.ApplicationRepository.GetByIdAsync(clientId);

            if (Client.isNull())
                return null;

            return new Client
            {
               

                ClientId = Client.ApplicationID,
                ClientName = Client.ApplicationID,
                ClientUri = Client.Uri,
                RequireConsent = Client.RequiresConcent,
                AllowOfflineAccess = true,
                AllowedGrantTypes = (Client.GrantTypes == Models.System.GrantTypes.Hybrid) ? GrantTypes.Hybrid :
                (Client.GrantTypes == Models.System.GrantTypes.ClientCredentials) ? GrantTypes.ClientCredentials : GrantTypes.Implicit,
                RedirectUris = { Client.RedirectUri },
                PostLogoutRedirectUris = { Client.PostLogoutRedirectUri },
                AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile
                    }
            };
        }
    }
}
