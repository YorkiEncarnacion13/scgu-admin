﻿using jsreport.AspNetCore;
using jsreport.Binary;
using jsreport.Local;
using jsreport.Types;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AuthServer.Controllers
{
    public static class ExtHelp
    {
        public static string Join(this string[] values, string separator) => string.Join(separator, values);

        public static string Times(this string val, int qntity)
        {
            string ret = string.Empty;
            for (int i = 0; i < qntity; i++)
                ret += val;
            return ret;
        }
        /// <summary>
        /// Masks a string, where the mask is the value which the character replaced will be changed to.
        /// CharPCT stipulates how much of the string will be masked, ranged from 1-10, where 1 = 10% and so on.
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="Mask"></param>
        /// <param name="CharPCT"></param>
        /// <returns></returns>
        public static string Mask(this string Value, string Mask, int pctToShow)
        {
            if (pctToShow > 100)
                throw new ArgumentOutOfRangeException("pctToShow", "The provided percentage exceeds the maximum value (100), received " + pctToShow);
            var asArray = Value.ToCharArray();
            var pctToHide = (asArray.Length * pctToShow) / 100;
            var seen = asArray.Take(pctToHide).Select(d => d.ToString()).ToList();
            var visible = seen.ToArray().Join("");
            var hidden = string.Join("", asArray.Skip(pctToHide).ToArray().Select(d => Mask).ToList());
            return visible + hidden;
        }
    }
    public class ReportsController : Controller
    {
        IRazorViewEngine _viewEngine;
        IHttpContextAccessor _httpContextAccessor;
        ILocalWebServerReportingService _reportingService;
        public ReportsController(IRazorViewEngine viewEngine, IHttpContextAccessor httpContextAccessor)
        {
            _viewEngine = viewEngine;
            _httpContextAccessor = httpContextAccessor;
            _reportingService = new LocalReporting()
                                      .UseBinary(JsReportBinary.GetBinary())
                                      .AsWebServer()
                                      .Create();

            Task.WaitAll(_reportingService.StartAsync());
        }

        public string Render<TModel>(string viewPath, TModel model)
        {
            var viewEngineResult = _viewEngine.GetView("~/", viewPath, false);

            if (!viewEngineResult.Success)
            {
                throw new InvalidOperationException($"Couldn't find view {viewPath}");
            }

            var view = viewEngineResult.View;

            using (var output = new StringWriter())
            {
                var viewContext = new ViewContext();
                viewContext.HttpContext = _httpContextAccessor.HttpContext;
                viewContext.ViewData = new ViewDataDictionary<TModel>(new EmptyModelMetadataProvider(), new ModelStateDictionary())
                { Model = model };
                viewContext.Writer = output;

                view.RenderAsync(viewContext).GetAwaiter().GetResult();

                return output.ToString();
            }
        }

        public IActionResult Index()
        {
            return View();
        }
        public class NewUserReportParams
        {
            public string Email { get; set; }
            public string Cedula { get; set; }
            public string Password { get; set; }
        }
        [HttpPost(template: "Reports/NewUserReport")]
        public async Task<IActionResult> NewUserReport([FromBody]NewUserReportParams reportParams)
        {
            try
            {

                string email = reportParams.Email;
                var EmailMask = email.Split('@');
                EmailMask[0] = EmailMask[0].Mask("*", 60);
                reportParams.Email = EmailMask.Join("@");
                reportParams.Cedula = reportParams.Cedula.Mask("*", 50);



                string reportContent = Render("Views/Reports/NewUserReport.cshtml", reportParams);


                var report = await _reportingService.RenderAsync(new RenderRequest
                {
                    Template = new Template
                    {
                        Content = reportContent,
                        Engine = Engine.None,
                        Recipe = Recipe.PhantomPdf
                    }
                });

                return File(report.Content, report.Meta.ContentType);
            }
            catch (System.Exception ee)
            {
                return BadRequest(ee);
                throw;
            }
        }
    }
}