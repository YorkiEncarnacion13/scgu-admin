﻿using AuthServer.ViewModels;
using AuthServerHelpers.Extensions;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AuthServer.Controllers
{
    public class UserLoginParams
    {
        public string Identifier { get; set; }
        public string Password { get; set; }
        public string ReturnUrl { get; set; }

        public string NewPassword { get; set; }
        public string NewPasswordToken { get; set; }



        public string Browser { get; set; }
        public string Browser_Version { get; set; }
        public string OS { get; set; }
        public string OS_Version { get; set; }
        public string Language { get; set; }
        public string Screen { get; set; }



    }
    public static class APP_ROLES
    {
        public const int ADMIN = 1003;
    }
    public class HomeController : Controller
    {
        private readonly IIdentityServerInteractionService _interaction;
        private readonly IClientStore _clientStore;
        private readonly IAuthenticationSchemeProvider _schemeProvider;
        private readonly IEventService _events;
        public HomeController(IIdentityServerInteractionService interaction,
            IClientStore clientStore,
            IAuthenticationSchemeProvider schemeProvider,
            IEventService events)
        {
            _interaction = interaction;
            _clientStore = clientStore;
            _schemeProvider = schemeProvider;
            _events = events;
        }

        [HttpGet, Route("/"), Authorize]
        public async Task<IActionResult> Index()
        {
            // var client = new AuthServerClient(Config.AppID, Config.SGU_URI);

            // var user = User.AsMicmUser()
            //                .SetApp(Config.AppID)
            //                .SetAuthority(Config.SGU_URI);

            //client.

            //var SCGU_Client = new AuthServerClient(Config.AppID, Config.SGU_URI);

            //var rees = await SCGU_Client.GetApplicationUsers("MICM POA");

            //var res = await SCGU_Client.GetUsersByRoleAsync("MICM_IDENTITY_SERVER", APP_ROLES.ADMIN);

            // var userInfo = await client.GetUsersAsync(user.UserID);

            // var result = await client.GetUsersAsync("40200386338", "00109772971", "324564321");

            return View(User.AsMicmUser());
        }
        [Authorize, HttpGet(template: "Logout")]
        public async Task<IActionResult> Logout()
        {
            try
            {
                var user = User.AsMicmUser();
                Request.Cookies = null;
                await HttpContext.SignOutAsync("Cookies");

                await HttpContext.SignOutAsync("oidc");
                return Redirect("/");
            }
            catch (System.Exception e)
            {

                return BadRequest(e);
            }
        }
      
        public async Task<IActionResult> Error(string errorId)
        {
            var vm = new ErrorViewModel();

            // retrieve error details from identityserver
            var message = await _interaction.GetErrorContextAsync(errorId);

            if (message != null)
            {
                vm.Error = message;
            }

            return View("Error", vm);
        }

    }
}