﻿using ERP.DAL.Repositories;
using Microsoft.AspNetCore.Mvc;
using Models.Auth;
using System.Collections.Generic;
namespace AuthServer.Controllers.api
{
    [Produces("application/json")]
    public class LocationsController : Controller
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        [HttpGet(template: "api/Locations")]
        public IEnumerable<Location> Get() => unitOfWork.LocationRepository.GetAll();

        [HttpGet(template: "api/Locations/GetLocations/{SearchTerm}")]
        public IEnumerable<Location> GetLocations(string SearchTerm) => unitOfWork.LocationRepository.SearchFor(SearchTerm);

    }
}