﻿using AuthServer.Services;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Threading.Tasks;

namespace AuthServer.Controllers.api
{
    [Produces("application/json")]
    //[Route("api/Sectors")]
    public class JCEController : Controller
    {

        [HttpGet(template: "api/JCE/GetCedulaInfo/{cedula}"), ResponseCache(Duration = 60 * 100)]
        public async Task<IActionResult> GetCedulaInfo(string cedula)
        {
            try
            {
           
                var info = await JCERestfullClient.GetCedulaInfoAsync(cedula);
                return Ok(info);
            }
            catch (System.Exception ee)
            {

                if (ee.InnerException != null) return StatusCode(500);
                return BadRequest();
            }
        }
        [HttpGet(template: "api/JCE/GetImage/{cedula}")/*, ResponseCache(Duration = 60 * 100)*/]
        public async Task<IActionResult> GetImage(string cedula)
        {
            try
            {
                var info = await JCERestfullClient.GetCedulaInfoAsync(cedula);

                return Redirect(info.RutaFoto);



                var res = getImageFromUrl(info.RutaFoto);

               
                return File(res, "image/*");

            }
            catch (System.Exception ee)
            {

                if (ee.InnerException != null) return StatusCode(500);
                return BadRequest();
            }
        }
        public byte[] getImageFromUrl(string url)
        {
            System.Net.HttpWebRequest request = null;
            System.Net.HttpWebResponse response = null;
            byte[] b = null;

            request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
            response = (System.Net.HttpWebResponse)request.GetResponse();

            if (request.HaveResponse)
            {
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Stream receiveStream = response.GetResponseStream();
                    using (BinaryReader br = new BinaryReader(receiveStream))
                    {
                        b = br.ReadBytes(500000);
                        br.Close();
                    }
                }
            }

            return b;
        }
    }
}