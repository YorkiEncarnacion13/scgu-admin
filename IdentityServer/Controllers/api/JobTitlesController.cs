﻿using ERP.DAL.Repositories;
using Microsoft.AspNetCore.Mvc;
using Models.Auth;
using System.Collections.Generic;
namespace AuthServer.Controllers.api
{
    [Produces("application/json")]
    public class JobTitlesController : Controller
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        [HttpGet(template: "api/JobTitles/GetJobTitles/{searchTermn}")]
        public IEnumerable<JobTitle> GetJobTitles(string searchTermn)
        {
            var JobTitles = unitOfWork.JobTitleRepository.SearchFor(searchTermn);

            return JobTitles;
        }
    }
}