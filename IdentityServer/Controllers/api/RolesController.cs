﻿using ERP.DAL.Repositories;
using Microsoft.AspNetCore.Mvc;
using Models.System;
using System;
using System.Threading.Tasks;
namespace AuthServer.Controllers.api
{
    public class UserRoleDTO
    {
        public string UserRoleName { get; set; }
        public string Description { get; set; }
        public string ApplicationID { get; set; }
    }

    [Produces("application/json")]
    public class RolesController : Controller
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        [HttpPost(template: "api/Roles/CreateAppRole")]
        public async Task<IActionResult> CreateAppRole([FromBody]UserRoleDTO userRoleDTO)
        {
            try
            {
                using (unitOfWork)
                {
                    var newRole = new UserRole
                    {
                        ApplicationID = userRoleDTO.ApplicationID,
                        Description = userRoleDTO.Description,
                        UserRoleName = userRoleDTO.UserRoleName
                    };

                    await unitOfWork.UserRoleRepository.InsertAsync(newRole);
                    return Ok(new
                    {
                        newRole.UserRoleID
                    });
                }
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }


        [HttpGet(template: "api/Roles/GetRoles")]
        public async Task<IActionResult> GetRoles([FromQuery]string UserID, [FromQuery]string ApplicationID)
        {
            try
            {
                var Results = await unitOfWork.ApplicationUserRoleRepository.GetRoles(UserID: UserID, ApplicationID: ApplicationID);

                return Ok(Results);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }



        [HttpGet(template: "api/Roles/HasRole")]
        public async Task<IActionResult> HasRole([FromQuery] string AppId, [FromQuery] string UserID, [FromQuery]int RoleID)
        {
            try
            {
                var Result = await unitOfWork.ApplicationUserRoleRepository.HasRole(AppId, UserID, RoleID);
                return Ok(Result);
            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
        }

    }
}