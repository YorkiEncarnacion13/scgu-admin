﻿using ERP.DAL.Repositories;
using Microsoft.AspNetCore.Mvc;
using Repositories.DTOS.Gets;
using System.Collections.Generic;
namespace AuthServer.Controllers.api
{
    [Produces("application/json")]
    public class CountriesController : Controller
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        [HttpGet(template: "api/Countries")]
        public IEnumerable<CountryDTO> Get()
        {
            return unitOfWork.CountryRepository.GetCountriesDTOS();
        }
    }
}