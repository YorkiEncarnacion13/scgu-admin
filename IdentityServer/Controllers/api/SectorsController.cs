﻿using ERP.DAL.Repositories;
using Microsoft.AspNetCore.Mvc;
using Models.Users;
using System.Collections.Generic;
namespace AuthServer.Controllers.api
{
    [Produces("application/json")]
    //[Route("api/Sectors")]
    public class SectorsController : Controller
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        [HttpGet(template: "api/Sectors/GetMunicipalitySectors/{id}")]
        public IEnumerable<Sector> GetMunicipalitySectors(int id)
        {
            return unitOfWork.SectorRepository.GetMunicipalitySectors(id);
        }
    }
}