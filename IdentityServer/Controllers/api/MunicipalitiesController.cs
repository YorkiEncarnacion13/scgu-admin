﻿using ERP.DAL.Repositories;
using Microsoft.AspNetCore.Mvc;
using Models.Users;
using System.Collections.Generic;
namespace AuthServer.Controllers.api
{
    [Produces("application/json")]
    //[Route("api/Municipalities")]
    public class MunicipalitiesController : Controller
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        [HttpGet(template: "api/Municipalities/GetProvinceMunicipalities/{id}")]
        public IEnumerable<Municipality> GetProvinceMunicipalities(int id)
        {
          
            return unitOfWork.MunicipalityRepository.GetProvinceMunicipalities(id);
        }

    }
}
