﻿using ERP.DAL.Repositories;
using Microsoft.AspNetCore.Mvc;
using Models.Users;
using System.Collections.Generic;
namespace AuthServer.Controllers.api
{
    [Produces("application/json")]
    [Route("api/Provinces")]
    public class ProvincesController : Controller
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        [HttpGet]
        public async System.Threading.Tasks.Task<IEnumerable<Province>> Get()
        {
            return  unitOfWork.ProvinceRepository.GetAll();
        }
    }
}