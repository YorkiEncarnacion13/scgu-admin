﻿using ERP.DAL.Repositories;
using Microsoft.AspNetCore.Mvc;
using Models.Auth;
using Repositories.DTOS.Creates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
namespace AuthServer.Controllers.api
{
    [Produces("application/json")]
    public class UsersController : Controller
    {
        UnitOfWork unitofwork = new UnitOfWork();

        [HttpGet(template: "api/Users/GetUsers")]
        public async Task<IActionResult> GetUsers([FromQuery]int Page, [FromQuery] int Qntity)
        {
            try
            {
                using (unitofwork)
                {
                    var results = await unitofwork.UsersRepository.GetUsersAsync(Page, Qntity);
                    return Ok(results);
                }
            }
            catch (System.Exception e)
            {

                return BadRequest(e);
            }
        }



        [HttpGet(template: "api/Users/GetUsersFiltered/")]
        public async Task<IActionResult> GetUsers(
            [FromQuery]int Page,
            [FromQuery]int Qntity,
            [FromQuery]string SearchTerm)
        {
            using (unitofwork)
            {
                var Results = await unitofwork.UsersRepository.GetUsersFilteredAsync(Page, Qntity, SearchTerm);
                return Ok(Results);
            }
        }
        public class GetUsersParams
        {
            public List<string> Ids { get; set; }
        }
        [HttpGet(template: "api/Users/GetUsersList")]
        public async Task<IActionResult> GetUsersList([FromQuery]string UserIds)
        {
            try
            {
                using (unitofwork)
                {
                    var ids = UserIds.Split(',').ToList();
                    var Results = await unitofwork.UsersRepository.GetUsersList(ids);
                    return Ok(Results);
                }
            }
            catch (System.Exception e)
            {

                return BadRequest(e);
            }
        }

        [HttpPost(template: "api/Users/CreateNewUser")]
        public async Task<object> Create([FromBody]UserCreateDTO value)
        {
            try
            {
                if (ModelState.IsValid == false)
                    if (value == null)
                        return BadRequest();



                var user = unitofwork.UsersRepository.CreateUser(value);

                await SendWelcomeEmail(user);
                return new { user.CreatedUser.UserID, user.ClearTextPassword };
            }
            catch (System.Exception e)
            {

                throw e;
            }
        }

        private static async Task SendWelcomeEmail(global::Repositories.DTOS.Results.UserCreationResult user)
        {
            var smtpClient = new SmtpClient
            {
                Host = "smtp-mail.outlook.com", // set your SMTP server name here
                Port = 587, // Port 
                EnableSsl = true,
                Credentials = new NetworkCredential("servicios@micm.gob.do", "T3cnologia")
            };

            using (var message = new MailMessage("servicios@micm.gob.do", user.CreatedUser.Email)
            {

                Body = $@"<h1 style=*font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; color:#407fce*>Solicitud de Registro Nueva cuenta de Usuario MICM</h1>
<p>Hola {user.CreatedUser.FullName}, Respondiendo a tu solicitud, hemos creado una cuenta de usuario a su nombre para el uso de los sistemas que pertenecen
    a la red SCGU.
    <br> Tu contraseña es:
    <strong>{user.ClearTextPassword}</strong>
    <br>
    <strong style=*color: red*>OJO:</strong> <br>
* El sistema le solicitará que cambie la contraseña la primera vez que inicie sesión. <br>
* Por favor, no respondas a este mensaje, el mismo es enviado de forma automática y no sera revisado. <br>
* Su usuario sera verificado antes de darle acceso a cualquier sistema.
* Código de usuario: {user.CreatedUser.UserID}
    <hr>
    Sistema Centralizado de Gestión de Usuarios (SCGU) <br>
Dirección de Tecnologías de la Información, y Comunicaciones - MICM {DateTime.Now.Year}
</p>
".Replace('*', '"'),
            })
            {
                message.Subject = "Cuenta MICM - NO CONTESTAR";
                message.Bcc.Add("brawny.mateo@micm.gob.do");
                message.Priority = MailPriority.High;
                message.IsBodyHtml = true;
                await smtpClient.SendMailAsync(message);
            }
        }

        private static async Task SendRestablecerPasswordEmail(global::Repositories.DTOS.Results.UserCreationResult user)
        {
            var smtpClient = new SmtpClient
            {
                Host = "smtp-mail.outlook.com", // set your SMTP server name here
                Port = 587, // Port 
                EnableSsl = true,
                Credentials = new NetworkCredential("servicios@micm.gob.do", "T3cnologia")
            };

            using (var message = new MailMessage("servicios@micm.gob.do", user.CreatedUser.Email)
            {

                Body = $@"<h1 style=*font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; color:#407fce*>Solicitud de Reestablecimiento de Contraseña MICM</h1>
                    <p>Hola {user.CreatedUser.FullName}, Respondiendo a tu solicitud, hemos creado una de contraseña provisional para el uso de los sistemas que pertenecen a la red SCGU.
                        <br> Tu contraseña es:
                        <strong>{user.ClearTextPassword}</strong>
                        <br>
                        <strong style=*color: red*>OJO:</strong> <br>
                    * El sistema le solicitará que cambie la contraseña la primera vez que inicie sesión. <br>
                    * Por favor, no respondas a este mensaje, el mismo es enviado de forma automática y no sera revisado. <br>
                    * Su usuario sera verificado antes de darle acceso a cualquier sistema.
                    * Código de usuario: {user.CreatedUser.UserID}
                        <hr>
                        Sistema Centralizado de Gestión de Usuarios (SCGU) <br>
                    Dirección de Tecnologías de la Información, y Comunicaciones - MICM {DateTime.Now.Year}
                    </p>
                    ".Replace('*', '"'),
                                })
                                {
                message.Subject = "Cuenta MICM - NO CONTESTAR";
                message.Bcc.Add("brawny.mateo@micm.gob.do");
                message.Priority = MailPriority.High;
                message.IsBodyHtml = true;
                await smtpClient.SendMailAsync(message);
            }
        }

        [HttpPut(template: "api/Users/Update")]
        public async Task<IActionResult> Update([FromBody]UserUpdateDTO value)
        {
            try
            {
                if (value == null) return BadRequest();

                var user = unitofwork.UsersRepository.FindById(value.UserID);

                user.Address = value.Address;
                user.BirthCountry = unitofwork.CountryRepository.FindById(value.BirthCountry.CountryIso2Code);
                user.BirthDate = value.BirthDate;
                user.Department = unitofwork.DepartmentRepository.FindById(value.Department.DepartmentID);
                user.Email = value.Email;
                user.FirstLastName = value.FirstLastName;
                user.FirstName = value.FirstName;
                user.FullName = value.FirstName + " " + value.SecondName + " " + value.FirstName + " " + value.SecondLastName;
                user.Gender = value.Gender;
                user.JobTitle = unitofwork.JobTitleRepository.FindById(value.JobTitle.JobTitleID);
                user.Location = unitofwork.LocationRepository.FindById(value.Location.LocationID);
                user.Municipality = unitofwork.MunicipalityRepository.FindById(value.Municipality.MunicipalityID);
                user.Province = unitofwork.ProvinceRepository.FindById(value.Province.ProvinceID);
                user.SecondName = value.SecondName;
                user.SecondLastName = value.SecondLastName;
                user.Sector = unitofwork.SectorRepository.FindById(value.Sector.SectorID);

                unitofwork.UsersRepository.Update(user);

                return Ok();
            }
            catch (System.Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet(template: "api/Users/GetUser/{UserId}")]
        public ActionResult GetUser(string UserId)
        {
            try
            {

                var user = unitofwork.UsersRepository.GetUserDTO(UserId);
                return Ok(user);
            }
            catch (System.Exception e)
            {

                return BadRequest(e);
            }
        }

        [HttpGet(template: "api/Users/IsEmailValid/{Email}")]
        public object IsEmailValid(string Email)
        {
            if (string.IsNullOrEmpty(Email)) return BadRequest();

            return Ok(new
            {
                IsEmailValid = !unitofwork.UsersRepository.ExistsByEmail(Email)
            });
        }

        [HttpGet(template: "api/Users/IsIDValid/{ID}")]
        public object IsIDValid(string ID)
        {
            if (string.IsNullOrEmpty(ID)) return BadRequest();

            return Ok(new
            {
                isIDValid = !unitofwork.UsersRepository.Exists(ID)
            });
        }

        [HttpGet(template: "api/Users/GetCreatedUserViewModel/{UserID}")]
        public async Task<IActionResult> GetCreatedUserViewModel(string UserID)
        {
            try
            {
                using (unitofwork)
                {
                    var Countries = await unitofwork.CountryRepository.GetAllAsync();
                    var user = await unitofwork.UsersRepository.FindByUserIDAsync(UserID);

                    return Ok(new
                    {
                        Countries,
                        Provinces = await unitofwork.ProvinceRepository.GetAllAsync(),
                        Sectors = user.MunicipalityID != null ? unitofwork.SectorRepository.GetMunicipalitySectors((int)user.MunicipalityID) : null,
                        Municipalities = user.ProvinceID != null ? unitofwork.MunicipalityRepository.GetProvinceMunicipalities((int)user.ProvinceID) : null,
                        user = new
                        {
                            user.UserID,
                            user.Address,
                            user.BirthCountry,
                            user.BirthDate,
                            Department = await unitofwork.DepartmentRepository.FindByIdAsync(user.DepartmentID),
                            user.Email,
                            user.FirstLastName,
                            user.FirstName,
                            user.FullName,
                            user.Gender,
                            user.isServerAdmin,
                            JobTitle = await unitofwork.JobTitleRepository.FindByIdAsync(user.JobTitleID),
                            Location = await unitofwork.LocationRepository.FindByIdAsync(user.LocationID),
                            Municipality = await unitofwork.MunicipalityRepository.FindByIdAsync(user.MunicipalityID),
                            Province = await unitofwork.ProvinceRepository.FindByIdAsync(user.ProvinceID),
                            user.SecondLastName,
                            user.SecondName,
                            Sector = await unitofwork.SectorRepository.FindByIdAsync(user.SectorID),
                            user.Status
                        }
                    });
                }
            }
            catch (System.Exception e)
            {

                return BadRequest(e);
            }
        }

        [HttpPut(template: "api/Users/ToggleUserStatus/{UserID}")]
        public async Task<IActionResult> ToggleUserStatus([FromRoute] string UserID)
        {
            try
            {
                using (unitofwork)
                {
                    var _user = await unitofwork.UsersRepository.FindByIdAsync(UserID);

                    if (_user.Status == UserStatuses.Deactivated)
                        _user.Status = UserStatuses.Normal;

                    else _user.Status = UserStatuses.Deactivated;

                    await unitofwork.UsersRepository.UpdateAsync(_user);
                    return Ok();
                }
            }
            catch (System.Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet(template: "api/Users/FindByEmailOrID/{UserId}")]
        public object FindByEmailOrID(string UserId)
        {
            try
            {

                var user = unitofwork.UsersRepository.FindByEmailOrID(UserId);
                return Ok(user);
            }
            catch (System.Exception e)
            {

                return BadRequest(e);
            }
        }

        [HttpPost(template: "api/Users/ResetPassword")]
        public async Task<object> ResetPassword([FromBody] User value)
        {
            try
            {

                var user = unitofwork.UsersRepository.ChangePasswordGenerate(value);


                await SendRestablecerPasswordEmail(user);
                return new { user.CreatedUser.UserID, user.ClearTextPassword };
            }
            catch (System.Exception e)
            {

                throw e;
            }
        }
    }
}