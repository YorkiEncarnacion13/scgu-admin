﻿using ERP.DAL.Repositories;
using Microsoft.AspNetCore.Mvc;
using Models.Auth;
using System.Collections.Generic;
namespace AuthServer.Controllers.api
{
    [Produces("application/json")]
    public class DepartmentsController : Controller
    {
        UnitOfWork unitOfWork = new UnitOfWork();


        [HttpGet(template: "api/Departments")]
        public IEnumerable<Department> Get() => unitOfWork.DepartmentRepository.GetAll();

        [HttpGet(template: "api/Departments/GetDepartments/{SearchTerm}")]
        public IEnumerable<Department> GetDepartments(string SearchTerm) => unitOfWork.DepartmentRepository.SearchFor(SearchTerm);



    }
}