﻿using ERP.DAL.Repositories;
using Microsoft.AspNetCore.Mvc;
using Models.System;
using Newtonsoft.Json;
using Repositories.DTOS.Creates;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
namespace AuthServer.Controllers.api
{
    [Produces("application/json")]
    public class ApplicationsController : Controller
    {
        public static HttpResponseMessage InternalServerError(object Data)
        {

            HttpResponseMessage msg = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            StringContent content = new StringContent(JsonConvert.SerializeObject(Data));
            msg.Content = content;
            return msg;
        }
        private UnitOfWork unitOfWork = new UnitOfWork();

        [HttpPost(template: "/api/Applications")]
        public async Task<ActionResult> Post([FromBody]ApplicationCreateDTO NewApplication)
        {
            try
            {
                using (unitOfWork)
                {
                    // The '?' character serves as a claim divider and thus, its use in application names is not legal.
                    if (NewApplication.ApplicationName.Contains("?")) return BadRequest("The application name cannot contain the '?' character.");

                    var newApp = await unitOfWork.ApplicationRepository.CreateFromDTOAsync(NewApplication);
                    var response = new { newApp.ApplicationID };
                    return Ok(response);
                }
            }
            catch (System.Exception e)
            {
                if (e.InnerException != null) return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
                return BadRequest();
            }
        }
        [HttpPut(template: "/api/Applications")]
        public async Task<ActionResult> Put([FromBody]Application application)
        {
            try
            {
                using (unitOfWork)
                {
                    // The '?' character serves as a claim divider and thus, its use in application names is not legal.
                    if (application.ApplicationID.Contains("?")) return BadRequest("The application name cannot contain the '?' character.");

                    var app = await unitOfWork.ApplicationRepository.FindByIdAsync(application.ApplicationID);

                    app.Description = application.Description;
                    app.DisplayName = application.DisplayName;
                    app.Uri = application.Uri;
                    app.ApplicationStatus = application.ApplicationStatus;


                    await unitOfWork.ApplicationRepository.UpdateAsync(app);

                    return Ok();
                }
            }
            catch (System.Exception e)
            {
                if (e.InnerException != null) return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
                return BadRequest();
            }
        }
        [HttpGet(template: "api/Applications/GetAppInfo/{AppID}")]
        public async Task<ActionResult> GetAppInfo(string AppID)
        {
            try
            {
                using (unitOfWork)
                {
                    var result = await unitOfWork.ApplicationRepository.GetAppViewModel(AppID);
                    return Ok(result);
                }
            }
            catch (System.Exception e)
            {
                return BadRequest(e);
            }
        }
        [HttpGet(template: "api/Applications/GetList")]
        public async Task<ActionResult> GetList([FromQuery]int page, [FromQuery]int qntity)
        {
            try
            {
                using (unitOfWork)
                {
                    var results = await unitOfWork.ApplicationRepository.GetList(page, qntity);
                    return Ok(results);
                }
            }
            catch (System.Exception e)
            {
                return BadRequest(e);
            }
        }
        public class TurnOnRolesDTO
        {
            public string ApplicationID { get; set; }
            public string UserID { get; set; }
            public List<int> RolesIDs { get; set; }
        }
        public class UpdateRolesParams
        {
            public string ApplicationID { get; set; }
            public string UserID { get; set; }
            public List<int> RolesToTurnOnIDs { get; set; }
            public List<int> RolesToTurnOffIDs { get; set; }
        }
        [HttpPut(template: "api/Applications/UserRoles/UpdateUserRoles")]
        public async Task<ActionResult> UpdateUserRoles([FromBody]UpdateRolesParams Params)
        {
            try
            {
                using (unitOfWork)
                {
                    var AppUser = await unitOfWork.ApplicationUserRepository.GetAsync(Params.ApplicationID, Params.UserID);

                    if (Params.RolesToTurnOnIDs?.Count > 0)
                        await unitOfWork.ApplicationUserRoleRepository.TurnOnRolesAsync(AppUser, Params.RolesToTurnOnIDs.ToArray());

                    if (Params.RolesToTurnOffIDs?.Count > 0)
                        await unitOfWork.ApplicationUserRoleRepository.TurnOffRolesAsync(AppUser, Params.RolesToTurnOffIDs.ToArray());

                    return Ok();
                }
            }
            catch (System.Exception e)
            {
                return BadRequest(e);
            }
        }
        [HttpPut(template: "api/Applications/UserRoles/TurnOnRoles")]
        public async Task<ActionResult> TurnOnRoles([FromBody]TurnOnRolesDTO Params)
        {
            try
            {
                using (unitOfWork)
                {
                    var AppUser = await unitOfWork.ApplicationUserRepository.GetAsync(Params.ApplicationID, Params.UserID);

                    await unitOfWork.ApplicationUserRoleRepository.TurnOnRolesAsync(AppUser, Params.RolesIDs.ToArray());

                    return Ok();
                }
            }
            catch (System.Exception e)
            {
                return BadRequest(e);
            }
        }
        [HttpPut(template: "api/Applications/UserRoles/TurnOffRoles")]
        public async Task<ActionResult> TurnOffRoles([FromBody]TurnOnRolesDTO Params)
        {
            try
            {
                using (unitOfWork)
                {
                    var AppUser = await unitOfWork.ApplicationUserRepository.GetAsync(Params.ApplicationID, Params.UserID);
                    await unitOfWork.ApplicationUserRoleRepository.TurnOffRolesAsync(AppUser, Params.RolesIDs.ToArray());
                    return Ok();
                }
            }
            catch (System.Exception e)
            {
                return BadRequest(e);
            }
        }
        [HttpPost(template: "/api/Applications/CreateApplicationUser")]
        public async Task<ActionResult> CreateApplicationUser([FromBody]NewApplicationUserDTO newApplicationUser)
        {
            try
            {
                using (unitOfWork)
                {
                    await unitOfWork.ApplicationRepository.CreateApplicationUserFromDTO(newApplicationUser);
                    return Ok();
                }
            }
            catch (System.Exception error)
            {
                return BadRequest(error);
            }
        }
        [HttpGet(template: "/api/Applications/GetManifest")]
        public async Task<IActionResult> GetManifest([FromQuery]string AppID)
        {
            using (unitOfWork)
            {
                var result = await unitOfWork.ApplicationRepository.GetManifest(AppID);
                string ResultContent = JsonConvert.SerializeObject(result);
                var bytes = Encoding.UTF8.GetBytes(ResultContent);
                return File(bytes, "application/json", AppID + "-MANIFEST.json");
            }
        }
        [HttpPut(template: "/api/Applications/EnableUser")]
        public async Task<IActionResult> EnableUser([FromQuery]int AppUserID)
        {
            try
            {
                var status = ApplicationUserStatus.Normal;
                await ToggleUserStatus(AppUserID, status);
                return Ok();

            }
            catch (System.Exception e)
            {
                return BadRequest(e);
            }
        }
        [HttpPut(template: "/api/Applications/DisableUser")]
        public async Task<IActionResult> DisableUser([FromQuery]int AppUserID)
        {
            try
            {
                var status = ApplicationUserStatus.Disabled;
                await ToggleUserStatus(AppUserID, status);
                return Ok();
            }
            catch (System.Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet(template: "/api/Applications/GetApplicationUsers/{ApplicationID}")]
        public async Task<IActionResult> GetApplicationUsers(string ApplicationID)
        {
            using (unitOfWork)
            {
                var res = await unitOfWork.ApplicationRepository.GetApplicationUsers(ApplicationID);
                return Ok(res);
            }
        }

        [HttpGet(template: "/api/Applications/GetApplicationUsersByRole")]
        public async Task<IActionResult> GetApplicationUsersByRole([FromQuery]string ApplicationID, [FromQuery]int RoleID)
        {
            try
            {
                using (unitOfWork)
                {
                    var res = await unitOfWork.ApplicationRepository.GetApplicationUsersByRole(ApplicationID, RoleID);
                    return Ok(res);
                }
            }
            catch (System.Exception e)
            {
                return BadRequest(e);
            }
        }


        [HttpGet(template: "/api/Applications/GetApplicationStatus/{ApplicationID}")]
        public async Task<IActionResult> GetApplicationStatus(string ApplicationID)
        {
            try
            {
                using (unitOfWork)
                {
                    var status = await unitOfWork.ApplicationRepository.GetApplicationStatus(ApplicationID);
                    return Ok(status);
                }
            }
            catch (System.Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut(template: "/api/Applications/UpdateAppStatus")]
        public async Task<IActionResult> UpdateAppStatus([FromQuery] string ApplicationID,
            [FromQuery] ApplicationStatuses applicationStatuses)
        {
            try
            {
                using (unitOfWork)
                {
                    var app = await unitOfWork.ApplicationRepository.FindByIdAsync(ApplicationID);
                    app.ApplicationStatus = applicationStatuses;
                    await unitOfWork.ApplicationRepository.UpdateAsync(app);
                    return Ok();
                }
            }
            catch (System.Exception e)
            {
                return BadRequest(e);
            }
        }

        // Helpers
        private async Task ToggleUserStatus(int AppUserID, ApplicationUserStatus status)
        {
            using (unitOfWork)
            {
                var appuser = await unitOfWork.ApplicationUserRepository.FindByIdAsync(AppUserID);
                appuser.AppUserStatus = status;
                await unitOfWork.ApplicationUserRepository.UpdateAsync(appuser);
            }
        }
    }
}