﻿using AuthServer.ViewModels;
using AuthServerHelpers.Extensions;
using ERP.DAL.Repositories;
using IdentityServer4.Events;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models.Auth;
using System;
using System.Threading.Tasks;

namespace AuthServer.Controllers
{

    public class AccountController : Controller
    {

        private readonly UnitOfWork unitOfWork = new UnitOfWork();

        private readonly IIdentityServerInteractionService _interaction;
        private readonly IClientStore _clientStore;
        private readonly IAuthenticationSchemeProvider _schemeProvider;
        private readonly IEventService _events;
        private readonly IConfiguration _Configuration;
        public AccountController(
            IIdentityServerInteractionService interaction,
            IClientStore clientStore,
            IAuthenticationSchemeProvider schemeProvider,
            IEventService events,
            IConfiguration configuration
            )
        {
            _interaction = interaction;
            _clientStore = clientStore;
            _schemeProvider = schemeProvider;
            _events = events;
            _Configuration = configuration;
        }

        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl)
        {
            var context = await _interaction.GetAuthorizationContextAsync(returnUrl);

            var displayName =await unitOfWork.ApplicationRepository.GetApplicationDisplayNameAsync(context.ClientId);
            
            return View("Login", new LoginVM
            {
                ReturnURL = returnUrl,
                ClientID = context.ClientId,
                DisplayName = displayName
            });
        }


        [HttpGet(template:"SignUp")]
        public async Task<IActionResult> SignUp()
        {
            return View("SignUp");
        }

        [HttpGet(template: "RecuperarPassword")]
        public async Task<IActionResult> RecuperarPassword()
        {
            return View("RecuperarPassword");
        }

        public class PasswordResetParams
        {
            public string NewPassword { get; set; }
            public string Token { get; set; }
        }
        private async Task<bool> UpdatePassword(User user, PasswordResetParams Parameters)
        {

            if (user.PasswordResetToken != Parameters.Token.Sha512())
                return false;

            else await unitOfWork.UsersRepository.ChangePasswordAsync(user, Parameters.NewPassword);
            return true;

        }

        [HttpPost, Route("/Login")]
        public async Task<IActionResult> LoginAsync([FromBody]UserLoginParams parameters)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (unitOfWork.UsersRepository.ValidateCredentials(parameters.Identifier, parameters.Password))
                    {
                        var user = await unitOfWork.UsersRepository.FindByEmailOrIDAsync(parameters.Identifier);

                        if (string.IsNullOrEmpty(parameters.NewPassword) == false)
                        {
                            if (string.IsNullOrEmpty(parameters.NewPasswordToken))
                                return BadRequest();
                            await UpdatePassword(user, new PasswordResetParams
                            {
                                NewPassword = parameters.NewPassword,
                                Token = parameters.NewPasswordToken
                            });

                            user.Status = UserStatuses.Normal;

                            await unitOfWork.UsersRepository.UpdateAsync(user);

                        }

                        if (user.Status == UserStatuses.PendingPasswordReset)
                        {
                            string Token = Guid.NewGuid().ToString();

                            user.PasswordResetToken = Token.Sha512();

                            await unitOfWork.UsersRepository.UpdateAsync(user);

                            return Ok(new
                            {
                                ResetPassword = true,
                                Token = Token
                            });
                        }
                        if (user.Status == UserStatuses.Deactivated)
                            return Unauthorized();
                        else
                        {
                            await _events.RaiseAsync(new UserLoginSuccessEvent(user.Email, user.UserID, user.FirstName + " " + user.FirstLastName));
                            await HttpContext.SignInAsync(user.UserID, user.Email, new AuthenticationProperties
                            {
                                IsPersistent = true,
                                ExpiresUtc = DateTimeOffset.UtcNow.Add(TimeSpan.FromDays(1)),
                                IssuedUtc = DateTime.UtcNow

                            });
                            if (_interaction.IsValidReturnUrl(parameters.ReturnUrl) || Url.IsLocalUrl(parameters.ReturnUrl))
                            {

                                string Secret = _Configuration.GetSection("AppSettings")["DefaultEncryptKey"];

                                Response.Cookies.Append("uid", user.UserID.ToString().Encrypt("CChYxfKZsAej5p9Bv9LtazK9uCcbkuZYRAwW2ZXub3JqQDMMk"), new CookieOptions
                                {
                                    Expires = DateTime.Now.AddYears(2)
                                });

                                var request = await _interaction.GetAuthorizationContextAsync(parameters.ReturnUrl);

                                await unitOfWork.UserloginRepository.InsertAsync(new Classes.Users.UserLogin
                                {
                                    LoginDate = DateTime.Now,
                                    UserID = user.UserID,
                                    TargetApp_ApplicationID = request.ClientId,
                                    Browser = parameters.Browser,
                                    Browser_Version = parameters.Browser_Version,
                                    Language = parameters.Language,
                                    OS_Version = parameters.OS_Version,
                                    OS = parameters.OS,
                                    Screen = parameters.Screen,
                                });

                                return Ok(new { parameters.ReturnUrl });
                            }
                            return Redirect("/");
                        }
                    }
                    await _events.RaiseAsync(new UserLoginFailureEvent(parameters.Identifier, "invalid credentials"));
                    return NotFound();
                }
            }
            catch (Exception e)
            {

                return BadRequest(e);
            }
            return BadRequest();
        }

        [HttpGet]
        public async Task<IActionResult> Logout(string logoutId)
        {

            var logout = await _interaction.GetLogoutContextAsync(logoutId);
            string redirect = logout.PostLogoutRedirectUri;
            await HttpContext.SignOutAsync();
            return View("Logout", logout.ClientName);
        }


        [HttpGet(template: "/GoToApp/{appID}")]
        public async Task<IActionResult> GoToApp(string appID)
        {
            using (unitOfWork)
            {
                var app = await unitOfWork.ApplicationRepository.FindByIdAsync(appID);
                return Redirect(app.Uri);
            }
        }
    }
}