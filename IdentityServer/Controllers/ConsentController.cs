﻿using IdentityServer4.Services;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Mvc;
using ERP.DAL.Repositories;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using IdentityServer4.Models;
using Brawn.Helpers;
using AuthServerHelpers.Extensions;
using Models.System;
namespace AuthServer.Controllers
{
    public class ConsentController : Controller
    {
        private readonly IIdentityServerInteractionService _interaction;
        private readonly IClientStore _clientStore;
        private readonly IResourceStore _resourceStore;
        private readonly IConfiguration _Configuration;
        public ConsentController(
            IIdentityServerInteractionService interaction,
            IClientStore clientStore,
            IResourceStore resourceStore, IConfiguration configuration)
        {
            _interaction = interaction;
            _clientStore = clientStore;
            _resourceStore = resourceStore;
            _Configuration = configuration;

        }
        public IActionResult Index()
        {
            return View();
        }

        UnitOfWork unit = new UnitOfWork();
        public class ConsentFailureModel
        {
            public string AppName { get; set; }
            public string ErrorMsg { get; set; }
            public string ErrorTitle { get; set; }
        }
        [HttpGet,]
        public async Task<IActionResult> Index(string returnUrl)
        {
            try
            {
              
                var request = await _interaction.GetAuthorizationContextAsync(returnUrl);
                string UserID = string.Empty;
                Request.Cookies.TryGetValue("uid", out UserID);

                string Secret = _Configuration.GetSection("AppSettings")["DefaultEncryptKey"];
                UserID = UserID.Decrypt(Secret);

                ConsentResponse grantedConsent = null;

                using (unit)
                {
                    var client = await unit.ApplicationRepository.GetByIdAsync(request.ClientId);
                 
                    var AppUser = await unit.ApplicationUserRepository.GetAsync(request.ClientId, UserID);

                    if (client.ApplicationStatus == ApplicationStatuses.onMaintenance)
                        return View("onMaintenance", client);


                    if (AppUser.isNull())
                    {
                        grantedConsent = ConsentResponse.Denied;
                        await _interaction.GrantConsentAsync(request, grantedConsent);

                        // Remove all states cookies so user is redirected to login pane 
                        // if attempts to login once again
                        foreach (var cookie in Request.Cookies)
                            Response.Cookies.Delete(cookie.Key);

                        return View("ConsentFailure", new ConsentFailureModel
                        {
                            AppName = client.ApplicationID,
                            ErrorMsg = "No tienes Acceso al sistema \"" + client.ApplicationID + "\"",
                            ErrorTitle = "NO AUTORIZADO"
                        });
                    }
                    else if (AppUser.AppUserStatus == ApplicationUserStatus.Disabled)
                    {
                        return View("ConsentFailure", new ConsentFailureModel
                        {
                            AppName = client.ApplicationID,
                            ErrorMsg = $"Tu usuario en el sistema '{client.ApplicationID}' ha sido desactivado.".Replace("'", "\""),
                            ErrorTitle = "USUARIO DESACTIVADO"
                        });
                    }
                    else
                    {
                        grantedConsent = new ConsentResponse
                        {
                            RememberConsent = false,
                            ScopesConsented = request.ScopesRequested
                        };
                        await _interaction.GrantConsentAsync(request, grantedConsent);
                     
                        return Redirect(returnUrl);
                    }
                }
            }
            catch (System.Exception e)
            {

                return View("ExceptionError", e);
            }
        }
    }
}