﻿namespace SCGULite
{
    public class ScguUser
    {
        public string UserID { get; set; }
        public string Email { get; set; }
    }
    public interface IScguUserParser
    {
        System.Threading.Tasks.Task<ScguUser> Parse(System.Security.Claims.ClaimsPrincipal principal);
    }
}
