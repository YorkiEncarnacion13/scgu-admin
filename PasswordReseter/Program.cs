﻿using ERP.DAL.Repositories;
using Repositories.MISC;
using System;

namespace PasswordReseter
{
    class Program
    {
        static void Main(string[] args)
        {
            var unit = new UnitOfWork();

            Console.WriteLine(@"
-------------------------------------------------
|               IDENTITY SERVER                 |
|------------------------------------------------
| APLICACION PARA RESTAURACION DE CREDENCIALES  |
|------------------------------------------------
| OPCIONES:                                     |
| 1) Cambio de Contraseña Manual.               |
| 2) Cambio de Contraseña Generada.             |
| 3) Cambio de Contraseña SELF-SERVICE          | 
|-----------------------------------------------|
");

            string Option = Console.ReadLine();

            if (Option == "1")
            {
                Console.Clear();
                Console.Write(@"
-------------------------------------------------
|               IDENTITY SERVER                 |
|------------------------------------------------
|             CAMBIO DE CONTRASEÑA              |
|------------------------------------------------
| Introduce el ID o Correo Electronico del      |
-> usuario: ");
                string UserID = Console.ReadLine();
                UserID = UserID.Replace(" ", "");

                var user = unit.UsersRepository.FindByEmailOrID(UserID);
                if (user == null)
                    Console.WriteLine(@"
<< USUARIO NO ENCONTRADO >>");
                else
                {
                    Console.Write($@"|
| USUARIO {user.FullName}
| EMAIL: {user.Email}

-> Ingresa la nueva contraseña: ");
                    string password = Console.ReadLine();
                    Console.Write(@"
-> Escribe la nueva contraseña una vez mas:  ");
                    string passwordRepeat = Console.ReadLine();

                    if (password == passwordRepeat)
                    {
                        Console.Write(@"
Procedemos a cambiar la contraseña? (Y/N):  ");

                        string Response = Console.ReadLine();
                        if (Response.ToUpper().Replace(" ", "") == "Y")
                        {
                            unit.UsersRepository.ChangePassword(user, password);
                            Console.WriteLine("Listo, la contraseña ha sido cambiada");
                        }
                    }
                }
            }

            if (Option == "2")
            {
                Console.Clear();
                Console.Write(@"
-------------------------------------------------
|               IDENTITY SERVER                 |
|------------------------------------------------
|          CAMBIO DE CONTRASEÑA GENERADA        |
| Con este metodo, el sistema genera una nueva  |
| contraseña para el usuario.                   |
|------------------------------------------------
| Introduce el ID o Correo Electronico del      |
-> usuario: ");
                string UserID = Console.ReadLine();
                UserID = UserID.Replace(" ", "");

                var user = unit.UsersRepository.FindByEmailOrID(UserID);
                if (user == null)
                    Console.WriteLine(@"
<< USUARIO NO ENCONTRADO >>");
                else
                {
                    Console.Write($@"|
| USUARIO {user.FullName}
| EMAIL: {user.Email} 
");
                    Console.Write("Procedemos a cambiar la contraseña por una autogenerada? (Y/N):  ");
                    string Response = Console.ReadLine();

                    if (Response.ToUpper().Replace(" ", "") == "Y")
                    {
                        var NewUserPassword = PasswordsMannager.GenerateRandomPassword(length: 8);
                        var PasswordsSet = PasswordsMannager.GenerateCredentials(Constants.Peppers, NewUserPassword);
                        user.Password = PasswordsSet.Password;
                        user.SecurityKey = PasswordsSet.Salt;

                        unit.UsersRepository.Update(user);

                        Console.WriteLine("Listo!, hemos actualizado la contraseña del usuario por la siguiente: " + NewUserPassword + @"");


                    }



                }
            }

            if (Option == "3")
            {
                Console.Clear();
                Console.Write(@"
-------------------------------------------------
|               IDENTITY SERVER                 |
|------------------------------------------------
|        CAMBIO DE CONTRASEÑA SELF SERVICE      |
| Con este metodo, el sistema le pedira al      |
| al usuario que cambie su contraseña la        |
| siguiente vez que inicie sesion               |
|------------------------------------------------
| Introduce el ID o Correo Electronico del      |
-> usuario: ");
                string UserID = Console.ReadLine();
                UserID = UserID.Replace(" ", "");

                var user = unit.UsersRepository.FindByEmailOrID(UserID);
                if (user == null)
                    Console.WriteLine(@"
<< USUARIO NO ENCONTRADO >>");
                else
                {
                    Console.Write($@"|
| USUARIO {user.FullName}
| EMAIL: {user.Email} 
");
                    Console.Write("Procedemos a generar la solicitud de reestablecimiento de contraseña? (Y/N):  ");
                    string Response = Console.ReadLine();

                    if (Response.ToUpper().Replace(" ", "") == "Y")
                    {
                        user.Status = Models.Auth.UserStatuses.PendingPasswordReset;
                        unit.UsersRepository.Update(user);
                        Console.WriteLine("Listo! La siguiente vez que el usuario intente iniciar sesion, el sistema le pedira que cambie su contraseña");
                    }
                }
            }
            Console.Read();
        }
    }
}