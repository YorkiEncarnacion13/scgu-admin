// <auto-generated />
namespace AuthServerContext.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class addsappstables : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addsappstables));
        
        string IMigrationMetadata.Id
        {
            get { return "201804251934130_adds apps tables"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
