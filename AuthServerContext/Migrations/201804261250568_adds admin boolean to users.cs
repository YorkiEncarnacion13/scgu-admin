namespace AuthServerContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addsadminbooleantousers : DbMigration
    {
        public override void Up()
        {
            AddColumn("AUTH.Users", "isServerAdmin", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("AUTH.Users", "isServerAdmin");
        }
    }
}
