namespace AuthServerContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addsusersroles : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "AUTH.UserRoles",
                c => new
                    {
                        UserRoleID = c.Int(nullable: false, identity: true),
                        UserRoleName = c.String(maxLength: 100),
                        Description = c.String(maxLength: 300),
                        Application_ApplicationID = c.Int(),
                    })
                .PrimaryKey(t => t.UserRoleID)
                .ForeignKey("AUTH.Applications", t => t.Application_ApplicationID)
                .Index(t => t.Application_ApplicationID);
            
            CreateTable(
                "AUTH.ApplicationUserRoles",
                c => new
                    {
                        ApplicationUserRoleID = c.Int(nullable: false, identity: true),
                        RoleAssignmentDate = c.DateTime(nullable: false),
                        ApplicationUser_ApplicationID = c.Int(),
                        ApplicationUser_UserID = c.String(maxLength: 100),
                        Role_UserRoleID = c.Int(),
                    })
                .PrimaryKey(t => t.ApplicationUserRoleID)
                .ForeignKey("AUTH.ApplicationUsers", t => new { t.ApplicationUser_ApplicationID, t.ApplicationUser_UserID })
                .ForeignKey("AUTH.UserRoles", t => t.Role_UserRoleID)
                .Index(t => new { t.ApplicationUser_ApplicationID, t.ApplicationUser_UserID })
                .Index(t => t.Role_UserRoleID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("AUTH.ApplicationUserRoles", "Role_UserRoleID", "AUTH.UserRoles");
            DropForeignKey("AUTH.ApplicationUserRoles", new[] { "ApplicationUser_ApplicationID", "ApplicationUser_UserID" }, "AUTH.ApplicationUsers");
            DropForeignKey("AUTH.UserRoles", "Application_ApplicationID", "AUTH.Applications");
            DropIndex("AUTH.ApplicationUserRoles", new[] { "Role_UserRoleID" });
            DropIndex("AUTH.ApplicationUserRoles", new[] { "ApplicationUser_ApplicationID", "ApplicationUser_UserID" });
            DropIndex("AUTH.UserRoles", new[] { "Application_ApplicationID" });
            DropTable("AUTH.ApplicationUserRoles");
            DropTable("AUTH.UserRoles");
        }
    }
}
