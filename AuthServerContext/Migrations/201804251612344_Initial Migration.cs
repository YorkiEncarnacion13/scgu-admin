namespace AuthServerContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "AUTH.Countries",
                c => new
                    {
                        CountryCode = c.String(nullable: false, maxLength: 6),
                        CountryName = c.String(nullable: false, maxLength: 30),
                        NationalityMasculine = c.String(maxLength: 30),
                        NationalityFemenine = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => t.CountryCode);
            
            CreateTable(
                "AUTH.Departments",
                c => new
                    {
                        DepartmentID = c.Int(nullable: false, identity: true),
                        DepartmentName = c.String(nullable: false, maxLength: 400),
                        Location_LocationID = c.Int(),
                    })
                .PrimaryKey(t => t.DepartmentID)
                .ForeignKey("AUTH.Locations", t => t.Location_LocationID)
                .Index(t => t.Location_LocationID);
            
            CreateTable(
                "AUTH.Locations",
                c => new
                    {
                        LocationID = c.Int(nullable: false, identity: true),
                        LocationName = c.String(),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.LocationID);
            
            CreateTable(
                "AUTH.JobTitles",
                c => new
                    {
                        JobTitleID = c.Int(nullable: false, identity: true),
                        JobTitleName = c.String(maxLength: 400),
                    })
                .PrimaryKey(t => t.JobTitleID);
            
            CreateTable(
                "AUTH.UserPhones",
                c => new
                    {
                        PhoneID = c.Int(nullable: false, identity: true),
                        PhoneNumber = c.String(),
                        PhoneType = c.Int(nullable: false),
                        User_UserID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.PhoneID)
                .ForeignKey("AUTH.Users", t => t.User_UserID)
                .Index(t => t.User_UserID);
            
            CreateTable(
                "AUTH.Users",
                c => new
                    {
                        UserID = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        SecondName = c.String(),
                        FirstLastName = c.String(),
                        SecondLastName = c.String(),
                        Email = c.String(),
                        Gender = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        BirthDate = c.DateTime(nullable: false),
                        isActive = c.Boolean(nullable: false),
                        Password = c.String(),
                        SecurityKey = c.Binary(),
                        BirthCountry_CountryCode = c.String(maxLength: 6),
                        Department_DepartmentID = c.Int(),
                        JobTitle_JobTitleID = c.Int(),
                    })
                .PrimaryKey(t => t.UserID)
                .ForeignKey("AUTH.Countries", t => t.BirthCountry_CountryCode)
                .ForeignKey("AUTH.Departments", t => t.Department_DepartmentID)
                .ForeignKey("AUTH.JobTitles", t => t.JobTitle_JobTitleID)
                .Index(t => t.BirthCountry_CountryCode)
                .Index(t => t.Department_DepartmentID)
                .Index(t => t.JobTitle_JobTitleID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("AUTH.UserPhones", "User_UserID", "AUTH.Users");
            DropForeignKey("AUTH.Users", "JobTitle_JobTitleID", "AUTH.JobTitles");
            DropForeignKey("AUTH.Users", "Department_DepartmentID", "AUTH.Departments");
            DropForeignKey("AUTH.Users", "BirthCountry_CountryCode", "AUTH.Countries");
            DropForeignKey("AUTH.Departments", "Location_LocationID", "AUTH.Locations");
            DropIndex("AUTH.Users", new[] { "JobTitle_JobTitleID" });
            DropIndex("AUTH.Users", new[] { "Department_DepartmentID" });
            DropIndex("AUTH.Users", new[] { "BirthCountry_CountryCode" });
            DropIndex("AUTH.UserPhones", new[] { "User_UserID" });
            DropIndex("AUTH.Departments", new[] { "Location_LocationID" });
            DropTable("AUTH.Users");
            DropTable("AUTH.UserPhones");
            DropTable("AUTH.JobTitles");
            DropTable("AUTH.Locations");
            DropTable("AUTH.Departments");
            DropTable("AUTH.Countries");
        }
    }
}
