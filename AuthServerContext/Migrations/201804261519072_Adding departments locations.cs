namespace AuthServerContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingdepartmentslocations : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("AUTH.Departments", "Location_LocationID", "AUTH.Locations");
            DropIndex("AUTH.Departments", new[] { "Location_LocationID" });
            CreateTable(
                "AUTH.DepartmentLocations",
                c => new
                    {
                        DepartmentID = c.Int(nullable: false),
                        LocationID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DepartmentID, t.LocationID })
                .ForeignKey("AUTH.Departments", t => t.DepartmentID, cascadeDelete: true)
                .ForeignKey("AUTH.Locations", t => t.LocationID, cascadeDelete: true)
                .Index(t => t.DepartmentID)
                .Index(t => t.LocationID);
            
            AddColumn("AUTH.Users", "Location_DepartmentID", c => c.Int());
            AddColumn("AUTH.Users", "Location_LocationID", c => c.Int());
            CreateIndex("AUTH.Users", new[] { "Location_DepartmentID", "Location_LocationID" });
            AddForeignKey("AUTH.Users", new[] { "Location_DepartmentID", "Location_LocationID" }, "AUTH.DepartmentLocations", new[] { "DepartmentID", "LocationID" });
            DropColumn("AUTH.Departments", "Location_LocationID");
        }
        
        public override void Down()
        {
            AddColumn("AUTH.Departments", "Location_LocationID", c => c.Int());
            DropForeignKey("AUTH.Users", new[] { "Location_DepartmentID", "Location_LocationID" }, "AUTH.DepartmentLocations");
            DropForeignKey("AUTH.DepartmentLocations", "LocationID", "AUTH.Locations");
            DropForeignKey("AUTH.DepartmentLocations", "DepartmentID", "AUTH.Departments");
            DropIndex("AUTH.DepartmentLocations", new[] { "LocationID" });
            DropIndex("AUTH.DepartmentLocations", new[] { "DepartmentID" });
            DropIndex("AUTH.Users", new[] { "Location_DepartmentID", "Location_LocationID" });
            DropColumn("AUTH.Users", "Location_LocationID");
            DropColumn("AUTH.Users", "Location_DepartmentID");
            DropTable("AUTH.DepartmentLocations");
            CreateIndex("AUTH.Departments", "Location_LocationID");
            AddForeignKey("AUTH.Departments", "Location_LocationID", "AUTH.Locations", "LocationID");
        }
    }
}
