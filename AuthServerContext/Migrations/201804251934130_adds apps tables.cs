namespace AuthServerContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addsappstables : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("AUTH.UserPhones", "User_UserID", "AUTH.Users");
            DropIndex("AUTH.UserPhones", new[] { "User_UserID" });
            DropPrimaryKey("AUTH.Users");
            CreateTable(
                "AUTH.Applications",
                c => new
                    {
                        ApplicationID = c.Int(nullable: false, identity: true),
                        ApplicationName = c.String(),
                        UsersCount = c.Int(nullable: false),
                        IconUrl = c.String(),
                        Description = c.String(maxLength: 400),
                        IntroductionDate = c.DateTime(nullable: false),
                        ApplicationStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ApplicationID);
            
            CreateTable(
                "AUTH.ApplicationUsers",
                c => new
                    {
                        ApplicationID = c.Int(nullable: false),
                        UserID = c.String(nullable: false, maxLength: 100),
                        RegistrationDate = c.DateTime(nullable: false),
                        SharePhones = c.Boolean(nullable: false),
                        ShareEmail = c.Boolean(nullable: false),
                        ShareDepartmentInfo = c.Boolean(nullable: false),
                        ShareGender = c.Boolean(nullable: false),
                        ShareAddress = c.Boolean(nullable: false),
                        SharePersonalInfo = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.ApplicationID, t.UserID })
                .ForeignKey("AUTH.Applications", t => t.ApplicationID, cascadeDelete: true)
                .ForeignKey("AUTH.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.ApplicationID)
                .Index(t => t.UserID);
            
            AlterColumn("AUTH.UserPhones", "User_UserID", c => c.String(maxLength: 100));
            AlterColumn("AUTH.Users", "UserID", c => c.String(nullable: false, maxLength: 100));
            AddPrimaryKey("AUTH.Users", "UserID");
            CreateIndex("AUTH.UserPhones", "User_UserID");
            AddForeignKey("AUTH.UserPhones", "User_UserID", "AUTH.Users", "UserID");
        }
        
        public override void Down()
        {
            DropForeignKey("AUTH.UserPhones", "User_UserID", "AUTH.Users");
            DropForeignKey("AUTH.ApplicationUsers", "UserID", "AUTH.Users");
            DropForeignKey("AUTH.ApplicationUsers", "ApplicationID", "AUTH.Applications");
            DropIndex("AUTH.UserPhones", new[] { "User_UserID" });
            DropIndex("AUTH.ApplicationUsers", new[] { "UserID" });
            DropIndex("AUTH.ApplicationUsers", new[] { "ApplicationID" });
            DropPrimaryKey("AUTH.Users");
            AlterColumn("AUTH.Users", "UserID", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("AUTH.UserPhones", "User_UserID", c => c.String(maxLength: 128));
            DropTable("AUTH.ApplicationUsers");
            DropTable("AUTH.Applications");
            AddPrimaryKey("AUTH.Users", "UserID");
            CreateIndex("AUTH.UserPhones", "User_UserID");
            AddForeignKey("AUTH.UserPhones", "User_UserID", "AUTH.Users", "UserID");
        }
    }
}
