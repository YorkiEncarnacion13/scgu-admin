﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Users
{
    public class Municipality
    {
        [Key]
        public int MunicipalityID { get; set; }
        public string MunicipalityName { get; set; }
        [ForeignKey("Province")]
        public int ProvinceID { get; set; }
        public Province Province { get; set; }
        public IEnumerable<Sector> Sectors { get; set; }
    }
}
