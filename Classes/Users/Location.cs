﻿using Models.Users;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models.Auth
{
    public class Location
    {
        [Key]
        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public string Address { get; set; }
        //public IEnumerable<DepartmentLocation> Departments { get; set; }
    }
}
