﻿using Models.Auth;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models.Users
{
    public class Province
    {
        [Key]
        public int ProvinceID { get; set; }
        public string ProvinceName { get; set; }
        public IEnumerable<Municipality> Municipalities { get; set; }
        public Country Country { get; set; }
    }
}
