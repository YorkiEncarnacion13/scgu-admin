﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Users
{
    public class Sector
    {
        [Key]
        public int SectorID { get; set; }
        public string SectorName { get; set; }
        [ForeignKey("Municipio")]
        public int MunicipalityID { get; set; }
        public Municipality Municipio { get; set; }
    }
}
