﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models.Auth
{
    public class JobTitle
    {
        [Key]
        public int JobTitleID { get; set; }
        [StringLength(400)]
        public string JobTitleName { get; set; }
        public IEnumerable<User> Employees { get; set; }
    }
}
