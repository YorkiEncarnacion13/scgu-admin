﻿using Models.Auth;
using Models.System;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Classes.Users
{
    public class UserLogin
    {
        [Key]
        public int LoginID { get; set; }
        [StringLength(100), MinLength(11)]
        public string UserID { get; set; }
        public DateTime LoginDate { get; set; }
        [ForeignKey("UserID")]
        public User User { get; set; }
        [ForeignKey("TargetApp_ApplicationID")]
        public Application TargetApp { get; set; }
        [StringLength(140)]
        public string TargetApp_ApplicationID { get; set; }
        [StringLength(350)]
        public string Browser { get; set; }
        [StringLength(350)]
        public string Browser_Version { get; set; }
        [StringLength(350)]
        public string OS { get; set; }
        [StringLength(350)]
        public string OS_Version { get; set; }

        [StringLength(350)]
        public string Language { get; set; }
        [StringLength(350)]
        public string Screen { get; set; }
    }
}
