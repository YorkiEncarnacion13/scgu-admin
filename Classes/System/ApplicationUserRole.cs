﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Models.System
{
    public class ApplicationUserRole
    {
        [Key]
        public int ApplicationUserRoleID { get; set; }
        public UserRole Role { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public DateTime RoleAssignmentDate { get; set; }
    }
}
