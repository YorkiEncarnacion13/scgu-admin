﻿using Models.Auth;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.System
{
    public enum ApplicationUserStatus
    {
        Normal = 2,
        Disabled = 3
    }
    public class ApplicationUser
    {
        [Key]
        public int ApplicationUserID { get; set; }
        [ForeignKey("Application"), StringLength(140)]
        public string ApplicationID { get; set; }
        [ForeignKey("User"), StringLength(100)]
        public string UserID { get; set; }
        public Application Application { get; set; }
        public User User { get; set; }
        public DateTime RegistrationDate { get; set; }

        /// <summary>
        /// What data can the application access
        /// from the user
        /// </summary>
        public bool SharePhones { get; set; }
        public bool ShareEmail { get; set; }
        public bool ShareDepartmentInfo { get; set; }
        public bool ShareGender { get; set; }
        public bool ShareAddress { get; set; }
        /// <summary>
        /// Personal Info:
        // Names & Surnames, birthdate, profile picture
        /// </summary>
        public bool SharePersonalInfo { get; set; }
        public IEnumerable<ApplicationUserRole> ApplicationUserRoles { get; set; }
        public ApplicationUserStatus AppUserStatus { get; set; }

    }
}
