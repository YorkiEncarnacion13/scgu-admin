﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.System
{
    public class UserRole
    {
        [Key]
        public int UserRoleID { get; set; }
        [StringLength(100)]
        public string UserRoleName { get; set; }
        [StringLength(300)]
        public string Description { get; set; }
        [ForeignKey("Application"), StringLength(140)]
        public string ApplicationID { get; set; }
        public Application Application { get; set; }
        public IEnumerable<ApplicationUserRole> ApplicationUserRoles { get; set; }
    }
}
