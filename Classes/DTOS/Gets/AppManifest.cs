﻿using System.Collections.Generic;

namespace Repositories.DTOS.Gets
{
    public class AppManifestRoles
    {
        public int RoleID { get; set; }
        public string RoleName { get; set; }
    }
    public class AppManifest
    {
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public IEnumerable<AppManifestRoles> Roles { get; set; }
    }
}
