﻿namespace Repositories.DTOS.Gets
{
    public class CountryDTO
    {
        public string CountryIso2Code { get; set; }
        public string CountryName { get; set; }
    }
}
