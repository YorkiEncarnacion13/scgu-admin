﻿using System.Collections.Generic;

namespace Repositories.DTOS.Creates
{
    public class NewApplicationUserDTO
    {
        public string UserID { get; set; }
        public string ApplicationID { get; set; }
        public List<int> UserRolesIds { get; set; }
    }
}
