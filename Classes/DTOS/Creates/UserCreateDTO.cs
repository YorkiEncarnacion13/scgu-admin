﻿using Models.Auth;
using Models.Users;
using System;

namespace Repositories.DTOS.Creates
{
    public class UserCreateDTO
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string FirstLastName { get; set; }
        public string SecondLastName { get; set; }
        public string Email { get; set; }
        public Genders Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public string Cedula { get; set; }
        public Department Department { get; set; }
        public bool isServerAdmin { get; set; }
        public Country BirthCountry { get; set; }
        public JobTitle JobTitle { get; set; }
        public Location Location { get; set; }
        //public int locationID { get; set; }
        public bool isActive { get; set; }
        public UserStatuses Status { get; set; }
        public Sector Sector { get; set; }
        public Municipality Municipality { get; set; }
        public Province Province { get; set; }
        public string Address { get; set; }
        public string Ext { get; set; }
    }
    public class UserUpdateDTO : UserCreateDTO
    {
        public string UserID { get; set; }
    }
}
