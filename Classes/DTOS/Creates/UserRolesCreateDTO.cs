﻿namespace Repositories.DTOS.Creates
{
    public class UserRolesCreateDTO
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
