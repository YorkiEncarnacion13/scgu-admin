﻿using Models.Auth;
namespace Repositories.DTOS.Results
{
    public class UserCreationResult
    {
        public User CreatedUser { get; set; }
        public string ClearTextPassword { get; set; }
    }
}
