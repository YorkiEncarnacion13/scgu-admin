﻿using System.Collections.Generic;
namespace IdentityServerRepositories.DTOS.Results
{
    public class VirtualCollectionResult<T> where T : class
    {
        public IEnumerable<T> Items { get; set; }
        public long VirtualRowCount { get; set; }
    }
}
